<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Auth;

class Staff extends Authenticatable{
    
    use Notifiable;

	protected $table = "staff";

    protected $primaryKey = 'staff_id';
    
    const staff_table = "staff";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'dob', 'nationality', 'role', 'phone_no'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected function isSystemAdmin(){

        if(  !Auth()->guard( "staff" )->check() )
            return false;
        
        $user = Auth()->guard( "staff" )->user();

        if(  $user["role"] == 1  )
            return true;
        else
            return false;

    }    

    
}
