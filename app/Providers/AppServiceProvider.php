<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

use App\Stock;
use App\Http\Traits\DBColumnsTrait;

class AppServiceProvider extends ServiceProvider
{

    use DBColumnsTrait;
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){

        Validator::extend('need_price_is_more_than_min', function($attribute, $value, $parameters, $validator) {

            $minPrice = Stock::where(  $this->stock_valid_plain, "=", "1")->min(  $this->stock_unit_cost_plain  );

            if(  $value >= $minPrice  )
                return true;
            
                return false;

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
