<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    
    protected $table = "product";

    const product_table = "product";

    protected $primaryKey = 'product_id';

}
