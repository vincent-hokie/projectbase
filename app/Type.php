<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model{
    
    protected $table = "type";
    
    protected $primaryKey = 'type_id';

    const type_table = "type";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

}
