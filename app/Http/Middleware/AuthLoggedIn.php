<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class AuthLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        if(  !Auth()->guard( "staff" )->check() &&  !Auth()->guard( "buyer" )->check() &&  !Auth()->guard( "farmer" )->check()  )
            return redirect("/");

        return $next($request);

    }

}
