<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


use App\Http\Traits\DBColumnsTrait;
use App\Stock;
use App\Product;
use App\Farmer;
use App\Land;
use App\User;


use App\Http\Requests\ValidateStock;
use App\Http\Requests\ValidateFilterParams;


trait StockTrait {

	use DBColumnsTrait;

	//if search parameters are set, persist them in the view using these
	private $startQuantity = null;
    private $endQuantity = null;
    private $startDate = null;
    private $endDate = null;
    public $stock = null;

	public function get_stock(  ValidateFilterParams $request, $product_id, $map  ){

		$stock = $this->getStockGivenProductID($product_id);

        if(  $request->input(  $this->input_startQuantity  )  )
            $this->startQuantity = $request->input(  $this->input_startQuantity  );

        if(  $request->input(  $this->input_endQuantity  )  )
            $this->startQuantity = $request->input(  $this->input_endQuantity  );

        if(  $request->input(  $this->input_startDate  )  )
            $this->startQuantity = $request->input(  $this->input_startDate  );

        if(  $request->input(  $this->input_endDate  )  )
            $this->startQuantity = $request->input(  $this->input_endDate  );

		if(  !Auth()->guard(  User::getRole()  )->check()  )
            $stock = $this->non_authenticated_user($stock);
        else{

        	//search parameters where sent through get variables
	        if(
	        		($request->input(  $this->input_startQuantity  ) || $request->input(  $this->input_endQuantity  ) || 
	            	$request->input(  $this->input_startDate  ) || $request->input(  $this->input_endDate  ))
	            ){

	            //either lower or upper limit date was set
	            if($request->input(  $this->input_startDate  ) !== null || $request->input(  $this->input_endDate  ) !== null) {

	                $lower = $request->input(  $this->input_startDate  ) !== null ? $request->input(  $this->input_startDate  ) : 0;
	                $upper = $request->input(  $this->input_endDate  ) !== null ? $request->input(  $this->input_endDate  ) : 0;

	                //ensure if both are set, lower is the smaller of the two
	                if($lower != 0 && $upper != 0){
	                    $temp = $lower;
	                    $lower = min($upper, $temp);
	                    $upper = max($upper, $temp);
	                }
	                
	                $stock = $this->filter_lower_date($stock, $lower);
	                $stock = $this->filter_upper_date($stock, $upper);

	            }

	        } //end if ( get variables are set )

        }


        //either lower or upper limit quantity was set
	    if($request->input(  $this->input_startQuantity  ) !== null || $request->input(  $this->input_endQuantity  ) !== null) {

	    	$lower = $request->input(  $this->input_startQuantity  ) !== null ? $request->input(  $this->input_startQuantity  ) : 0;
	    	$upper = $request->input(  $this->input_endQuantity  ) !== null ? $request->input(  $this->input_endQuantity  ) : 0;

	    	//ensure if both are set, lower is the smaller of the two
		    if($lower != 0 && $upper != 0){
		    	$temp = $lower;
		    	$lower = min($upper, $temp);
		    	$upper = max($upper, $temp);
		    }

	    	$stock = $this->filter_lower_quantity($stock, $lower);
	    	$stock = $this->filter_upper_quantity($stock, $upper);
	    }

	    //lets get the farmer's user id
	    $stock->join( $this->farmer_table, $this->farmer_id, '=', $this->stock_farmer_id  );

        //get the stock location
        $stock->join( $this->land_table, $this->land_id, '=', $this->stock_land_id  );

        
        //get only what we need
		$stock->select( 
			$this->farmer_first_name, 
			$this->farmer_last_name, 
            $this->farmer_email, 
            $this->farmer_verified, 
			$this->land_location,
            $this->land_city,
			$this->stock_table.'.*');


        return array(  
                $stock->get(), 
                $this->startQuantity, 
                $this->endQuantity, 
                $this->startDate, 
                $this->endDate, 
                number_format(  $stock->avg("unit_cost"),2  )  );
	}

	private function getStockGivenProductID($product_id){

		return Stock::where(  $this->stock_product_id, '=', $product_id  )
                        ->where(  $this->stock_valid_plain, "=", '1'  );
	}

	private function non_authenticated_user($stock){

		return $stock->where( $this->stock_maturity_date, "<=", date('Y-m-d'));
	}

	private function filter_lower_quantity($stock, $lower){

		if((int)$lower != 0){
	        $this->startQuantity = $lower;
	        $stock->where(function($stock) use ($lower){
	        	$stock->where(  $this->stock_quantity_actual, ">=", $lower);

	        	if(Auth::check())
	        		$stock->orWhere(  $this->stock_quantity_expected, ">=", $lower);
	        });
	        
	        return $stock;
	    }else
	    	return $stock;
	}

	private function filter_lower_date($stock, $lower){
		
		if((int)$lower != 0){
	        $this->startDate = $lower;
	        return $stock->where(  $this->stock_maturity_date, ">=", $lower);
	    }else
	    	return $stock;
	}

	private function filter_upper_quantity($stock, $upper){

		if((int)$upper != 0){
	        $this->endQuantity = $upper;
	        $stock->where(function($stock) use ($upper){
	        	$stock->where(  $this->stock_quantity_actual, "<=", $upper);

	        	if(Auth::check())
	        		$stock->orWhere(  $this->stock_quantity_expected, "<=", $upper);
	        });
	        return $stock;
		}else
	    	return $stock;
	}

	private function filter_upper_date($stock, $upper){
		
		if((int)$upper != 0){
	        $this->endDate = $upper;
	        return $stock->where(  $this->stock_maturity_date, "<=", $upper);
	    }else
	    	return $stock;
	}


	public function get_farmer_stock($item){

        $theFarmer = Auth()->guard(  User::getRole()  )->user();

        //get the buyers id given user_id
        $stock = Farmer::where( $this->farmer_id, '=', $theFarmer[  $this->farmer_id_plain  ] );

        if($item)
            $stock = $this->given_stock_id($item, $stock);

        //get the stock corresponding to the user id
        $stock->join(  $this->stock_table, $this->stock_farmer_id, '=', $this->farmer_id );

        //get the product info corresponding to the stock
        $stock->join(  $this->product_table, $this->product_id, '=', $this->stock_product_id  );

        //get the land info corresponding to the stock
        $stock->join(  $this->land_table, $this->land_id, '=', $this->stock_land_id  );

        //get only what we need
        $stock->select(  
            $this->stock_id, 
            $this->stock_quantity_actual, 
            $this->stock_quantity_expected, 
            $this->stock_maturity_date, 
            $this->stock_unit_cost, 
            $this->stock_valid, 
            $this->stock_product_id, 
            $this->stock_description, 
            $this->land_name ." as land_name", 
            $this->product_name, 
            $this->product_photo);

        return $stock->get();
    }

    private function given_stock_id($item, $stock){
    	
        return $stock->where(  $this->stock_id, '=', $item);
    }


    //inject validation request object into the controller method
    public function store_stock(ValidateStock $request){
        
        if(  Auth()->guard("farmer")->check()  ){

            $id = Auth()->guard("farmer")->user();
            $id = $id[  $this->farmer_id_plain  ];

             if(Stock::create([

                $this->stock_farmer_id_plain => $id,
                $this->stock_product_id_plain => $request->input('product_name'),
                $this->stock_land_id_plain => $request->input('land_name'),
                $this->stock_maturity_date_plain => $request->input('maturity_date'),
                $this->stock_unit_cost_plain => $request->input('unit_cost'),
                $this->stock_quantity_actual_plain => $request->input('quantity_actual'),
                $this->stock_quantity_expected_plain => $request->input('quantity_expected'),
                $this->stock_description_plain => $request->input('description')

                ])
            ){
                return redirect('/dashboard/my-stock');
            }

        }else
            return redirect('/');

    }


    /*
    *   new stock form
    */
    public function newStock(Request $request){

        //ensure the user logged in is a buyer
        $user = $this->determine_particular_role("farmer");

        $farmerDetails = Auth()->guard(  "farmer"  )->user();

        $prods = explode(",", $farmerDetails[  $this->farmer_products_plain  ]  );
        $products = Product::whereIn(  $this->product_id_plain, $prods  )->get();

        $lands = Land::where(  $this->land_farmer_id_plain, "=",  $farmerDetails[  $this->farmer_id_plain  ] )->get();

        //if user is a registered buyer, send role to file and open the file
        if( $user == 1 ){
            
            $send = [  "role" => "farmer", "products" => $products, "lands" => $lands  ];
            return view('new_stock', $send);

        }else
            return redirect('/');

    }



    /*
    *   get my stock
    */
    public function myStock(Request $request){

        //make sure logged in user is a buyer
        $user = $this->determine_particular_role("farmer");

        //if user is a buyer
        if( $user == 1 ){

            //get all the needs of the user
            $stock = $this->get_farmer_stock(null);

            //package info in an array
            $pageData = ["role" => "farmer", "stock" => $stock];

            //send this info back to the page
            return view('my_stock', $pageData);
        
        }else
            return redirect('/');

    }



    /*
    *   get a particular stock
    */
    public function getStock(Request $request, $item){

        //get a partcular stock
        $stock = $this->get_farmer_stock($item);

        //make sure user is a buyer
        $user = $this->determine_particular_role("farmer");

        //if we have a correct user and a valid stock
        if( $user == 1 && sizeof($stock) == 1){
            
            //package info in an array
            $pageData = ["role" => "farmer", "stock" => $stock[0]];

            //send packaged info to the page
            return view('stock_view', $pageData);
        
        }else
            return redirect('/');

    }



    /*
    *   get a particular stock (editable)
    */
    public function editStock(Request $request, $item){

        //get details of a particular need, products available and find out if user is really a registered buyer
        $stock = $this->get_farmer_stock($item);
        $user = $this->determine_particular_role("farmer");
        $products = Product::all();
        $lands = Land::where(  $this->land_farmer_id, "=", Auth()->guard(  "farmer"  )->user()[  User::getRole()."_id"  ]  )->get();

        //if user exists and so does the need, we package the data in an array and send it to the page
        if( $user == 1 && sizeof($stock) == 1){
            
            //package data in array
            $pageData = [  "role" => "farmer", "stock" => $stock[0], "products" => $products, "lands" => $lands  ];

            //send info to page
            return view('stock_edit', $pageData);
        
        }else
            return redirect('/');

    }


    /*
    *   update stock
    */
    //inject validation request so that whatever comes is first validated
    public function update_stock(ValidateStock $request, $item){

        //$update[  column_name ] = updated_value;
        $update[  $this->stock_product_id_plain  ] = $request->input("product_name");
        $update[  $this->stock_maturity_date_plain  ] = $request->input("maturity_date");
        $update[  $this->stock_quantity_actual_plain  ] = $request->input("quantity_actual");
        $update[  $this->stock_quantity_expected_plain  ] = $request->input("quantity_expected");
        $update[  $this->stock_unit_cost_plain  ] = $request->input("unit_cost");
        $update[  $this->stock_land_id_plain  ] = $request->input("land_name");
        $update[  $this->stock_description_plain  ] = $request->input("description");


        //if the update works, show the user their updated profile. Otherwise take them back to the editable profile to apply their changes again
        if(  Stock::where(  $this->stock_id, $item  )->update($update)  )
            return redirect('/dashboard/my-stock/'.$item);
        else
            return redirect('/dashboard/my-stock/'.$item.'/edit');

    }




    /* 
    *   delete stock
    */
    public function deleteStock(Request $request){

        //try delete a product with a given id, and send back whether it was a success or failure
        return Stock::where(  $this->stock_id, "=", $request->input("_data")  )->delete();
    }

    
    /*
    *   give/revoke validity of a stock
    */
    public function validate_stock(Request $request, $action, $id){
        
        if(  $action == "enable"  )
            return Stock::where(  $this->stock_id_plain, $id  )->update([ "valid" => "1"]);
        else if(  $action == "disable"  )
            return Stock::where(  $this->stock_id_plain, $id  )->update([ "valid" => "0"]);
        
    }

	
}