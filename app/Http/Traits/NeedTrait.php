<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Traits\DBColumnsTrait;

use App\Http\Requests\ValidateNeed;

use App\Buyer;
use App\Product;
use App\User;
use App\Need;

trait NeedTrait {

    use DBColumnsTrait;


    //inject validation request object into the controller method
    public function store_need(ValidateNeed $request){

        $buyer_id = Auth()->guard(  User::getRole()  )->user();

        if(Need::create([

            $this->need_buyer_id_plain => $buyer_id[  User::getRole().'_id'  ],
            $this->need_product_id_plain => $request->input('product_name'),
            $this->need_quantity_plain => $request->input('quantity'),
            $this->need_proposed_price_plain => $request->input('proposed_price'),
            $this->need_description_plain => $request->input('description'),
            $this->need_date_required_plain => $request->input('date_required')

            ])
        ){
            return redirect('/dashboard/my-needs');
        }

    }


    public function get_need($item){
        
        //get the buyer id given user_id
        $user = Auth()->guard("buyer")->user();
        $need = Buyer::where(  $this->buyer_id, '=', $user[  $this->buyer_id_plain  ]  );

        if($item)
            $need = $this->given_need_id($item, $need);

        //get the needs corresponding to the user id
        $need->join(  $this->need_table, $this->need_buyer_id, '=', $this->buyer_id  );

        //get the product info corresponding to the need
        $need->join(  $this->product_table, $this->product_id, '=', $this->need_product_id  );

        //get only what we need
        $need->select(
            $this->need_id,
            $this->need_quantity,
            $this->need_proposed_price,
            $this->need_date_required,
            $this->need_valid,
            $this->product_id,
            $this->product_name, 
            $this->product_photo, 
            $this->need_description  );

        return $need->get();
    }

    private function given_need_id($item, $need){
        return $need->where(  $this->need_id, '=', $item);
    }


    /*
    *   new need form
    */
    public function newNeed(Request $request){

        $products = Product::all();

        //ensure the user logged in is a buyer
        $user = $this->determine_particular_role("buyer");

        //if user is a registered buyer, send role to file and open the file
        if( $user == 1 ){
            
            $send = [  "role" => "buyer", "products" => $products  ];
            return view(  'new_need', $send  );

        }else
            return redirect('/');

    }



    /*
    *   get my needs
    */
    public function myNeeds(Request $request){

        //make sure logged in user is a buyer
        $user = $this->determine_particular_role("buyer");

        //if user is a buyer
        if( $user == 1 ){

            //get all the needs of the user
            $need = $this->get_need(null);

            //package info in an array
            $pageData = ["role" => "buyer", "needs" => $need];

            //send this info back to the page
            return view('my_needs', $pageData);
        
        }else
            return redirect('/');

    }



    /*
    *   get a particular need
    */
    public function getNeed(Request $request, $item){

        //get a partcular need
        $need = $this->get_need($item);
        
        //make sure user is a buyer
        $user = $this->determine_particular_role("buyer");

        //if we have a correct user and a valid need
        if( $user == 1 && sizeof($need) == 1){
            
            //package info in an array
            $pageData = ["role" => "buyer", "need" => $need[0]];

            //send packaged info to the page
            return view('need_view', $pageData);
        
        }else
            return redirect('/');

    }



    /*
    *   get a particular need (editable)
    */
    public function editNeed(Request $request, $item){

        //get details of a particular need, products available and find out if user is really a registered buyer
        $need = $this->get_need($item);
        $user = $this->determine_particular_role("buyer");
        $products = Product::all();

        //if user exists and so does the need, we package the data in an array and send it to the page
        if( $user == 1 && sizeof($need) == 1){
            
            //package data in array
            $pageData = ["role" => "buyer", "need" => $need[0], "products" => $products];

            //send info to page
            return view('need_edit', $pageData);
        
        }else
            return redirect('/');

    }

    /*
    *   update need
    */
    //inject validation request so that whatever comes is first validated
    public function update_need(ValidateNeed $request, $item){

        //$update[  column_name ] = updated_value;
        $update[  $this->need_product_id_plain  ] = $request->input("product_name");
        $update[  $this->need_date_required_plain  ] = $request->input("date_required");
        $update[  $this->need_proposed_price_plain  ] = $request->input("proposed_price");
        $update[  $this->need_description_plain  ] = $request->input("description");
        $update[  $this->need_quantity_plain  ] = $request->input("quantity");

        //if the update works, show the user their updated profile. Otherwise take them back to the editable profile to apply their changes again
        if(  Need::where(  $this->need_id_plain, $item)->update($update)  )
            return redirect('/dashboard/my-needs/'.$item);
        else
            return redirect('/dashboard/my-needs/'.$item.'/edit');
    }


    /* 
    *   delete need
    */
    public function deleteNeed(Request $request){

        //try delete a product with a given id, and send back whether it was a success or failure
        return Need::where(  $this->need_id_plain, "=", $request->input("_data")  )->delete();
    }




    //function used to calculate the number of needs a particular has
    public function numNeeds(){
        
        $user = Auth()->guard("buyer")->user();
        return Need::where(  $this->buyer_id_plain, "=", $user[$buyer_id_plain]  )->count();

    }



    /*
    *   give/revoke validity of a need
    */
    public function validate_need(Request $request, $action, $id){
        
        if(  $action == "enable"  )
            return Need::where(  $this->need_id_plain, $id  )->update([ "valid" => "1"]);
        else if(  $action == "disable"  )
            return Need::where(  $this->need_id_plain, $id  )->update([ "valid" => "0"]);
        
    }
    

}