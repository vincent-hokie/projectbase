<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Requests\ValidateNewFarmer;

use App\Http\Traits\DBColumnsTrait;

use App\Farmer;
use App\Country;
use App\Product;
use App\Land;

trait FarmerTrait {

    use DBColumnsTrait;

    /*
    *   new farmer form
    */
    public function newFarmer(Request $request){

        $countries = Country::all();

        //get countries for a dropdown menu
        $products = Product::all();

        //get countries for a dropdown menu
        //prepare info that page is going to use
        $send = [  "role" => "staff", "countries" => $countries, "products" => $products  ];

        return view('new_farmer', $send);

    }


    public function newFarmer2(Request $request){

        $farmer_id = "";

        if(  session('farmer_id')  )
            $farmer_id = session("farmer_id");
        else
            return redirect('/dashboard/new/farmer');


        //get countries for a dropdown menu
        $products = Product::all();

        //prepare info that page is going to use
        $send = [  "role" => "staff", "products" => $products, "farmer_id" => $farmer_id  ];

        return view('new_farmer2', $send);

    }


    public function newFarmer3(Request $request){

        //get countries for a dropdown menu
        $products = Product::all();

        $countries = Country::all();

        $farmer_id = "";

        if(  session('farmer_id')  )
            $farmer_id = session("farmer_id");
        else
            return redirect('/dashboard/new/farmer');


        /*if(  session('farmer_id')  )
            $farmer_id = session("farmer_id");
        else
            return redirect('/dashboard/new/farmer');*/


        //prepare info that page is going to use
        $send = [  "role" => "staff", "countries" => $countries, "products" => $products, "farmer_id" => $farmer_id  ];

        return view('new_farmer3', $send);

    }


    /*
    *   save new farmer
    */
    //inject validation request so that whatever comes is first validated
    public function farmer_insert(ValidateNewFarmer $request){


        //nationality can come from 2 seperate elements, we first check which was used
        $nationality = $request->input("nationality");
        if(  $nationality == "none"  )
            $nationality = null;

        //create a comma seperated value of crops the farmer will deal in
        $products = [];

        for (  $i = 0; $i < 5; $i++  )
            if(  array_search(  $request->input(  'product_type'.$i  ), $products  ) === FALSE  && $request->input(  'product_type'.$i  ) != "none" )
                array_push(  $products, $request->input(  'product_type'.$i  )  );

        if(  sizeof($products) > 0  )
            $products = implode(  ",", $products  );
        else
            $products = null;


        $user = Auth()->guard("staff")->user();
        $user = $user[$this->staff_id_plain];

        $userModel = new Farmer([
            $this->farmer_first_name_plain => $request->input('first_name'),
            $this->farmer_last_name_plain => $request->input('last_name'),
            $this->farmer_middle_name_plain => $request->input('middle_name'),
            $this->farmer_email_plain => $request->input('email'),
            $this->farmer_password_plain => bcrypt($request->input('password')),
            $this->farmer_dob_plain => $request->input('dob_date'),
            $this->farmer_nationality_plain => $nationality,
            $this->farmer_phone_no_plain => $request->input('phone_contact'),
            $this->farmer_location_plain => $request->input('location'),
            $this->farmer_staff_id_plain => $user,
            $this->farmer_products_plain => $products,
        ]);


        if(  $userModel->save()  )
            return redirect('/dashboard/new/farmer/3')->with(  "farmer_id", $userModel->farmer_id  );

    }


    public function farmer_insert_land(Request $request){
        
        $number = $request->input(  'number'  );
        $farmer_id = $request->input(  'farmer_id'  );
        $count = 0;

        for (  $i = 0; $i < $number; $i++  ) { 

            if(  $request->input(  'land'.$i  )  ){

                $landModel = new Land([
                    $this->land_farmer_id_plain => $farmer_id,
                    $this->land_name_plain => $request->input(  'land'.$i  ),
                    $this->land_location_plain => $request->input(  'latlng'.$i  ),
                    $this->land_size_plain => $request->input(  'size'.$i  ),
                    $this->land_city_plain => $request->input(  'general'.$i  ),
                    $this->land_city_location_plain => $request->input(  'general_latlng'.$i  ),
                ]);

                if(  $landModel->save()  )
                    $count++;

            }
                

        }


        if(  $count == 0  )
            return redirect('/dashboard/new/farmer/3')->with(  "farmer_id", $farmer_id  );
        else
            return redirect('/dashboard/farmers')->with(  "farmer_insert", "success"  );
    }


    public function farmer_update_land(Request $request, $id){
        
        $number = $request->input(  'number'  );
        $farmer_id = $request->input(  'farmer_id'  );
        $deleted = explode( ",", $request->input(  'delete'  )  );

        $count = 0;

        if(  sizeof($deleted) > 0  ){
            
            if(  Land::whereIn(  $this->land_id_plain, $deleted  )->delete() )
                $count++;

        }
            

        for (  $i = 0; $i < $number; $i++  ) { 

            if(  $request->input(  'land_id'.$i  )  ){

                $update = [];
                $update[  $this->land_name_plain  ] = $request->input(  'land'.$i  );
                $update[  $this->land_location_plain  ] = $request->input(  'latlng'.$i  );
                $update[  $this->land_size_plain  ] = $request->input(  'size'.$i  );
                $update[  $this->land_city_plain  ] = $request->input(  'general'.$i  );
                $update[  $this->land_city_location_plain  ] = $request->input(  'general_latlng'.$i  );

                if(  Land::where(  $this->land_farmer_id_plain, "=", $farmer_id  )
                        ->where(  $this->land_id_plain, "=", $request->input(  'land_id'.$i  )  )
                        ->update(  $update  )  )
                    $count++;

                continue;

            }


            if(  $request->input(  'land'.$i  )  ){

                $landModel = new Land([
                    $this->land_farmer_id_plain => $farmer_id,
                    $this->land_name_plain => $request->input(  'land'.$i  ),
                    $this->land_location_plain => $request->input(  'latlng'.$i  ),
                    $this->land_size_plain => $request->input(  'size'.$i  ),
                    $this->land_city_plain => $request->input(  'general'.$i  ),
                    $this->land_city_location_plain => $request->input(  'general_latlng'.$i  ),
                ]);

                if(  $landModel->save()  )
                    $count++;

            }
                

        }


        $farmer = Farmer::where( $this->farmer_id_plain, "=", $id)->get([ $this->farmer_email_plain ]);

        if(  $count == 0  ){

            if(  sizeof($farmer) == 1  ){
                return redirect('/dashboard/farmer/'.$farmer[0][ $this->farmer_email_plain ].'/land')->with(  "farmer_update", "false"  );
            }else
                return redirect('/dashboard/farmers');
            
        }else{

            if(  sizeof($farmer) == 1  ){
                return redirect('/dashboard/farmer/'.$farmer[0][ $this->farmer_email_plain ].'/land')->with(  "farmer_update", "true"  );
            }else
                return redirect('/dashboard/farmers');

        }

    }


    /*
    *   view farmers
    */
    public function view_farmers(Request $request){
    
        $user = Auth()->guard("staff")->user();
        $farmers = Farmer::where(  $this->staff_id_plain, "=", $user["staff_id"]  )->get();

        //prepare info that page is going to use
        $send = ["role" => "staff", "farmers" => $farmers];

        return view('farmers', $send);

    }



    /*
    *   verify farmer account
    */
    public function verify_farmer(Request $request, $id){
        
        return Farmer::where(  $this->farmer_id_plain, $id  )->update([ "verified" => "1"]);

    }


    /*
    *   disble farmer account
    */
    public function enable_farmer(Request $request, $id){
        
        return Farmer::where(  $this->farmer_id_plain, $id  )->update([ "enabled" => "1"]);
    }


    /*
    *   unverify farmer account
    */
    public function unverify_farmer(Request $request, $id){
        
        return Farmer::where(  $this->farmer_id_plain, $id  )->update([ "verified" => "0"]);
    }


    /*
    *   disble farmer account
    */
    public function disable_farmer(Request $request, $id){
        
        return Farmer::where(  $this->farmer_id_plain, $id  )->update([ "enabled" => "0"]);

    }



}