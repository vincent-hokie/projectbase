<?php

namespace App\Http\Traits;

use App\Stock;
use App\Farmer;
use App\Product;
use App\Buyer;
use App\Country;
use App\Need;
use App\Staff;
use App\Type;
use App\Land;
use App\Bid;

trait DBColumnsTrait {

    private $input_product_name = "product_name";
    private $input_startQuantity = "start_quantity";
    private $input_endQuantity = "end_quantity";
    private $input_startDate = "start_date";
    private $input_endDate = "end_date";



    private $farmer_table = Farmer::farmer_table;
    private $farmer_id_plain = 'farmer_id';
    private $farmer_first_name_plain = "first_name";
    private $farmer_middle_name_plain = "middle_name";
    private $farmer_last_name_plain = "last_name";
    private $farmer_location_plain = "location";
    private $farmer_email_plain = "email";
    private $farmer_password_plain = "password";
    private $farmer_dob_plain = "dob";
    private $farmer_nationality_plain = "nationality";
    private $farmer_phone_no_plain = "phone_no";
    private $farmer_products_plain = "products";
    private $farmer_verified_plain = "verified";
    private $farmer_staff_id_plain = "staff_id";


    private $farmer_id = Farmer::farmer_table.'.farmer_id';
    private $farmer_first_name = Farmer::farmer_table.".first_name";
    private $farmer_middle_name = Farmer::farmer_table.".middle_name";
    private $farmer_last_name = Farmer::farmer_table.".last_name";
    private $farmer_location = Farmer::farmer_table.".location";
    private $farmer_email = Farmer::farmer_table.".email";
    private $farmer_password = Farmer::farmer_table.".password";
    private $farmer_dob = Farmer::farmer_table.".dob";
    private $farmer_nationality = Farmer::farmer_table.".nationality";
    private $farmer_phone_no = Farmer::farmer_table.".phone_no";
    private $farmer_products = Farmer::farmer_table.".products";
    private $farmer_verified = Farmer::farmer_table.".verified";
    private $farmer_staff_id = Farmer::farmer_table.".staff_id";





    private $staff_table = Staff::staff_table;
    private $staff_id_plain = 'staff_id';
    private $staff_first_name_plain = "first_name";
    private $staff_last_name_plain = "last_name";
    private $staff_location_plain = "location";
    private $staff_email_plain = "email";
    private $staff_role_plain = "role";

    private $staff_id = Staff::staff_table.'.staff_id';
    private $staff_first_name = Staff::staff_table.".first_name";
    private $staff_last_name = Staff::staff_table.".last_name";
    private $staff_location = Staff::staff_table.".location";
    private $staff_email = Staff::staff_table.".email";
    private $staff_role = Staff::staff_table.".role";





    private $buyer_table = Buyer::buyer_table;
    private $buyer_id_plain = 'buyer_id';
    private $buyer_first_name_plain = "first_name";
    private $buyer_last_name_plain = "last_name";
    private $buyer_location_plain = "location";
    private $buyer_email_plain = "email";
    private $buyer_verified_plain = "verified";

    private $buyer_id = Buyer::buyer_table.'.buyer_id';
    private $buyer_first_name = Buyer::buyer_table.".first_name";
    private $buyer_last_name = Buyer::buyer_table.".last_name";
    private $buyer_location = Buyer::buyer_table.".location";
    private $buyer_email = Buyer::buyer_table.".email";
    private $buyer_verified = Buyer::buyer_table.".verified";




    private $stock_table = Stock::stock_table;
    private $stock_id_plain = "stock_id";
    private $stock_quantity_actual_plain = "quantity_actual";
    private $stock_quantity_expected_plain = "quantity_expected";
    private $stock_maturity_date_plain = "maturity_date";
    private $stock_valid_plain = "valid";
    private $stock_farmer_id_plain = "farmer_id";
    private $stock_product_id_plain = "product_id";
    private $stock_land_id_plain = "land_id";
    private $stock_description_plain = "description";
    private $stock_sowing_date_plain = "sowing_date";
    private $stock_unit_cost_plain = "unit_cost";

    private $stock_id = Stock::stock_table.".stock_id";
    private $stock_quantity_actual = Stock::stock_table.".quantity_actual";
    private $stock_quantity_expected = Stock::stock_table.".quantity_expected";
    private $stock_maturity_date = Stock::stock_table.".maturity_date";
    private $stock_valid = Stock::stock_table.".valid";
    private $stock_farmer_id = Stock::stock_table.".farmer_id";
    private $stock_product_id = Stock::stock_table.".product_id";
    private $stock_land_id = Stock::stock_table.".land_id";
    private $stock_description = Stock::stock_table.".description";
    private $stock_sowing_date = Stock::stock_table.".sowing_date";
    private $stock_unit_cost = Stock::stock_table.".unit_cost";




    private $product_table = Product::product_table;
    private $product_id_plain = "product_id";
    private $product_name_plain = "name";
    private $product_type_plain = "type";
    private $product_visits_plain = "visits";
    private $product_description_plain = "description";
    private $product_photo_plain = "photo";

    private $product_id = Product::product_table.".product_id";
    private $product_name = Product::product_table.".name";
    private $product_type = Product::product_table.".type";
    private $product_visits = Product::product_table.".visits";
    private $product_description = Product::product_table.".description";
    private $product_photo = Product::product_table.".photo";




    private $type_table = Type::type_table;
    private $type_id_plain = "type_id";
    private $type_name_plain = "name";

    private $type_id = Type::type_table.".type_id";
    private $type_name = Type::type_table.".name";




    private $country_table = Country::country_table;
    private $country_id_plain = "id";
    private $country_name_plain = "country_name";
    private $country_code_plain = "country_code";

    private $country_id = Country::country_table.".id";
    private $country_name = Country::country_table.".country_name";
    private $country_code = Country::country_table.".country_code";




    private $land_table = Land::land_table;
    private $land_id_plain = "land_id";
    private $land_farmer_id_plain = "farmer_id";
    private $land_name_plain = "name";
    private $land_location_plain = "location";
    private $land_size_plain = "size";
    private $land_city_plain = "city";
    private $land_city_location_plain = "city_location";

    private $land_id = Land::land_table.".land_id";
    private $land_farmer_id = Land::land_table.".farmer_id";
    private $land_name = Land::land_table.".name";
    private $land_location = Land::land_table.".location";
    private $land_size = Land::land_table.".size";
    private $land_city = Land::land_table.".city";
    private $land_city_location = Land::land_table.".city_location";





    private $need_table = Need::need_table;
    private $need_id_plain = "need_id";
    private $need_buyer_id_plain = "buyer_id";
    private $need_product_id_plain = "product_id";
    private $need_quantity_plain = "quantity";
    private $need_proposed_price_plain = "proposed_price";
    private $need_description_plain = "description";
    private $need_date_required_plain = "date_required";
    private $need_valid_plain = "valid";

    private $need_id = Need::need_table.".need_id";
    private $need_buyer_id = Need::need_table.".buyer_id";
    private $need_product_id = Need::need_table.".product_id";
    private $need_quantity = Need::need_table.".quantity";
    private $need_proposed_price = Need::need_table.".proposed_price";
    private $need_description = Need::need_table.".description";
    private $need_date_required = Need::need_table.".date_required";
    private $need_valid = Need::need_table.".valid";




    private $bid_table = Bid::bid_table;
    private $bid_id_plain = "bid_id";
    private $bid_farmer_id_plain = "farmer_id";
    private $bid_stock_id_plain = "stock_id";
    private $bid_buyer_id_plain = "buyer_id";
    private $bid_quantity_plain = "quantity";
    private $bid_unit_cost_plain = "unit_cost";
    private $bid_need_id_plain = "need_id";

    private $bid_id = Bid::bid_table.".bid_id";
    private $bid_farmer_id = Bid::bid_table.".farmer_id";
    private $bid_stock_id = Bid::bid_table.".stock_id";
    private $bid_buyer_id = Bid::bid_table.".buyer_id";
    private $bid_quantity = Bid::bid_table.".quantity";
    private $bid_unit_cost = Bid::bid_table.".unit_cost";
    private $bid_need_id = Bid::bid_table.".need_id";



	
}