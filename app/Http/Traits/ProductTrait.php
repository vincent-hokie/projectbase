<?php

namespace App\Http\Traits;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Http\Requests\ValidateProductInfo;
use App\Http\Requests\ValidateNewProduct;

use App\Product;
use App\Type;

trait ProductTrait {


    /*
    *   new product form
    */
    public function newProduct(Request $request){
        
        //find out if user is a staff
        $user = $this->determine_particular_role("staff");

        //if theyre staff, show them the form. Otherwise redirect to home page
        if(  $user == 1 && Auth()->guard("staff")->user()->role == 1  ){

            //if the user reaches this point, only one product was found
            $types = Type::all();

            //package information into an array and send it to the file where it will be used
            $send = [  "role" => "staff", "types" => $types  ];

            return view('new_product', $send);

        }else
            return redirect('/');
    }

    
    public function get_all_products(){
        
        $products = Product::where(  $this->product_id_plain, ">", 0 );

        //get the farmer info corresponding to the user id
        $products->join(  $this->type_table, $this->product_type, '=', $this->type_id  );

        //get only what we need
        $products->select( 
            $this->product_id, 
            $this->product_name,
            $this->product_visits,
            "$this->type_name as type",
            "$this->type_id as type_id");

        return $products->get();
    }


	public function get_product(Request $req){

    	if(!is_nan($req->input('id') )){

    		$product = Product::find($req->input('id'));

    		if(sizeof($product) == 1)
    			return $product;
    		else
    			return false;

    	}else
    		return false;
    	
    }

    public function product_search_result(Request $req){

    	if($req->query('search')){

            $products = Product::where('name', 'like', '%'.$req->query('search').'%')->get(['id', 'name', 'photo']);

            return array("parameter" => $req->query('search'), "result" => $products);

    	}else
    		return array("parameter" => null, "result" => []);

    }

    public function insert_product(ValidateNewProduct $req){

        $user = Auth()->guard("staff")->user();

        if(  $user->role == 1  ){

            $file = $req->file('image');
            $name = $req->input('product_name');
            $type = $req->input('product_type');
            $description = $req->input('description');

            if($this->image_upload($file, str_replace(" ", "-", $name))){

                $product = new Product();
                $product->photo = str_replace(" ", "-", $name).'.'.$file->getClientOriginalExtension();
                $product->name = $name;
                $product->type = $type;
                $product->description = $description;
                
                if($product->save())
                     return redirect(  '/dashboard/products/'  );
                 else
                    return redirect(  '/dashboard/new/product'  );
            }else
                return redirect(  '/dashboard/new/product'  );

        }else
            return redirect("/");
    	

    }

    private function check_image_size($size){
		
		if($size < 5*1024*1024)
			return true;
		else
			return false;

    }

    private function image_upload($file, $name, $oldName = null){

    	$ext = $file->getClientOriginalExtension();
		$destinationPath = 'images/products';

        //delete the image that exists from the db first before updating
        $extensions = ['jpeg', 'png', 'bmp', 'gif', 'svg'];


        if(  $oldName  )
            if(  File::exists(  $destinationPath.'/'.$oldName  )  )
                File::delete(  $destinationPath.'/'.$oldName  );


		//Move Uploaded File
		if($file->move($destinationPath,$name.'.'.$ext))
			return true;
		else
			return false;

    }

    private function is_image_accepted_format($file_type){
		
		if($file_type == 'image/png' || $file_type == 'image/jpeg')
			return true;
		else
			return false;

    }


    /* 
    *   see all products
    */
    public function products(Request $request){

        //determine whether the logged in user is a staff
        $user = $this->determine_particular_role("staff");


        if(  $user == 1  ){
            //the user is staff

            //get all the products
            $Product = $this->get_all_products();

            //package it in an array
            $pageData = ["role" => "staff", "Product" => $Product];

            //send it to our page where it will be used
            return view('products', $pageData);

        }else
            //if user isnt staff, take them to the home page
            return redirect('/');

    }


    /* 
    *   see a particular product
    */
    public function getProduct($item){

        //get a particular product
        $products = Product::
            where(  $this->product_name, 'like', '%'.$item.'%'  )
            ->orWhere(    $this->product_name, 'like', '%'.str_replace("-", " ", $item).'%')
            ->get();

        //if the name either returns no result or many results, redirect to search page
        if(sizeof($products) == 0 || sizeof($products) > 1){
            return redirect('/search?search='.$item);
        }

        //if the user reaches this point, only one product was found
        //get a particular product
        $products = Product::
                        where(  $this->product_name, 'like', '%'.$item.'%'  )
                        ->orWhere(    $this->product_name, 'like', '%'.str_replace("-", " ", $item).'%');

        //get the type info corresponding to the type id
        $products->join(  $this->type_table, $this->product_type, '=', $this->type_id  );

        //get only what we need
        $products->select( 
            $this->product_id,
            $this->product_name,
            $this->product_description,
            $this->product_photo,
            "$this->type_name as type");

        $products = $products->get();

        //determine whether user is staff
        $user = $this->determine_particular_role("staff");

        $isAdmin = Auth()->guard("staff")->user()->role == 1;

        //if user is staff, show the product profile, otherwise redirect to the home page
        if( $user == 1)
            return view('product_view', array("role" => "staff", "product" => $products[0], "isAdmin" => $isAdmin ));
        else
            return redirect('/');
    }



    /* 
    *   see a particular product
    */
    public function saveProduct($item, ValidateProductInfo $request){

        $user = Auth()->guard("staff")->user();

        if(  $user->role == 1  ){

             //$update[  column_name ] = updated_value;
            $file = null;
            if(  $request->file('image')  )
                $file = $request->file('image');

            $name = $request->input("product_name");
            $update["name"] = $name;
            $update["type"] = $request->input("product_type");
            $update["description"] = $request->input("description");



            //determine if the data sent back has been tampered with by checking if the name corresponds to the id
            $productExists = Product::
                where(  $this->product_id, "=", $request->input("_data"))
                ->where(  $this->product_name, "=", str_replace("-", " ", $item))
                ->get(['photo']);

            if(  sizeof(  $productExists  ) == 1 && $file  )
                if($this->image_upload($file, str_replace(" ", "-", $name), $productExists[0]['photo']  ))
                    $update["photo"] = str_replace(" ", "-", $name).'.'.$file->getClientOriginalExtension();
            


            

            //if the product exists & update works, show the user their updated product. 
            //Otherwise take them back to the editable profile to apply their changes again
            if( sizeof(  $productExists  ) == 1 && Product::where(  $this->product_id, $request->input("_data"))->update($update)  )
                return redirect('/dashboard/products/'.strtolower(  str_replace(" ", "-", $request->input("product_name")  ))  );
            else
                return redirect('/dashboard/products/'.strtolower(  str_replace(" ", "-", $request->input("product_name")  )).'/edit');

        }else
            return redirect("/");
       
    }
    



    /* 
    *   delete product
    */
    public function deleteProduct(Request $request){

        $user = Auth()->guard("staff")->user();

        if(  $user->role == 1  ){

            //try delete a product with a given id, and send back whether it was a success or failure
            return Product::where(  $this->product_id, "=", $request->input("_data"))->delete();

        }else
            return 0;
        
    }



    /* 
    *   edit a particular product form
    */
    public function editProduct($item){

        $user = Auth()->guard("staff")->user();
        $isAdmin = $user->role == 1;

        //get the product corresponding to a particualt name
        $products = Product::
                        where(  $this->product_name, 'like', '%'.$item.'%'  )
                        ->orWhere(  $this->product_name, 'like', '%'.str_replace("-", " ", $item).'%')
                        ->get();

        //if the name either returns no result or many results, redirect to search page
        if(sizeof($products) == 0 || sizeof($products) > 1){
            return redirect('/search?search='.$item);
        }  

        //if the user reaches this point, only one product was found
        $types = Type::all();

        //determine whether user is staff
        $user = $this->determine_particular_role("staff");

        //package it in an array
        $pageData = [  "role" => "staff", "product" => $products[0], "types" => $types, "isAdmin" => $isAdmin  ];

        //if user is staff, show the editable product profile, otherwise redirect to the home page
        if( $user == 1)
            return view(  'product_edit', $pageData  );
        else
            return redirect('/');
    }



}