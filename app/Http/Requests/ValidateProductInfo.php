<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateProductInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    
     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [
            
            'product_name.required' => 'We need the product name',
            'product_name.max' => 'We\'re expecting a name of less than 50 characters',
            'product_name.alpha' => 'We only expect alphabetic characters in this field',

            'product_type.required' => 'We need the product type',
            'product_type.integer' => 'This is not a valid product type',

            'image.required' => 'We need the image',
            'image.image' => 'This is not a valid image',
            'image.size' => 'This image is beyond the 5MB limit',

        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){

        return [
            
            'product_name' => 'required|max:50|alpha',
            'product_type' => 'required|integer',
            'description' => '',
            'image' => 'image|max:5120'

        ];
    }

}
