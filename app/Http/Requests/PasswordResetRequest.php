<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordResetRequest extends FormRequest{

        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    
     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'password.required' => 'Your account requires a password!',
            'password.min' => 'Your password should be at least 6 characters for proper security',
            'password.confirmed' => 'Your passwords dont match! Please re-enter',

        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'password' => 'required|min:6|confirmed',

        ];
    }


}
