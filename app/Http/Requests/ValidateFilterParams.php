<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateFilterParams extends FormRequest{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }


     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'product_name.integer' => 'We\'re expecting a valid product',

            'start_quantity.numeric' => 'This must be must be a number',

            'end_quantity.numeric' => 'This must be must be a number',

            'start_date.date' => 'This needs to be a proper date',

            'end_date.date' => 'This needs to be a proper date',
            
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => '',
            'start_quantity' => 'numeric',
            'end_quantity' => 'numeric',
            'start_date' => 'date',
            'end_date' => 'date',
        ];
    }

}
