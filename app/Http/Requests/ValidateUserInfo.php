<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;

use App\User;

class ValidateUserInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }


     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'first_name.required' => 'We need to know your first name!',
            'first_name.max' => 'We hope for a name less than 50 characters',
            'first_name.alpha' => 'We only expect alphabetic characters in this field',

            'last_name.required' => 'We need to know your first name!',
            'last_name.max' => 'We hope for a name less than 50 characters',
            'last_name.alpha' => 'We only expect alphabetic characters in this field',

            'middle_name.max' => 'We hope for a name less than 50 characters',
            'middle_name.alpha' => 'We only expect alphabetic characters in this field',

            'dob.required' => 'We\'ll need to know when you were born',
            'dob.date' => 'This should be a date!',

            'email.required' => 'We need your email address',
            'email.email' => 'This needs to be a proper email address',
            'email.max' => 'Your email appears to be too long',
            'email.unique' => 'We seem to already have this email, please change it',

            /*'gender.required' => 'We need to know your gender',
            'gender.in' => 'Please select a gender',*/


        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:50|alpha',
            'last_name' => 'required|max:50|alpha',
            'middle_name' => 'max:50|alpha',
            'dob' => 'required|date',
            'email' => 'required|email|max:255|unique:'.User::getRole().',email,'.Auth()->guard(  User::getRole()  )->user()[  User::getRole().'_id'].','.User::getRole().'_id',
            /*'gender' => 'required|in:M,F',*/
            'nationality' => '',
            'location' => '',
            'bio' => '',
            'phone_no' => 'numeric',
            
        ];
    }



}
