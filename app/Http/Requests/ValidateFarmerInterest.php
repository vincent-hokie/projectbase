<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateFarmerInterest extends FormRequest{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }


     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'stock_id.integer' => 'We\'re expecting a valid stock',
            'stock_id.required' => 'We need the stock',

            'buyer_id.integer' => 'We\'re expecting a valid stock',
            'buyer_id.required' => 'We need the stock',

            'need_id.numeric' => 'This must be must be a number',
            'need_id.required' => 'The cost must be filled',

            'quantity.numeric' => 'This must be must be a number',
            
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'quantity' => 'numeric',
            'unit_cost' => 'numeric',
            'stock_id' => 'required|integer',
            'buyer_id' => 'required|integer',
            'need_id' => 'required|integer',
        ];
    }
}
