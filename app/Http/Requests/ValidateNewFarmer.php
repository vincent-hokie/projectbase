<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateNewFarmer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }




    
     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [
            
            'first_name.required' => 'We need to know your first name!',
            'first_name.max' => 'We hope for a name less than 50 characters',
            'first_name.alpha' => 'We only expect alphabetic characters in this field',

            'last_name.required' => 'We need to know your first name!',
            'last_name.max' => 'We hope for a name less than 50 characters',
            'last_name.alpha' => 'We only expect alphabetic characters in this field',

            'middle_name.max' => 'We hope for a name less than 50 characters',
            'middle_name.alpha' => 'We only expect alphabetic characters in this field',
            
            'email.required' => 'We need your email address',
            'email.email' => 'This needs to be a proper email address',
            'email.max' => 'Your email appears to be too long',
            'email.unique' => 'We seem to already have this email, please change it',

            'password.required' => 'Your account requires a password!',
            'password.min' => 'Your password should be at least 6 characters for proper security',
            'password.confirmed' => 'Your passwords dont match! Please re-enter',

            'dob_date.required' => 'We\'ll need to know when you were born',
            'dob_date.date' => 'This should be a date!',

        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'first_name' => 'required|max:50|alpha',
            'last_name' => 'required|max:50|alpha',
            'middle_name' => 'max:50|alpha',
            'email' => 'required|email|max:255|unique:farmer',
            'password' => 'required|min:6|confirmed',
            'dob_date' => 'required|date',
            'nationality' => '',
            'location' => '',
            'phone_contact' => 'numeric',

        ];
    }


}
