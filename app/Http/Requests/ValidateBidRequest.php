<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateBidRequest extends FormRequest{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }


     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'stock_id.integer' => 'We\'re expecting a valid stock',
            'stock_id.required' => 'We need the stock',

            'unit_cost.numeric' => 'This must be must be a number',
            'unit_cost.required' => 'The cost must be filled',

            'quantity.numeric' => 'This must be must be a number',
            'quantity.required' => 'The quantity is needed',
            
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric',
            'unit_cost' => 'required|numeric',
            'stock_id' => 'required|integer',
        ];
    }
}
