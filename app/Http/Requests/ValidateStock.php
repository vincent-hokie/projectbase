<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateStock extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    
     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'product_name.required' => 'The product must be filled in',
            'product_name.integer' => 'We\'re expecting a valid product',

            'land_name.required' => 'The land must be filled in',
            'land_name.integer' => 'We\'re expecting a valid piece of land',

            'date_required.required' => 'We need to know when you want your products',
            'date_required.date' => 'This needs to be a proper date',

            'quantity_actual.required' => 'The quantity must be filled in',
            'quantity_actual.integer' => 'Quantity must be a number',

            'quantity_expected.required' => 'The quantity must be filled in',
            'quantity_expected.integer' => 'Quantity must be a number',

            'unit_cost.required' => 'The cost must be filled in',
            'unit_cost.integer' => 'Unit cost must be a number',

        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|integer',
            'land_name' => 'required|integer',
            'maturity_date' => 'required|date',
            'quantity_actual' => 'required|numeric',
            'quantity_expected' => 'required|numeric',
            'unit_cost' => 'required|numeric',
            'description' => '',
        ];
    }

}
