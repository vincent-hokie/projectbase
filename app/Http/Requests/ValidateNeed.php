<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateNeed extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


     /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages(){

        return [

            'product_name.required' => 'The product must be filled in',
            'product_name.integer' => 'we\'re expecting a valid product',

            'date_required.required' => 'We need to know when you want your products',
            'date_required.date' => 'This needs to be a proper date',

            'quantity.required' => 'The quantity must be filled in',
            'quantity.numeric' => 'Quantity must be a number',

            'proposed_price.required' => 'The unit cost must be filled in',
            'proposed_price.numeric' => 'Unit cost must be a number',
            
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|integer',
            'date_required' => 'required|date',
            'quantity' => 'required|numeric',
            'proposed_price' => 'required|numeric|need_price_is_more_than_min',
            'description' => '',
        ];
    }

}
