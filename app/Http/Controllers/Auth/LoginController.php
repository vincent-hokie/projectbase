<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    protected $guard = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

        $this->middleware('guest', ['except' => 'logout']);
    }


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(){

        $user = NULL;

        if (Auth::guard('buyer')->attempt(['email' => $email, 'password' => $password, 'enabled' => "1"  ])) {
            
            // Authentication passed...
            $user = Auth()->guard(  "buyer"  )->user();
            return redirect('/dashboard')->with(  "name", $user["first_name"]." ".$user["last_name"]  );

        }else if (Auth::guard('farmer')->attempt(['email' => $email, 'password' => $password, 'enabled' => "1"  ])) {
            
            // Authentication passed...
            $user = Auth()->guard(  "farmer"  )->user();
            return redirect('/dashboard')->with(  "name", $user["first_name"]." ".$user["last_name"]  );

        }else if (Auth::guard('staff')->attempt(['email' => $email, 'password' => $password, 'enabled' => "1"  ])) {
            
            // Authentication passed...
            $user = Auth()->guard(  "staff"  )->user();
         //   return redirect('/dashboard')->with(  "name", $user["first_name"]." ".$user["last_name"]  );

        }

    }


    public function authenticated( Request $request ) {
        // flash message
        $user = Auth()->guard(  User::getRole()  )->user();

        // This section is the only change
        if (  $user->enabled != "1"  ) {
            
            Auth()->guard(  User::getRole()  )->logout();

            return redirect("/login")
            ->withInput($request->only(  'email', 'remember'  ))
            ->withErrors([
                'enabled' => 'This account has been disabled. Contact the administrator through email( admin@agri.com  ) to have it reacticated.'
            ]);
            
        }

        return redirect("/dashboard")->with(  "name", $user["first_name"]." ".$user["last_name"]  );
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request){

        $credentials =  $this->credentials($request);
        $has_remember = $request->has('remember');
        $email = $request->input("email");
        $password = $request->input("password");

        if(  Auth::guard('buyer')->attempt($credentials, $has_remember)  ){
            $this->guard = "buyer";
            return true;
        }else if(  Auth::guard('farmer')->attempt($credentials, $has_remember)  ){
            $this->guard = "farmer";
            return true;
        }else if(Auth::guard('staff')->attempt($credentials, $has_remember)  ){
            $this->guard = "staff";
            return true;
        }

        return false;

        //return $this->guard()->attempt(
        //    $this->credentials($request), $request->has('remember')
        //);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect("/login")
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        /*/ the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }*/

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        $user = Auth()->guard( $this->guard )->user();

        return $this->authenticated(  $request, $user  )
                ?: redirect()->intended($this->redirectPath());
    }


    
}
