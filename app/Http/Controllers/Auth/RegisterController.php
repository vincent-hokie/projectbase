<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Country;
use App\Buyer;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data){

        return Validator::make($data, [
            'first_name' => 'required|max:50|alpha',
            'last_name' => 'required|max:50|alpha',
            'middle_name' => 'max:50|alpha',
            'dob_date' => 'required|date',
            'email' => 'required|email|max:255|unique:buyer',
            'password' => 'required|min:6|confirmed',
            /*'gender' => 'required',*/
            'nationality' => 'required',
            'location' => '',
            'buyer_profile' => ''
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $nationality = null;
        if(  $data['nationality'] != "none"  )
            $nationality = $data['nationality'];

        $location = null;
        if(trim($data['location']) != "")
            $location = $data['location'];

        $bio = null;
        if(trim($data['bio']) != "")
            $bio = $data['bio'];

        /*$gender = null;
        if(trim($data['gender']) == "M" || trim($data['gender']) == "F")
            $gender = $data['gender'];*/

        $userModel = new Buyer([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name'],
            'dob' => $data['dob_date'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            /*'gender' => $gender,*/
            'nationality' => $nationality,
            'location' => $location,
            'buyer_profile' => $bio
        ]);

        if(  $userModel->save()  )
            return $userModel;
        else
            return false;

    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        if(  $this->create(  $request->all()  )  )
            return redirect("/")->with("registration", "success");
        else
            return redirect("/")->with("registration", "fail");
    }


    
}
