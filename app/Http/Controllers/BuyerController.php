<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\NeedTrait;
use App\Http\Traits\ProductTrait;

use App\Http\Requests\ValidateBidRequest;

use App\User;
use App\Bid;
use App\Stock;

use DB;

/*use App\Product;
use App\Need;
use App\Buyer;
use App\Country;
use App\Staff;

use App\Farmer;

*/
class BuyerController extends Controller{
    
    use NeedTrait;

    //class constructor, called each time a function in this file is used
    public function __construct(){
    
        //to use this file, user must be logged in
        $this->middleware('auth:buyer');

    }

    //this function returns whether the currently logged in user is a buyer, farmer or staff
    private function determine_role(){

        return User::getRole();
    }


    //this determines the participation of a user in a specific role
    private function determine_particular_role($role){
        
        if( User::getRole() == $role  )
            return 1;
        else
            return 0;

    }


    public function buyerBids(Request $request, $stock_id, $buyer_id){

        $bids = Bid::where(  $this->bid_stock_id, "=", $stock_id  )
                    ->where(  $this->bid_buyer_id, "=", $buyer_id  )
                    ->join(  $this->stock_table, $this->bid_stock_id, $this->stock_id  )
                    ->join(  $this->farmer_table, $this->stock_farmer_id, $this->farmer_id  )
                    ->get([
                        Bid::bid_table.".*",
                        $this->farmer_first_name,
                        $this->farmer_last_name
                        ]);
        $stock = Stock::where(  $this->stock_id_plain, "=", $stock_id  )->get();

        if( sizeof($stock) == 1  )
            $stock = $stock[0];
        else
            $stock = [];

        return view("bid_history", [  "role" => "buyer", "bids" => $bids, "stock" => $stock  ]);

    }


    public function makeBid(ValidateBidRequest $request){  

        $user = Auth()->guard(  "buyer"  )->user();

        $bidModel = new Bid([
            $this->bid_stock_id_plain => $request->input('stock_id'),
            $this->bid_buyer_id_plain => $user[  $this->buyer_id_plain  ],
            $this->bid_quantity_plain => $request->input('quantity'),
            $this->bid_unit_cost_plain => $request->input('unit_cost')
        ]);

        if(  $bidModel->save()  )
            return 1;
        else
            return 0;

    }


    public function makeBidReload(ValidateBidRequest $request){  

        $user = Auth()->guard(  "buyer"  )->user();

        $bidModel = new Bid([
            $this->bid_stock_id_plain => $request->input('stock_id'),
            $this->bid_buyer_id_plain => $user[  $this->buyer_id_plain  ],
            $this->bid_quantity_plain => $request->input('quantity'),
            $this->bid_unit_cost_plain => $request->input('unit_cost')
        ]);

        if(  $bidModel->save()  )
            return redirect('/dashboard/buyer/bid/converstion/'.$request->input('stock_id').'/'.$request->input('buyer_id') )->with(  "bid_status", "true"  );
        else
            return redirect('/dashboard/buyer/bid/converstion/'.$request->input('stock_id').'/'.$request->input('buyer_id') )->with(  "bid_status", "false"  );

    }


    public function buyerBidHistory(Request $request){

        $user = Auth()->guard(  "buyer"  )->user();

        $bids = DB::table(  $this->bid_table  )
                 ->select(  $this->bid_buyer_id, $this->bid_stock_id, $this->stock_maturity_date, $this->stock_unit_cost, $this->product_name, $this->farmer_first_name, $this->farmer_last_name, $this->stock_quantity_actual, $this->stock_quantity_expected, $this->farmer_verified, $this->farmer_email  )
                 ->where(  $this->bid_buyer_id, "=", $user->buyer_id  )
                 ->groupBy(  $this->bid_buyer_id, $this->bid_stock_id, $this->stock_maturity_date, $this->stock_unit_cost, $this->product_name, $this->farmer_first_name, $this->farmer_last_name, $this->stock_quantity_actual, $this->stock_quantity_expected, $this->farmer_verified, $this->farmer_email   );
                 //->get();

        //$bids = Bid::where(  $this->bid_id, "<>",  null  );
        $bids->join(  $this->stock_table, $this->bid_stock_id, $this->stock_id  );
        $bids->join(  $this->farmer_table, $this->stock_farmer_id, $this->farmer_id  );
        $bids->join(  $this->product_table, $this->stock_product_id, $this->product_id  );

        return view("buyer_bid_history", [  "role" => "buyer", "bids" => $bids->get()  ]);

    }


}
