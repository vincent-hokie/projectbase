<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Traits\DBColumnsTrait;
use App\Http\Traits\StockTrait;
use App\Http\Traits\ProductTrait;

use App\Http\Requests\ValidateFilterParams;

use App\Http\Requests;
use App\Product;
use App\Stock;
use App\User;
use App\Type;
use DB;

use Redirect;

class MiscController extends Controller{

    use StockTrait;

    public function index($type=null){

        $menu = "";

        $types = Type::all()->sortBy('name');

        if($type){

            $wanted_type = Type::where(  "name", "=", str_replace("-", " ", $type))
                ->orWhere(  "name", "=", $type)
                ->get(["type_id"]);

            if( sizeof($wanted_type) != 1)
                $menu = Product::all()->sortByDesc('visits')->take(9);
            else
                $menu = Product::where("type", "=", $wanted_type[0]["type_id"])->get()->sortByDesc('visits')->take(9);

        }else
            $menu = Product::all()->sortByDesc('visits')->take(9);
            
        return view('index', 
            [  "menu" => $menu, "types" => $types, "typee" => $type  ]
            );

    }

    public function product_search(Request $request){

        return view('search', $this->product_search_result($request));
    }

    public function product_search_result(Request $req){

        if($req->query('search')){

            $products = Product::
                where(  $this->product_name_plain, 'like', '%'.strtolower(str_replace("-", " ", $req->query('search')  )).'%')
                ->orWhere(  $this->product_name_plain, 'like', '%'.$req->query('search').'%')
                ->get([  $this->product_id_plain , $this->product_name_plain, $this->product_photo_plain  ]);

            return array("parameter" => $req->query('search'), "result" => $products);

        }else
            return array("parameter" => null, "result" => []);

    }

    public function get_item_profile(ValidateFilterParams $request, $product){

        $products = Product::
                        where(  $this->product_name, 'like', '%'.$product  .'%')
                        ->orWhere(  $this->product_name, 'like', '%'.strtolower(str_replace("-", " ", $product )).'%')
                        ->get([  $this->product_id, $this->product_photo,  $this->product_name  ]);

        if(sizeof($products) == 0 || sizeof($products) > 1){
            return Redirect::to('/search?search='.$product);
        }

        $stock = $this->get_stock(  $request, $products[0][  $this->product_id_plain  ], false  );

        DB::table(  $this->product_table )->where(  $this->product_id_plain, "=", $products[0][  $this->product_id_plain  ]  )->increment('visits');

        if(!$stock[1])
            $stock[1] = 0;

        if(!$stock[2])
            $stock[2] = 0;

        if($stock[3])
            $stock[3] = date("Y-m-d", strtotime($stock[3]));

        if($stock[4])
            $stock[4] = date("Y-m-d", strtotime($stock[4]));

        return view('product_profile', array(
                                        "stock" => $stock[0], 
                                        "request" => $request, 
                                        "product" => $products[0],
                                        "startQuantity" => $stock[1],
                                        "endQuantity" => $stock[2],
                                        "startDate" => $stock[3],
                                        "endDate" => $stock[4],
                                        "role" => User::getRole(),
                                        "average" => $stock[5] ));

    }


    public function get_map_profile(ValidateFilterParams $request, $product){

        $products = Product::
                        where(  $this->product_name, 'like', '%'.$product  .'%')
                        ->orWhere(  $this->product_name, 'like', '%'.strtolower(str_replace("-", " ", $product )).'%')
                        ->get([  $this->product_id, $this->product_photo,  $this->product_name  ]);

        if(sizeof($products) == 0 || sizeof($products) > 1){
            return Redirect::to('/search?search='.$product);
        }

        $stock = $this->get_stock(  $request, $products[0][  $this->product_id_plain  ], true  );

        DB::table(  $this->product_table )->where(  $this->product_id_plain, "=", $products[0][  $this->product_id_plain  ]  )->increment('visits');

        if(!$stock[1])
            $stock[1] = 0;

        if(!$stock[2])
            $stock[2] = 0;

        if($stock[3])
            $stock[3] = date("Y-m-d", strtotime($stock[3]));

        if($stock[4])
            $stock[4] = date("Y-m-d", strtotime($stock[4]));

        return view('map_product_profile', array(
                                        "stock" => $stock[0], 
                                        "request" => $request, 
                                        "product" => $products[0],
                                        "startQuantity" => $stock[1],
                                        "endQuantity" => $stock[2],
                                        "startDate" => $stock[3],
                                        "endDate" => $stock[4],
                                        "role" => User::getRole(),
                                        "average" => $stock[5] ));

    }

}
