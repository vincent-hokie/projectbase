<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\FarmerTrait;
use App\Http\Traits\ProductTrait;

use App\Http\Requests\ValidateNewStaff;
use App\Http\Requests\ValidateProductInfo;

use App\User;
use App\Staff;
use App\Buyer;
use App\Farmer;
use App\Country;
use App\Product;
use App\Land;

class StaffController extends Controller{
    
    use FarmerTrait, ProductTrait;

    //class constructor, called each time a function in this file is used
    public function __construct(){
    
        //to use this file, user must be logged in
        $this->middleware('auth:staff');

    }

    //this function returns whether the currently logged in user is a buyer, farmer or staff
    private function determine_role(){

        return User::getRole();
    }


    //this determines the participation of a user in a specific role
    private function determine_particular_role($role){
        
        if( User::getRole() == $role  )
            return 1;
        else
            return 0;

    }


    /* 
    *   see staff profile
    */
    public function staff_profile(Request $request, $email){

        //get the logged in user info
        $user = Staff::where(  $this->staff_email_plain  , "=", $email  )->get();
        
        $nationality = "";

        if(  sizeof($user) == 1  ){

            if(  $user[0]->nationality  )
                $nationality = Country::where(  $this->country_id_plain, "=", $user[0]->nationality  )->get([  $this->country_name_plain  ]);

            //determine user role
            $role = $this->determine_role();

            //package information in an array
            $pageData = ["role" => $role, "user" => $user[0], "nationality" => $nationality, "profile" => false, "userRole" => "staff"  ];

            //send this info to our page where it will be used
            return view('user_profile', $pageData); 


        }else{

        }

        
    }



    /*
    *   new staff form
    */
    public function newStaff(Request $request){

        $countries = Country::all();

        //prepare info that page is going to use
        $send = [  "role" => "staff", "countries" => $countries  ];

        return view('new_staff', $send);

    }



    /*
    *   save new staff
    */
    //inject validation request so that whatever comes is first validated
    public function staff_insert(ValidateNewStaff $request){


        //nationality can come from 2 seperate elements, we first check which was used
        $nationality = $request->input("nationality");
        if(  $nationality == "none"  )
            $nationality = null;

        $staffModel = new Staff([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'middle_name' => $request->input('middle_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'dob' => $request->input('dob_date'),
            'nationality' => $nationality,
            'phone_no' => $request->input('phone_contact'),
        ]);

        if(  $staffModel->save()  )
            return redirect('/dashboard/staff')->with(  "staff", $request->input('first_name').' '.$request->input('last_name')  );

    }



    /*
    *   view staff
    */
    public function view_staff(Request $request){
        
        $staff = Staff::all();
        
        //prepare info that page is going to use
        $send = ["role" => "staff", "staff" => $staff];

        return view('staff', $send);

    }



    /*
    *   view buyers
    */
    public function view_buyers(Request $request){
        
        $buyers = Buyer::all();
        
        if(   Auth()->guard("staff")->user()->role == 1  ){
            
            //prepare info that page is going to use
            $send = ["role" => "staff", "buyers" => $buyers];

            return view('buyers', $send);

        }else
            return redirect("/");

    }



    /*
    *   enable/disable staff account
    */
    public function enable_staff(Request $request, $action, $id){
        
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  )
                return Staff::where(  $this->staff_email_plain, $id  )->update([ "enabled" => "1"]);
            else if(  $action == "disable"  )
                return Staff::where(  $this->staff_email_plain, $id  )->update([ "enabled" => "0"]);
        else
            return 0;

    }


    /*
    *   enable/disable staff account
    */
    public function enable_farmer(Request $request, $action, $id){
        
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  )
                return Farmer::where(  $this->farmer_email_plain, $id  )->update([ "enabled" => "1"]);
            else if(  $action == "disable"  )
                return Farmer::where(  $this->farmer_email_plain, $id  )->update([ "enabled" => "0"]);
        else
            return 0;

    }


    /*
    *   enable/disable buyer account
    */
    public function enable_buyer(Request $request, $action, $id){
        
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  )
                return Buyer::where(  $this->buyer_email_plain, $id  )->update([ "enabled" => "1"]);
            else if(  $action == "disable"  )
                return Buyer::where(  $this->buyer_email_plain, $id  )->update([ "enabled" => "0"]);
        else
            return 0;

    }



    /*
    *   verify/unverify farmer account
    */
    public function verify_farmer(Request $request, $action, $id){
        
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  )
                return Farmer::where(  $this->farmer_email_plain, $id  )->update([ "verified" => "1"]);
            else if(  $action == "disable"  )
                return Farmer::where(  $this->farmer_email_plain, $id  )->update([ "verified" => "0"]);
        else
            return 0;

    }



    /*
    *   verify/unverify buyer account
    */
    public function verify_buyer(Request $request, $action, $id){
        
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  )
                return Buyer::where(  $this->buyer_email_plain, $id  )->update([ "verified" => "1"]);
            else if(  $action == "disable"  )
                return Buyer::where(  $this->buyer_email_plain, $id  )->update([ "verified" => "0"]);
        else
            return 0;

    }



    /*
    *   give/revoke rights on staff account
    */
    public function verify_staff(Request $request, $action, $id){
            
        if( Staff::isSystemAdmin()  )
            if(  $action == "enable"  ){

                return Staff::where(  $this->staff_email_plain, $id  )->update([ "role" => "1"]);

            }else if(  $action == "disable"  ){

                $count = Staff::where(  $this->staff_role_plain, "=", "1")->count();

                if(  $count > 1  )
                    if(  Staff::where(  $this->staff_email_plain, $id  )->update([ "role" => "0"])  ){

                        if(  Auth()->guard(  "staff"  )->user()->email == $id  )
                            return 3;
                        else
                            return 1;

                    }else
                        return 0;
                else
                    return 2;

            }
        else
            return 0;

    }


    public function farmer_land(Request $request, $email){

        $farmer = Farmer::where(  $this->farmer_email_plain, "=", $email  )->get();
        $farmer_name = $farmer[0][  $this->farmer_first_name_plain  ]."  ".$farmer[0][  $this->farmer_last_name_plain  ];

        if(  sizeof($farmer) == 1  ){

            $lands = Land::where(  $this->land_farmer_id_plain, "=", $farmer[0][  $this->farmer_id_plain  ] )->get();
            
            //prepare info that page is going to use
            $send = [  
                "role" => "staff", 
                "lands" => $lands, 
                "farmer_id" => $farmer[0][  $this->farmer_id_plain  ], 
                "farmer_name" => $farmer_name, 
                "farmer_email" =>  $farmer[0][  $this->farmer_email_plain  ]  ];

            return view('farmer_land', $send);

        }
        

    }


    public function farmer_products(Request $request, $email){

        $products = Product::all();

        //prepare info that page is going to use
        $send = [  "role" => "staff", "products" => $products  ];

        return view('farmer_products', $send);

    }


    public function updateProducts(Request $request, $email){

        if(  Farmer::where(  $this->farmer_email_plain, $email  )->update([  $this->farmer_products_plain => implode(",", $request->input("products"))  ])  )
            return redirect(  '/dashboard/farmer/'.$email  )->with(  "productUpdateStatus", "true"  );
        else
            return redirect(  '/dashboard/farmer/'.$email  )->with("productUpdateStatus", "false"  );

    }


}
