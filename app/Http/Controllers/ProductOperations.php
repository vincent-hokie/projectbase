<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\ProductTrait;

use App\Http\Requests\ValidateProductInfo;

use App\Http\Requests;
use App\Product;
use Redirect;
use Illuminate\Foundation\Auth\RedirectsUsers;

class ProductOperations extends Controller{
    

    use ProductTrait;

    public function __construct(){
        $this->middleware('auth');
    }

    public function product_view(Request $req){

        $product = $this->get_product($req);

        if($product)
            return view("fragments.product_profile", array("product" => $product));
        else
            return false;

    }

    public function product_edit(Request $req){

        $product = $this->get_product($req);

        if($product)
            return view("fragments.product_profile_edit", array("product" => $product));
        else
            return false;

    }

    public function insert_product(ValidateProductInfo $req){

    	$file = $req->file('image');
    	$name = $req->input('product_name');
        $type = $req->input('product_type');
        $unit_cost = $req->input('unit_cost');
        $description = $req->input('description');

    	if($this->check_image_size($file->getSize()) && $this->is_image_accepted_format($file->getMimeType())){

    		if($this->image_upload($file, $name)){

    			$product = new Product();
    			$product->photo = $name.'.'.$file->getClientOriginalExtension();
    			$product->name = $name;
                $product->type = $type;
                $product->unit_cost = $unit_cost;
                $product->description = $description;
    			
                if($product->save())
                    return redirect('/dashboard/products/'.strtolower( str_replace(" ", "-", $req->input("product_name"))) );

    			     echo 'Successfully inserted';
    		}

    	}else
    		return false;

    }

    private function check_image_size($size){
		
		if($size < 5*1024*1024)
			return true;
		else
			return false;

    }

    private function image_upload($file, $name){

    	$ext = $file->getClientOriginalExtension();
		
		//Move Uploaded File
		$destinationPath = 'images';
		if($file->move($destinationPath,$name.'.'.$ext))
			return true;
		else
			return false;

    }

    private function is_image_accepted_format($file_type){
		
		if($file_type == 'image/png' || $file_type == 'image/jpeg')
			return true;
		else
			return false;

    }

}
