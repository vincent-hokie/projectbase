<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\StockTrait;
use App\Http\Traits\NeedTrait;

use App\Http\Requests\ValidateFilterParams;
use App\Http\Requests\ValidateFarmerInterest;
use App\Http\Requests\ValidateBidRequest;


use App\User;
use App\Product;
use App\Need;
use App\Stock;
use App\Bid;

use DB;

class FarmerController extends Controller{

    use StockTrait;

    //class constructor, called each time a function in this file is used
    public function __construct(){
    
        //to use this file, user must be logged in
        $this->middleware('auth:farmer');

    }

    //this function returns whether the currently logged in user is a buyer, farmer or staff
    private function determine_role(){

        return User::getRole();
    }


    //this determines the participation of a user in a specific role
    private function determine_particular_role($role){
        
        if( User::getRole() == $role  )
            return 1;
        else
            return 0;

    }


    public function searchNeedForm(ValidateFilterParams $request){

        $pageData = $this->searchNeed(  $request  );
        return view(  "search_need_form", $pageData  );

    }


    public function searchNeed(ValidateFilterParams $request){

        $user = $this->determine_particular_role("farmer");

        $farmerDetails = Auth()->guard(  "farmer"  )->user();
        $prods = explode(",", $farmerDetails[  $this->farmer_products_plain  ]  );
        $products = Product::whereIn(  $this->product_id_plain, $prods  )->get();


        $stock = Stock::where(  $this->stock_valid, "=", "1"  )
                    ->where(  $this->stock_farmer_id, "=", $farmerDetails[  $this->farmer_id_plain  ] );

        $stock->join(  $this->product_table, $this->stock_product_id, "=", $this->product_id  );
        $stock = $stock->get();

        if($user == 1){

            $need = Need::where(  $this->need_valid, "=", "1");//->get();

            //get the farmer info corresponding to the user id
            $need->join(  $this->buyer_table, $this->buyer_id, '=', $this->need_buyer_id  );

            $theProduct = null;
            $startQuantity = null;
            $endQuantity = null;
            $startDate = null;
            $endDate = null;


            //search parameters where sent through get variables
            if( ($request->input(  $this->input_product_name  ) != "none" && $request->input(  $this->input_product_name  )) || 
                $request->input(  $this->input_startQuantity  ) || $request->input(  $this->input_endQuantity  ) || 
                $request->input(  $this->input_startDate  ) || $request->input(  $this->input_endDate  ) ) {

                //product was set
                if(  $request->input(  $this->input_product_name  ) != "none"  ){
                    $need = $need->where(  $this->need_product_id, "=", $request->input(  $this->input_product_name  ));
                    $theProduct = $request->input(  $this->input_product_name  );
                }
                
                //either lower or upper limit quantity was set
                if($request->input(  $this->input_startQuantity  ) !== null || $request->input(  $this->input_endQuantity  ) !== null) {

                    $lower = $request->input(  $this->input_startQuantity  ) !== null ? $request->input(  $this->input_startQuantity  ) : 0;
                    $upper = $request->input(  $this->input_endQuantity  ) !== null ? $request->input(  $this->input_endQuantity  ) : 0;

                    if((int)$lower != 0){
                        
                        $need = $need->where(  $this->need_quantity, '>=', $lower  );
                        $startQuantity = $lower;

                    }

                    if((int)$upper != 0){

                        $need = $need->where(  $this->need_quantity, '<=', $upper  );
                        $endQuantity = $upper;
                    }

                }

                //either lower or upper limit date was set
                if($request->input(  $this->input_startDate  ) !== null || $request->input(  $this->input_endDate  ) !== null) {

                    $lower = $request->input(  $this->input_startDate  ) !== null && strtotime($request->input(  $this->input_startDate  )) ? 
                                $request->input(  $this->input_startDate  ) : 0;
                    $upper = $request->input(  $this->input_endDate  ) !== null && strtotime($request->input(  $this->input_endDate  )) ? 
                                $request->input(  $this->input_endDate  ) : 0;

                    if((int)$lower != 0){

                        $need = $need->where(  $this->need_date_required, ">=", $lower  );
                        $startDate = $lower;

                    }

                    if((int)$upper != 0){

                        $need = $need->where(  $this->need_date_required, "<=", $upper  );
                        $endDate = $upper;

                    }

                }

            } //end if ( get variables are set )
            
            //get the product corresponding to the product ids
            $need->join(  $this->product_table, $this->need_product_id, '=', $this->product_id  );

            $pageData = ["role" => "farmer", "need" => $need->get(), "products" => $products, "theProduct" => $theProduct, 
                        "startQuantity" => $startQuantity, "endQuantity" => $endQuantity, "startDate" => $startDate,
                        "endDate" => $endDate, "stock" => $stock ];

            return $pageData;

        }

    }


    public function farmerInterest(ValidateFarmerInterest $request){  

        $user = Auth()->guard(  "farmer"  )->user();

        $bidModel = new Bid([
            $this->bid_stock_id_plain => $request->input('stock_id'),
            $this->bid_buyer_id_plain =>  $request->input('buyer_id'),
            $this->bid_need_id_plain =>  $request->input('need_id'),
            $this->bid_farmer_id_plain =>  $user[  $this->farmer_id_plain  ],
            $this->bid_quantity_plain => 0,
            $this->bid_unit_cost_plain => 0
        ]);

        if(  $bidModel->save()  )
            return 1;
        else
            return 0;

    }


    public function farmerBids(Request $request, $stock_id, $buyer_id){

        $bids = Bid::where(  $this->bid_stock_id, "=", $stock_id  )
                    ->where(  $this->bid_buyer_id, "=", $buyer_id  )
                    ->join(  $this->stock_table, $this->bid_stock_id, $this->stock_id  )
                    ->join(  $this->farmer_table, $this->stock_farmer_id, $this->farmer_id  )
                    ->get([
                        Bid::bid_table.".*",
                        $this->farmer_first_name,
                        $this->farmer_last_name
                        ]);
        
        $stock = Stock::where(  $this->stock_id_plain, "=", $stock_id  )->get();

        if( sizeof($stock) == 1  )
            $stock = $stock[0];
        else
            $stock = [];


        return view("bid_history", [  "role" => "farmer", "bids" => $bids, "stock" => $stock  ]);

    }

    public function farmerBidHistory(Request $request){

        $user = Auth()->guard(  "farmer"  )->user();

        $bids = DB::table(  $this->bid_table  )
                 ->select(  $this->bid_buyer_id, $this->bid_stock_id, $this->stock_maturity_date, $this->stock_unit_cost, $this->product_name, $this->buyer_first_name, $this->buyer_last_name, $this->stock_quantity_actual, $this->stock_quantity_expected, $this->buyer_verified, $this->buyer_email  )
                 ->groupBy(  $this->bid_buyer_id, $this->bid_stock_id, $this->stock_maturity_date, $this->stock_unit_cost, $this->product_name, $this->buyer_first_name, $this->buyer_last_name, $this->stock_quantity_actual, $this->stock_quantity_expected, $this->buyer_verified, $this->buyer_email   );
                 //->get();

        //$bids = Bid::where(  $this->bid_id, "<>",  null  );
        $bids->join(  $this->stock_table, $this->bid_stock_id, $this->stock_id  );
        $bids->join(  $this->buyer_table, $this->bid_buyer_id, $this->buyer_id  );
        $bids->join(  $this->product_table, $this->stock_product_id, $this->product_id  );
        $bids->where(  $this->stock_farmer_id, "=", $user[  $this->farmer_id_plain  ]  );
        
        return view("farmer_bid_history", [  "role" => "farmer", "bids" => $bids->get()  ]);

    }


    public function makeBidReload(ValidateBidRequest $request){  

        $user = Auth()->guard(  "farmer"  )->user();

        $bidModel = new Bid([
            $this->bid_stock_id_plain => $request->input('stock_id'),
            $this->bid_buyer_id_plain => $request->input('buyer_id'),
            $this->bid_farmer_id_plain => $user[  $this->farmer_id_plain  ],
            $this->bid_quantity_plain => $request->input('quantity'),
            $this->bid_unit_cost_plain => $request->input('unit_cost')
        ]);

        if(  $bidModel->save()  )
            return redirect('/dashboard/farmer/bid/converstion/'.$request->input('stock_id').'/'.$request->input('buyer_id') )->with(  "bid_status", "true"  );
        else
            return redirect('/dashboard/farmer/bid/converstion/'.$request->input('stock_id').'/'.$request->input('buyer_id') )->with(  "bid_status", "false"  );

    }


}
