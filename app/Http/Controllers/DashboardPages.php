<?php

namespace App\Http\Controllers;

//use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\File;

use App\Http\Traits\NeedTrait;
use App\Http\Traits\StockTrait;

use App\Http\Requests\ValidateUserInfo;
use App\Http\Requests\ValidateUserRegistration;
use App\Http\Requests\ValidateStock;
use App\Http\Requests\ValidateNewFarmer;
use App\Http\Requests\PasswordResetRequest;


use Illuminate\Foundation\Auth\RegistersUsers;

use DB;
use App\Product;
use App\Need;
use App\Buyer;
use App\Country;
use App\Staff;
use App\User;
use App\Farmer;
use App\Stock;

class DashboardPages extends Controller{
    
    use RegistersUsers, StockTrait;

    private $destinationPath = 'images/users';

    //class constructor, called each time a function in this file is used
    public function __construct(){
    
        //to use this file, user must be logged in
        $this->middleware('logged_in');

    }


    //this function returns whether the currently logged in user is a buyer, farmer or staff
    private function determine_role(){

        return User::getRole();
    }


    //this determines the participation of a user in a specific role
    private function determine_particular_role($role){
        
        if( User::getRole() == $role  )
            return 1;
        else
            return 0;

    }


    //used to display user dashboard
    public function index(){

        //get the user's role
        $role = $this->determine_role();

        //package it in an array
        $send = [ "role" => $role ];

        //if the user has a role, dispay his/her dashboard
    	return view('my_dashboard', $send );
    }


    /* 
    *	see my profile
    */
    public function profile(Request $request){

        //get the logged in user info
        $user = Auth()->guard(  User::getRole()  )->user();

        $nationality = null;
        if(  $user->nationality  ){
            $nationality = Country::find(  $user->nationality );
            $nationality = $nationality->country_name;
        }

        $countries = Country::orderBy("country_name")->get();

        //determine user role
        $role = $this->determine_role();

        //package information in an array
        $pageData = [  
            "role" => $role, 
            "user" => $user, 
            "nationality" => $nationality, 
            "profile" => TRUE, 
            "userRole" => $role,
            "countries" => $countries   ];


        //if the user is a farmer, grab the products they provide for display in the profile
        if(  $role == "farmer"  ){
            
            $prods = $user[  $this->farmer_products_plain  ];
            $prods = Product::whereIn(  $this->product_id_plain, explode(",", $prods)  )->get([ $this->product_name_plain ]);

            $pageData["farmer_products"] = $prods;

        }
        
        //send this info to our page where it will be used
    	return view('user_profile', $pageData);

    }



    /* 
    *	edit my profile
    */
    public function profileEdit(Request $request){

        //retrieve the information of the user who is logged in ie general info and nationality & countries for a drop down menu
        $user = $user = Auth()->guard(User::getRole())->user();
        $countries = Country::all();

        $nationality = null;
        
        if(  isset($user->nationality)  )
            $nationality = Country::find($user->nationality)->country_name;

        //determie the role of the user in the system
        $role = $this->determine_role();

        //send all this info to our page where it will be used
        $pageData = [  "role" => $role, "user" => $user, "countries" => $countries, "nationality" => $nationality, "userRole" => $role  ];
        
        //show the editable user profile
    	return view('user_profile_edit', $pageData);
    }


    /*
    *   update profile
    */
    //inject validation request so that whatever comes is first validated
    public function profileSave(ValidateUserInfo $request){

        //$update[  column_name ] = updated_value;
        $update["first_name"] = $request->input("first_name");
        $update["middle_name"] = $request->input("middle_name");
        $update["last_name"] = $request->input("last_name");
        $update["dob"] = $request->input("dob");
        $update["email"] = $request->input("email");
        /*$update["gender"] = $request->input("gender");*/

        if(  User::getRole() != "staff"  )
            $update["location"] = $request->input("location");
        
        $update["phone_no"] = $request->input("phone_no");

        if(  User::getRole() != "staff"  )
            $update[  User::getRole()."_profile"  ] = $request->input("bio");

        //if user wanted the existing photo removed
        if(  $request->input('picAction')  ){

            $user = Auth()->guard(  User::getRole()  )->user();
            $oldName = $user['photo'];

            if(  File::exists(  $this->destinationPath.'/'.$oldName  )  )
                if(  File::delete(  $this->destinationPath.'/'.$oldName  )  )
                    $update["photo"] = null;
                
        }

        //if a file was selected in the form, process it
        $file = null;
        if(  $request->file('image')  )
            $file = $request->file('image');

        if(  $file  ){

            $user = Auth()->guard(  User::getRole()  )->user();
            $name = sha1(  $user[  User::getRole().'_id'  ].User::getRole().strtotime("now")  );
            $oldName = null;
            if(  $user['photo']  )
                $oldName = $user['photo'];

            if(  $this->image_upload(  $file, $name, $oldName  )  )
                $update["photo"] = str_replace(" ", "-", $name).'.'.$file->getClientOriginalExtension();

        }

        //nationality can come from 2 seperate elements, we first check which was used
        $nationality = $request->input("nationality");
        if(  $nationality == "none" )
            $nationality = null;

        $update["nationality"] = $nationality;

        
        

        $model = false;

        if(  User::getRole() == "staff"  )
            $model = Staff::where( User::getRole().'_id', Auth()->guard(  User::getRole()  )->user()[  User::getRole().'_id'  ])->update($update)  ;
        else if(  User::getRole() == "farmer"  )
            $model = Farmer::where( User::getRole().'_id', Auth()->guard(  User::getRole()  )->user()[  User::getRole().'_id'  ])->update($update)  ;
        else
            $model = Buyer::where( User::getRole().'_id', Auth()->guard(  User::getRole()  )->user()[  User::getRole().'_id'  ])->update($update);

        //if the update works, show the user their updated profile. Otherwise take them back to the editable profile to apply their changes again
        if(  $model  )
            return redirect('/dashboard/profile');
        else
            return redirect('/dashboard/profile/edit');
    }

    
    public function searchStockForm(Request $request){

        $user = $this->determine_particular_role("buyer");

        $products = Product::all();

    	if($user == 1)
    		return view('search_stock', array("role" => "buyer", "products" => $products  ));
    	else
    		return redirect('/');

    }


    public function searchStock(Request $request){

        $user = $this->determine_particular_role("buyer");

        if($user == 1){

            $stock = Stock::where(  $this->stock_valid, "=", "1");//->get();

            //get the farmer info corresponding to the user id
            $stock->join(  $this->farmer_table, $this->farmer_id, '=', $this->stock_farmer_id  );

            $theProduct = null;
            $startQuantity = null;
            $endQuantity = null;
            $startDate = null;
            $endDate = null;

            //search parameters where sent through get variables
            if( ( $request->input('product_name') != "none" && $request->input('product_name') ) || 
                $request->input('start_quantity') || $request->input('end_quantity') || 
                $request->input('start_date') || $request->input('end_date') ) {

                //product was set
                if($request->input('product_name') != "none"){
                    $stock = $stock->where(  $this->stock_product_id, "=", $request->input('product_name'));
                    $theProduct = $request->input('product_name');
                }
                
                //either lower or upper limit quantity was set
                if($request->input('start_quantity') !== null || $request->input('end_quantity') !== null) {

                    $lower = $request->input('start_quantity') !== null ? $request->input('start_quantity') : 0;
                    $upper = $request->input('end_quantity') !== null ? $request->input('end_quantity') : 0;

                    if((int)$lower != 0){
                        $stock = $stock->where(function ($query) use ($lower) {

                                $query->where(function ($query) use ($lower) {
                                                $query->where(  $this->stock_quantity_expected, '>=', $lower)
                                                      ->where(  $this->stock_maturity_date, '>', date("Y-m-d"));
                                            });

                                $query->orWhere(function ($query) use ($lower) {
                                                $query->where(  $this->stock_quantity_actual, '>=', $lower)
                                                      ->where(  $this->stock_maturity_date, '<=', date("Y-m-d"));
                                            });

                            });

                        $startQuantity = $lower;
                    }

                    if((int)$upper != 0){

                        $stock = $stock->where(function ($query) use ($upper) {

                            $query->where(function ($query) use ($upper) {
                                            $query->where(  $this->stock_quantity_expected, '<=', $upper)
                                                  ->where(  $this->stock_maturity_date, '>', date("Y-m-d"));
                                        });

                            $query->orWhere(function ($query) use ($upper) {
                                            $query->where(  $this->stock_quantity_actual, '<=', $upper)
                                                  ->where(  $this->stock_maturity_date, '<=', date("Y-m-d"));
                                        });

                        });

                        $endQuantity = $upper;
                    }

                }

                //either lower or upper limit date was set
                if($request->input('start_date') !== null || $request->input('end_date') !== null) {

                    $lower = $request->input('start_date') !== null && strtotime($request->input('start_date')) ? 
                                $request->input('start_date') : 0;
                    $upper = $request->input('end_date') !== null && strtotime($request->input('end_date')) ? 
                                $request->input('end_date') : 0;

                    if((int)$lower != 0){
                        $stock = $stock->where(  $this->stock_maturity_date, ">=", $lower);
                        $startDate = $lower;
                    }

                    if((int)$upper != 0){
                        $stock = $stock->where(  $this->stock_maturity_date, "<=", $upper);
                        $endDate = $upper;
                    }

                }

            } //end if ( get variables are set )
            
            //get the product corresponding to the product ids
            $stock->join(  $this->product_table, $this->stock_product_id, '=', $this->product_id  );


            //get the land info corresponding to the land ids
            $stock->join(  $this->land_table, $this->stock_land_id, '=', $this->land_id  );

            $stock = $stock->get([
                $this->farmer_first_name,
                $this->farmer_last_name,
                $this->farmer_email,
                "$this->farmer_verified as fverified",
                "$this->product_name as pname",
                $this->land_table.".*",
                $this->stock_table.".*"
                ]);

            $products = Product::all();

            return ["role" => "buyer", "stock" => $stock, "products" => $products, "theProduct" => $theProduct, 
                        "startQuantity" => $startQuantity, "endQuantity" => $endQuantity, "startDate" => $startDate,
                        "endDate" => $endDate ];

        }

    }


    
    public function searchStock_table(Request $request){

        $stock = $this->searchStock($request);

        return view('search_stock', $stock );


    }




    public function searchStock_map(Request $request){

        $stock = $this->searchStock($request);

        return view('search_stock_map', $stock );
        
    }




    /* 
    *   see farmer profile
    */
    public function farmer_profile(Request $request, $email){

        //get the logged in user info
        $user = Farmer::where(  $this->farmer_email_plain  , "=", $email  )->get();
        
        $nationality = "";

        if(  sizeof($user) == 1  ){

            if(  $user[0]->nationality  )
                $nationality = Country::where(  $this->country_id_plain, "=", $user[0]->nationality  )->get([  $this->country_name_plain  ]);

            //determine user role
            $role = $this->determine_role();

            //package information in an array
            $pageData = ["role" => $role, "user" => $user[0], "nationality" => $nationality, "profile" => false, "userRole" => "farmer"  ];

            
            //if the user is a farmer, grab the products they provide for display in the profile
            $prods = $user[0][  $this->farmer_products_plain  ];
            $prods = Product::whereIn(  $this->product_id_plain, explode(",", $prods)  )->get([ $this->product_name_plain ]);

            $pageData["farmer_products"] = $prods;

            $products = Product::all();
            $pageData["products"] = $products;

            //send this info to our page where it will be used
            return view('user_profile', $pageData); 


        }else{

        }
   
    }



    /* 
    *   see buyer profile
    */
    public function buyer_profile(Request $request, $email){

        //get the logged in user info
        $user = Buyer::where(  $this->buyer_email_plain  , "=", $email  )->get();
        
        $nationality = "";

        if(  sizeof($user) == 1  ){

            if(  $user[0]->nationality  )
                $nationality = Country::where(  $this->country_id_plain, "=", $user[0]->nationality  )->get([  $this->country_name_plain  ]);

            //determine user role
            $role = $this->determine_role();

            //package information in an array
            $pageData = ["role" => $role, "user" => $user[0], "nationality" => $nationality, "profile" => false, "userRole" => "buyer"  ];

            //send this info to our page where it will be used
            return view('user_profile', $pageData); 


        }else{

        }

        
    }


    private function image_upload($file, $name, $oldName = null){

        $ext = $file->getClientOriginalExtension();

        //delete the image that exists from the db first before updating
        $extensions = ['jpeg', 'png', 'bmp', 'gif', 'svg'];


        if(  $oldName  )
            if(  File::exists(  $this->destinationPath.'/'.$oldName  )  )
                File::delete(  $this->destinationPath.'/'.$oldName  );


        //Move Uploaded File
        if($file->move($this->destinationPath,$name.'.'.$ext))
            return true;
        else
            return false;

    }


    public function changePassword(PasswordResetRequest $request, $role, $email){

        if(  $email == Auth()->guard(  User::getRole()  )->user()["email"]  ||  Staff::isSystemAdmin()  ){

            $model = false;
            $update[  "password"  ] = bcrypt(  $request->input('password')  );

            if(  $role == "staff"  )
                $model = Staff::where(  $this->staff_email_plain, $email  )->update($update);
            else if (    $role == "buyer"    )
                $model = Buyer::where(  $this->buyer_email_plain, $email  )->update($update);
            else if (  $role == "farmer"  )
                $model = Farmer::where(  $this->farmer_email_plain, $email  )->update($update);
            
            $status = "false";

            if(  $model  )
                $status = "true";

            if(  $email == Auth()->guard(  User::getRole()  )->user()["email"]  )
                return redirect(  "/dashboard/profile"  )->with(  "changePasswordStatus", $status  );
            else
                return redirect(  "/dashboard/".$role."/".$email  )->with(  "changePasswordStatus", $status  );

        }else
            return redirect("/")->with("403", "You don't have the authority to perform the requested action!!");

    }


}
