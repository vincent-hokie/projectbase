<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Need extends Model{
    
    protected $table = "need";

    const need_table = "need";

    protected $primaryKey = 'need_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'quantity', 'proposed_price', 'date_required', 'description', 'buyer_id'
    ];


    //use date_ordered rather than "created_at"
    //setCreatedAt("date_ordered");


}
