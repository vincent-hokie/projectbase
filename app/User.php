<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    
    protected $table = "buyer";

    protected $primaryKey = 'buyer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /*'name', */'email', 'password', 'first_name', 'last_name',
        'middle_name', 'dob', 'gender', 'nationality', 'location', 'bio'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected function getRole(){

        if(  !Auth()->guard( "staff" )->check()  &&  !Auth()->guard( "buyer" )->check()  &&  !Auth()->guard( "farmer" )->check()  ){
            \Session::forget("role");
            return null;
        }

        if( ! \Session::has("role")  ){

            if(  Auth()->guard( "staff" )->check()  )
                \Session::put("role", "staff"  );
            else if(  Auth()->guard( "buyer" )->check()  )
                \Session::put("role", "buyer"  );
            else if(  Auth()->guard( "farmer" )->check()  )
                \Session::put("role", "farmer"  );

        }

        return \Session::get("role");
    }    


}
