<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Auth;

class Buyer extends Authenticatable
{
    
    use Notifiable;
    
	protected $table = "buyer";

    protected $primaryKey = 'buyer_id';

    const buyer_table = "buyer";
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /*'name', */'email', 'password', 'first_name', 'last_name',
        'middle_name', 'dob', 'gender', 'nationality', 'location', 'bio'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    



}
