<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model{
    
    protected $table = "bid";

    const bid_table = "bid";

    protected $primaryKey = 'bid_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [  'farmer_id', 'quantity', 'unit_cost', 'buyer_id', 'stock_id' ];

}
