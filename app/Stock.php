<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model{
    
    protected $table = "stock";
    
    const stock_table = "stock";

    protected $primaryKey = 'stock_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'farmer_id', 'product_id', 'maturity_date', 'quantity_actual', 'quantity_expected', 'description', 'land_id', 'unit_cost'
    ];

    

}
