<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Land extends Model{
    
    protected $table = "land";

    const land_table = "land";

    protected $primaryKey = 'land_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'farmer_id', 'name', 'location', 'size', 'city', 'city_location'
    ];


}
