<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Auth;

//use Illuminate\Database\Eloquent\Model;

class Farmer extends Authenticatable{
    
    use Notifiable;
    
    protected $table = "farmer";

    const farmer_table = "farmer";
    
    protected $primaryKey = 'farmer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /*'name', */'email', 'password', 'first_name', 'last_name',
        'middle_name', 'dob', 'gender', 'nationality', 'location', 'bio', 'phone_no', 'products', 'staff_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    



}
