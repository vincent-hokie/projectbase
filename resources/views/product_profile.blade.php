<!doctype html>
<html>
<head>
    
    <title>Item</title>

    @include('templates.header')

    <link type="text/css" rel="stylesheet" href="/agri/css/template.css" />
    <link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

    <script src="/agri/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/agri/js/advanced-form-components.js"></script>

</head>
<style>

	.farmers{ margin-top:10px; cursor:pointer }
	.farmers p { background-color: #eee; padding: 5px; margin: 1px 0 0 0 }

  .product_info{height: 100px; background-color: rgba(0,0,0,0.5); margin-left: 20px; margin-top: 0.5%; border-radius: 20px; display: none}
  .product_info p{ color: #fff; margin: 0; position: relative; text-align: center; padding: 15px;}

    .showFilterFormNoJs { display: block; }
    .hideFilterFormJs {  position: absolute; top: 80px; }

    body{ transition:1s; overflow-x:hidden; }
    #swipeme{ transition:1s; }

    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 0;
    }

    nav{  background-color: rgba(0,0,0,0.6) !important; margin-top: -50px; }

    i.fa.fa-leaf{ color: green }

</style>
<body>
    
    <div class="jumbotron" style="background-image: url(/agri/images/products/{{ $product->photo }}); background-size: contain; margin-bottom: 0">
      <div class="container text-center" style="color: #fff; background-color: rgba(0,0,0,0.1);">
        <h1>{{ $product->name }}</h1>
        <p><strong>Average unit cost: UgX {{ $average }}</strong></p>
      </div>
    </div>

		@include('fragments.dashboardNavbar')
        
        @include('templates.login_modal')

        <div class="alert alert-success bid" style="margin-top:10px; display:none" id="bidSuccess">
            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your bid was successfully made!
        </div>

        <div class="alert alert-danger bid" style="margin-top:10px; display:none" id="bidErrorGeneral">
            <strong><span class="glyphicon glyphicon-thumbs-down"></span></strong>Your bid was not made successfully. Please try again.
        </div>

        <div class="alert alert-danger bid" style="margin-top:10px; display:none" id="bidError">
            <strong><span class="glyphicon glyphicon-thumbs-down"></span></strong>
            Your bid was not made. Something went wrong with these:<br>
            <span class="error"></span>
            Please try again.
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="swipeme">
        	
            <!-- if the user is logged in, show his dash here as well -->
                <!-- dashboard menu -->
                    @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
                        <aside class="col-lg-2 col-md-3 col-sm-4 col-xs-6 off-canvas-left" style="margin-top:30px; background: #2a3542;" id="swipeme-left">
                            
                            <div id="sidebar" class="nav-collapse">
                                <!-- sidebar menu start-->
                                <ul class="sidebar-menu" id="nav-accordion">
                                    
                                    <li>
                                        <a href="/agri/dashboard">
                                            <i class="fa fa-dashboard"></i>
                                            <span>Dashboard</span>
                                        </a>
                                    </li>


                                    @include('menus.'.$role)

                                </ul>
                                <!-- sidebar menu end-->
                            </div>

                        </aside>
                    @endif
                <!-- dashboard menu end -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 main-content" id="swipeme-main">

                @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">

                        <button class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding:0" id="dashboard_show">
                            <i class="fa fa-dashboard"></i>
                        </button>

                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">

                            <a href="/agri/product_profile/{{ strtolower($product->name) }}/map" class="btn btn-info col-lg-1 col-lg-offset-5 col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-2  col-xs-offset-4" role="button" style="padding:0">
                                <i class="fa fa-map-marker"></i>
                            </a>

                            <a href="/agri/product_profile/{{ strtolower($product->name) }}" class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" role="button" style="padding:0">
                                <i class="fa fa-table"></i>
                            </a>

                        </div>

                        <button class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding:0" id="filter_show">
                            <span class="glyphicon glyphicon-filter"></span>
                        </button>

                    </div>

                @else

                    <div class="alert alert-info" style="margin-top:10px">
                        <strong><span class="glyphicon glyphicon-info-sign"></span></strong> Register and sign in to have access to farmer profiles and future stock information!
                    </div>

                @endif

                <!-- page center -->




                @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )

                    <!--dynamic table-->
                    <link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
                    <link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
                    <link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

                    <link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


                    <!--main content start-->
                    <div class="col-sm-12">

                        <section class="panel">
                            
                            <div class="panel-body">

                                <div class="adv-table">

                                    <table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

                                        <thead>
                                            <tr id="none">
                                                <th></th>
                                                <th>Farmer</th>
                                                <th>Location</th>
                                                <th>Cost</th>
                                                <th>Maturity Date</th>
                                                <th>Quantity</th>
                                                @if(  $role == "buyer"  )
                                                    <th></th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                @else

                    <table class="table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:5px">

                        <tr id="none">
                            <th></th>
                            <th>Farmer</th>
                            <th>Location</th>
                            <th>Cost</th>
                            <th>Maturity Date</th>
                            <th>Quantity</th>
                        </tr>

                @endif

                    

                    <style type="text/css">

                        .table tr td { padding:0 !important }
                        .table tr td a { text-decoration: none; color: #000 }
                        .table tr td a:hover { text-decoration: none }
                        .table tr td a p { padding:8px; margin:0 }

                    </style>

                    @for(  $i = 0; $i < sizeof($stock); $i++  )
                        
                        @if($stock[$i]->quantity_actual == 0 && 
                            !(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  ) )
                            @continue
                        @endif

                        <tr id='{{  $stock[$i]->id  }}'>
                                
                        <td>
                            <p>{{  ($i+1)  }}</p>
                        </td>
                        <td>
                            <a class='pushUrl' href='/agri/dashboard/farmer/{{  str_replace(" ", "-", strtolower($stock[$i]->email))  }}'>
                                <p>

                                @if(  $stock[$i]->verified == "1"  )
                                    <i class='fa fa-leaf' style='margin-right:10px'></i>
                                @endif

                                {{  $stock[$i]->first_name  }} {{  $stock[$i]->last_name  }}</p>
                            </a>
                        </td>
                        <td>
                            <p>{{  $stock[$i]->city  }}</p>
                        </td>
                        <td>
                            <p>{{  $stock[$i]->unit_cost  }}</p>
                        </td>
                        <td>
                            <p>{{  $stock[$i]->maturity_date  }}</p>
                        </td>
                        <td>
                            @if(  $stock[$i]->quantity_actual == 0  )
                                <p>{{  $stock[$i]->quantity_expected  }}</p>
                            @else
                                <p>{{  $stock[$i]->quantity_actual  }}</p>
                            @endif

                        @if(  $role == "buyer"  )
                            <td>
                                <button type='button' data-placement='top' data-html='true' class='btn btn-success' data-toggle='popover' title='Bid Form' 
                                data-content='
                                
                                <form action="#" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 makeBidForm" role="form" id="{{  $stock[$i]->stock_id  }}">
    
                                    <!-- Laravel Requirement -->
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                                    <input type="hidden" name="stock_id" value="{{  $stock[$i]->stock_id  }}" />

                                    <div style="margin:10px auto" class="form-group">
                                        <label for="unit_cost">Quantity</label>
                                        <input type="number" class="form-control" name="quantity" required min=0 max=
                                            @if($stock[$i]->quantity_actual == 0)
                                                {{  $stock[$i]->quantity_expected  }}
                                            @else
                                                {{  $stock[$i]->quantity_actual  }}
                                            @endif
                                        >
                                    </div>

                                    <div style="margin:10px auto" class="form-group">
                                        <label for="unit_cost">Unit Cost</label>
                                        <input type="number" class="form-control" id="unit_cost" required name="unit_cost" min={{  $stock[$i]->unit_cost  }}>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Make Bid</button>

                                </form>'

                                data-original-title="Popover Header">Bid</button>
                            </td>

                        @endif

                        </tr>

                    @endfor


                    @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )

                                        </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th>Farmer</th>
                                                    <th>Location</th>
                                                    <th>Cost</th>
                                                    <th>Maturity Date</th>
                                                    <th>Quantity</th>
                                                    @if(  $role == "buyer"  )
                                                        <th></th>
                                                    @endif
                                                </tr>
                                            </tfoot>

                                        </table>

                                    </div>

                                </div>

                            </section>

                        </div>


                        <!-- js placed at the end of the document so the pages load faster -->
                        <script type="text/javascript" language="javascript" 
                        src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
                        <script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

                        <!--dynamic table initialization -->
                        <script src="/agri/js/dynamic_table_init.js"></script>
                    
                    @else

                        </table>

                    @endif

            </div>


            <div style="margin-top:10px; padding:0" class="col-lg-3 col-md-3 col-sm-4 col-xs-6 off-canvas-right" id="swipeme-right">

                <div style="padding: 10px; background-color: rgb(74, 69, 69); color: #fff; margin-bottom:0" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="glyphicon glyphicon-filter"></span> Filter</div>
                <div id="demo" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0 0 20px 0">
                    
                    <form action="/agri/product_profile/{{ strtolower($product->name) }}" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" role="form" enctype="multipart/form-data">
                
                         <!-- Laravel Requirement -->
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                        <div style="margin:10px auto" class="form-group">
                            <label for="unit_cost">Quantity</label>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                    <label for="start_quantity">Between</label>
                                    <input type="number" class="form-control" name="start_quantity" min=0 
                                        @if(  isset($startQuantity) && $startQuantity  ) 
                                            value="{{  $startQuantity  }}";
                                        @endif
                                        id="start_quantity" >
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                    <label for="end_quantity">And</label>
                                    <input type="number" class="form-control" name="end_quantity" min=0
                                        @if(  isset($endQuantity) && $endQuantity  )
                                            value="{{  $endQuantity  }}";
                                        @endif
                                        id="end_quantity" >
                                </div>
                            </div>

                            @if (  $errors->has('start_quantity') || $errors->has('end_quantity'))
                                <span class="help-block">
                                    <strong>These values must be numbers</strong>
                                </span>
                            @endif
                            
                        </div>

                        @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
                            <div style="margin:10px auto" class="form-group">
                                <label for="unit_cost">Maturity Date</label>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                        <label for="start_date">Between</label>
                                        <input type="text" class="form-control input-medium default-date-picker" name="start_date" id="start_date"
                                            @if($startDate)
                                                value='{{  $startDate  }}'
                                            @endif
                                            >
                                    </div>
                                    
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                        <label for="end_date">And</label>
                                        <input type="text" class="form-control input-medium default-date-picker" name="end_date" id="end_date"
                                            @if($endDate) 
                                                value='{{  $endDate  }}'
                                            @endif
                                        >
                                    </div>
                                </div>

                                @if (  $errors->has('end_date') || $errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>We need these to be valid dates</strong>
                                    </span>
                                @endif

                            </div>
                        @endif


                        <button type="submit" class="btn btn-primary btn-sm col-lg-6 col-md-7 col-sm-9 col-xs-10" data-dismiss="modal">Filter Stock</button>

                    </form>

                </div>
            </div>

        </div>

    
    @include('templates.js_links')

    <!-- allow us have the fancy dashboard on the left -->
    <script class="include" type="text/javascript" src="/agri/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/agri/js/jquery.scrollTo.min.js"></script>

    <!-- custom script (should be removed or edited) -->
    <script src="/agri/js/common-scripts.js"></script>

    @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
        <script type="text/javascript">
            $(document).ready(function(){
                $("aside").css("margin-left", -$("aside").width()-50  );
            });
        </script>
    @endif

    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on("click", "#dashboard_show", function(){
                $("#swipeme").css("margin-left", $("aside").width()+50);
            });

            $(document).on("click", "#filter_show", function(){
                $("#swipeme").css("margin-left", 0);
            });

            $('[data-toggle="popover"]').popover();   

        $(document).on("submit", "form.makeBidForm", function(){

            var data = $(this).serialize();

            $.ajax({
              type: "POST",
              url: "/agri/dashboard/make/bid",
              data: data,
              datatype: "JSON",
              success: function(data){
                
                $(".bid.alert").hide(500);

                if( data == 1)
                    $("#bidSuccess").show(500);
                else if(  data == 0  )
                    $("#bidErrorGeneral").show(500);

                $("[data-toggle='popover']").popover('hide');

              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {

                $(".bid.alert").hide(500);
                var returned = JSON.parse(  XMLHttpRequest.responseText  );
                
                var msgSpan = $("#bidError .error");

                if(  returned["quantity"]  ){
                    msgSpan.append("Quantity: "+returned["quantity"][0]+"<br>");
                }

                if(  returned["unit_cost"]  )
                    msgSpan.append("Unit Cost: "+returned["unit_cost"][0]+"<br>");

                if(  returned["stock_id"]  )
                    msgSpan.append("Stock: The stock identifier isn't properly set!<br>");

                if(  msgSpan.html() != ""  )
                    $("#bidError").show(500);
                else
                    $("#bidErrorGeneral").show(500);

                $("[data-toggle='popover']").popover('hide');

              }
            });

            return false;

        });


        });

    </script>


</body>
</html> 