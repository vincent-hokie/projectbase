@extends('templates.dashboard')

@section('menus')
    @include('menus.'.$role)
@endsection

@section('content')
    @include('fragments.search_stock_map')
@endsection
