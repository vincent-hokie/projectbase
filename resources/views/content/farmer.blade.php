<div class="tab-content">
	
	<link href="/agri/assets/morris.js-0.4.3/morris.css" rel="stylesheet" />

	@if(  session("name")  )

	<div class="alert alert-success" style="margin-top:10px">
		<strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> {{  session("name")  }}, you have successfully logged in. Add your stock, make buyers aware of your future stock and browse demand to plan.
	</div>

	@endif

	<div id="home" class="tab-pane fade in active">
		<h3>Welcome to your dashboard {{ Auth()->guard("farmer")->user()->first_name }}</h3>
		<p>All your actions can be started from the menu on the left.<br>Feel free to leave us a comment to let us know what you're happy or unhappy about by clicking <strong>Feedback</strong>.</p>
	</div>

	<!--state overview start-->
	<div class="row state-overview">
		<div class="col-lg-3 col-sm-6">
			<section class="panel">
				<div class="symbol terques">
					<i class="fa fa-user"></i>
				</div>
				<div class="value">
					<h1 class="count">
						0
					</h1>
					<p>New Users</p>
				</div>
			</section>
		</div>
		<div class="col-lg-3 col-sm-6">
			<section class="panel">
				<div class="symbol red">
					<i class="fa fa-tags"></i>
				</div>
				<div class="value">
					<h1 class=" count2">
						0
					</h1>
					<p>Sales</p>
				</div>
			</section>
		</div>
		<div class="col-lg-3 col-sm-6">
			<section class="panel">
				<div class="symbol yellow">
					<i class="fa fa-shopping-cart"></i>
				</div>
				<div class="value">
					<h1 class=" count3">
						0
					</h1>
					<p>New Order</p>
				</div>
			</section>
		</div>
		<div class="col-lg-3 col-sm-6">
			<section class="panel">
				<div class="symbol blue">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="value">
					<h1 class=" count4">
						0
					</h1>
					<p>Total Profit</p>
				</div>
			</section>
		</div>
	</div>
	<!--state overview end-->

	<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading">
				Top 5 crops in stock
			</header>
			<div class="panel-body">
				<div id="hero-donut" class="graph"></div>
			</div>
		</section>
	</div>

	<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading">
				Deals made in the past year (2016)
			</header>
			<div class="panel-body">
				<div id="hero-bar" class="graph"></div>
			</div>
		</section>
	</div>

	<script src="/agri/assets/morris.js-0.4.3/morris.min.js" type="text/javascript"></script>
	<script src="/agri/assets/morris.js-0.4.3/raphael-min.js" type="text/javascript"></script>

	<!-- script for this page only-->
	<script src="/agri/js/morris-script3.js"></script>

</div>