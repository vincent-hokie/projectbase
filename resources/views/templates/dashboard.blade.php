<!doctype html>
<html>

    <head>
        
        <title>Dashboard</title>
        <link type="text/css" rel="stylesheet" href="/agri/css/template.css" />
        <link type="text/css" rel="stylesheet" href="/agri/css/dashboard.css" />
        
        @include('templates.header')

        <script src="/agri/js/jquery-1.12.4.min.js"></script>

    </head>

<body>

    @include('fragments.dashboardNavbar')

    <!-- body -->
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">

            <!-- dashboard menu -->
                <aside class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                    
                    <div id="sidebar" class="nav-collapse">
                        <!-- sidebar menu start-->
                        <ul class="sidebar-menu" id="nav-accordion">
                            
                            <li>
                                <a class="active" href="/agri/dashboard">
                                    <i class="fa fa-dashboard"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>


                            @yield('menus')

                        </ul>
                        <!-- sidebar menu end-->
                    </div>

                </aside>
            <!-- dashboard menu end -->
            
            <!-- main content start -->
                <main style="margin-bottom:10px" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    @yield('content')
                </main>
            <!-- main content end -->
      
        </div>
    <!-- body -->
    
    <!-- Modal --
    <div class="modal fade" id="deleteProduct" role="dialog">
        <div class="modal-dialog">
    
        <!-- Modal content--
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Delete Product</h4>
            </div>
            
            <div class="modal-body">
                <p>Are you sure you want to delete this product?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" style="float:left">Yes, Delete it</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" style="float:left">No, Thanks</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
        </div>
    </div>

    <!-- Laravel Requirement -->
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

    @include('fragments.footer')


    <!-- load scripts at the end so that the pages load faster -->
    @include('templates.js_links')

        <script src="/agri/js/dashboard.js"></script>
            
        <!-- allow us have the fancy dashboard on the left -->
        <script class="include" type="text/javascript" src="/agri/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="/agri/js/jquery.scrollTo.min.js"></script>


        <!-- custom script (should be removed or edited) -->
        <script src="/agri/js/common-scripts.js"></script>

        


</body>
</html>
