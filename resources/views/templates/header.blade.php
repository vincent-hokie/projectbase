
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="/agri/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="/agri/css/vex.css" />
<link type="text/css" rel="stylesheet" href="/agri/css/vex-theme-os.css" />

<!-- dowmloaded css -->
<link href="/agri/css/style.css" rel="stylesheet" />

<!-- package resposible for icons and fonts -->
<link href="/agri/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--    bootstrap switch styling    -->
<link href="/agri/css/bootstrap-switch.min.css" rel="stylesheet" />

<style type="text/css">
	span.help-block { color: #a94442; }
	i.fa.fa-leaf{ color: green }
</style>