<!-- custom notification scripts -->
    <script src="/agri/js/jquery.mobile.custom.min.js"></script>
    <script src="/agri/js/bootstrap.min.js"></script>

    <!-- used to create animated switches -->
    <script src="/agri/js/bootstrap-switch.min.js"></script>

    <script src="/agri/js/vex.combined.min.js"></script>
    <script>vex.defaultOptions.className = 'vex-theme-os'</script>
    
	<!-- change the appearance of the scrolling and scroll bars -->
    <script src="/agri/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/agri/js/slidebars.min.js"></script>

	<script type="text/javascript">

		$(document).ready(function(){

			$("input[type='checkbox']").bootstrapSwitch();

			//warn user before logging out
	        $(document).on("click", "#logoutButton", function(){

	            vex.dialog.defaultOptions.showCloseButton = true;
	            vex.dialog.defaultOptions.escapeButtonCloses = true;
	            vex.dialog.defaultOptions.overlayClosesOnClick = true;

	            vex.dialog.buttons.YES.text = 'Yes'
	            vex.dialog.buttons.NO.text = 'No, thank you!'

	            vex.dialog.confirm({
	                message: 'Are you sure you want to log out?',
	                callback: function (value) {

	                    if(value == true)
	                        window.location = "/agri/logout";
	                    
	                }
	            });
	            
	        });

		});
	</script>