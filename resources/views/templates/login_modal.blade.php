<!-- Login Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Login</h4>
			</div>
			<form action="/agri/login" method="post">

				<!-- Laravel Requirement -->
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

				<input type="text" class="form-control" id="usr" name="email" required placeholder="E-Mail" style="width:90%; margin:10px auto">
				<input type="password" class="form-control" id="pwd" name="password" required placeholder="Password" style="width:90%; margin:10px auto">
				<button type="submit" class="btn btn-primary btn-lg" style="width:50%; margin:10px 25%">Login</button>
			</form>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>