<!doctype html>
<html>

    <head>
    
        @include('templates.header')

        <script src="/agri/js/jquery-1.12.4.min.js"></script>
        
        <link type="text/css" rel="stylesheet" href="/agri/css/template.css" />

    </head>

    <body>
    	
        @include('fragments.dashboardNavbar')
            
        <div class="container">

            <div style="width:30%; margin:auto; margin-bottom:40px">
                <form action="/agri/search" method="get">

                    <span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
                    <span>Enter the product here</span>
                    <input type="text" class="form-control" name="search" required placeholder="Product search word" size="30" style="width:90%; display:inline-block; margin-bottom:20px"  value="{{ $parameter }}" />

                </form>
            </div>

            @if ($parameter)
                <h3>Search results for: "{{ $parameter }}" </h3>
            @else
                <h3>Enter what you are looking for above</h3>
            @endif

            @if (sizeof($result) > 0)
                @foreach($result as $product)
                    <a href="/agri/product_profile/{{ strtolower(str_replace(' ', '-', $product->name)) }}" id='{{ $product->id }}' class="col-lg-2 col-md-2 col-sm-3 col-xs-4 well well-default" style="border: 2px solid #fff">
                        <img class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dope-menu resting_menu thumbnail" src="/agri/images/products/{{ $product->photo }}" />
                        <p>{{ $product->name }}</p>
                    </a>
                @endforeach
            @else
                <h4>We haven't found anything matching "{{ $parameter }}"</h4>
            @endif

        </div>

        <!-- Login Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Login</h4>
                  </div>
                  <form action="/agri/login" method="post">

                    <!-- Laravel Requirement -->
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                    <input type="text" class="form-control" id="usr" name="email" required placeholder="E-Mail" style="width:90%; margin:10px auto">
                    <input type="password" class="form-control" id="pwd" name="password" required placeholder="Password" style="width:90%; margin:10px auto">
                    <button type="submit" class="btn btn-primary btn-lg" style="width:50%; margin:10px 25%">Login</button>
                  </form>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>

        @include('templates.js_links')

    </body>

</html>