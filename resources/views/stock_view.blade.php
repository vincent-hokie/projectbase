@extends('templates.dashboard')

@section('menus')
    @include('menus.'.$role)
@endsection

@section('content')
    @include('fragments.stock_view')
@endsection
