@extends('templates.dashboard')

@section('menus')
    @include('menus.'.$role)
@endsection

@section('content')
    @include('fragments.farmer_reg')
@endsection
