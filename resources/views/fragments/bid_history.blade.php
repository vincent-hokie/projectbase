
<style type="text/css">
    .timeline-desk .panel { background: #fff; }
    .timeline:before { background-color: #000; }
    section.panel{  background-color: rgba(0,0,0,0); max-height: 500px; overflow: hidden; overflow-y: scroll;  }
</style>


    <div class="col-lg-8">

        <div class="text-center mbot30">
            <h3 class="timeline-title" style="color:#000">Timeline</h3>
            <p class="t-info" style="color:#000">Stock bidding timeline</p>
            <p class="t-info" style="color:#000">FARMER -- BUYER</p>
        </div>
        
        <?php $dayOfTheWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]; ?>

        <!--timeline start-->
        <section class="panel">
            <div class="panel-body">

                <div class="timeline">

                    @foreach(  $bids as $bid  )
                       
                       @if(  !$bid->farmer_id  )
                            
                            <article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon red"></span>
                                            <span class="timeline-date"> 
                                                {{  date("h:i a",  strtotime(    $bid->created_at    )  )  }}
                                            </span>
                                            <h1 class="red"> 
                                                {{  date( "d  F y",  strtotime(    $bid->created_at    )  )  }} |
                                                {{  $dayOfTheWeek[ date(  "w",  strtotime(    $bid->created_at    )  ) ]  }}
                                            </h1>
                                            <p>{{  $bid->quantity  }} at Ugx {{  $bid->unit_cost  }} each</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                        @else
                            
                            @if(  !$bid->unit_cost && !$bid->quantity  )
                                
                                <article class="timeline-item alt">
                                    <div class="timeline-desk">
                                        <div class="panel">
                                            <div class="panel-body">
                                                <span class="arrow-alt"></span>
                                                <span class="timeline-icon green"></span>
                                                <span class="timeline-date">
                                                    {{  date("h:i a",  strtotime(    $bid->created_at    )  )  }} 
                                                </span>
                                                <h1 class="green">
                                                    {{  date( "d  F y",  strtotime(    $bid->created_at    )  )  }} | 
                                                    {{  $dayOfTheWeek[ date(  "w",  strtotime(    $bid->created_at    )  ) ]  }}
                                                </h1>
                                                <p> {{ $bid->first_name  }} {{ $bid->last_name  }} showed interest in satisfying a need </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                            @else

                                <article class="timeline-item alt">
                                    <div class="timeline-desk">
                                        <div class="panel">
                                            <div class="panel-body">
                                                <span class="arrow-alt"></span>
                                                <span class="timeline-icon green"></span>
                                                <span class="timeline-date">
                                                    {{  date("h:i a",  strtotime(    $bid->created_at    )  )  }} 
                                                </span>
                                                <h1 class="green">
                                                    {{  date( "d  F y",  strtotime(    $bid->created_at    )  )  }} | 
                                                    {{  $dayOfTheWeek[ date(  "w",  strtotime(    $bid->created_at    )  ) ]  }}
                                                </h1>
                                                <p>{{  $bid->quantity  }} at Ugx {{  $bid->unit_cost  }} each</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>

                            @endif

                        @endif

                    @endforeach
                </div>

                <div class="clearfix">&nbsp;</div>
            </div>
        </section>
    <!--timeline end-->
</div>
    
    <form 
        
        @if(  $role == "buyer"  )
            action="/agri/buyer/make/bid"
        @elseif(  $role == "farmer"  )
            action="/agri/farmer/make/bid"
        @endif
     method="post" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 makeBidForm" role="form" id="{{  $stock->stock_id  }}">
    
    <h5 style="margin-bottom:20px"><strong>Bid/ Counter bid form</strong></h5>

    <!-- Laravel Requirement -->
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

    <input type="hidden" name="buyer_id" value="{{  $bid->buyer_id  }}" />

    <input type="hidden" name="stock_id" value="{{  $stock->stock_id  }}" />

    @if(  $role == "buyer"  )
        <input type="hidden" name="bid" value="1" />
    @elseif(  $role == "farmer"  )
        <input type="hidden" name="counterbid" value="1" />
    @endif

    <div style="margin:10px auto" class="form-group">
        <label for="unit_cost">Quantity</label>
        <input type="number" class="form-control" name="quantity" required min=0 max=
            @if($stock->quantity_actual == 0)
                {{  $stock->quantity_expected  }}
            @else
                {{  $stock->quantity_actual  }}
            @endif
        >
    </div>

    <div style="margin:10px auto" class="form-group">
        <label for="unit_cost">Unit Cost</label>
        <input type="number" class="form-control" id="unit_cost" required name="unit_cost" min={{  $stock->unit_cost  }}>
    </div>

    @if(  $role == "buyer"  )
        <button type="submit" class="btn btn-primary">Make Bid</button>
    @elseif(  $role == "farmer"  )
        <button type="submit" class="btn btn-primary">Make Counter Bid</button>
    @endif


</form>
