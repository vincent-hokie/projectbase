<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<h4 style="text-align:center">Register new staff here!</h4>
<h6 style="text-align:center">
	All staff registered are added with general privileges, a provision to provide system privileges will be provided after successful registration
</h6>

<form role="form" action="/agri/save/staff" method="post" style="width:70%; margin:auto" class="form-horizontal">

	<!-- Laravel Requirement -->
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

	<!-- Names of the user input -->
	<div style="width:100%; margin:auto">
		<div style="width:30%; margin:10px auto; display:inline-block">
			<input type="text" class="form-control" name="first_name" id="first_name" required placeholder="First Name" value="{{ old('first_name') }}">

			@if ($errors->has('first_name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('first_name') }}</strong>
	            </span>
	        @endif

		</div>

		<div style="width:30%; margin:10px auto; display:inline-block">
			<input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="{{ old('middle_name') }}">

			@if ($errors->has('middle_name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('middle_name') }}</strong>
	            </span>
	        @endif

		</div>

		<div style="width:30%; margin:10px auto; display:inline-block">
			<input type="text" class="form-control" name="last_name" id="last_name" required placeholder="Last Name" value="{{ old('last_name') }}">

			@if ($errors->has('last_name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('last_name') }}</strong>
	            </span>
	        @endif

		</div>
	</div>
	 
	<!-- E-mail input -->
	<div class="form-group">
		<label for="email">Email address:</label>
		<input type="email" class="form-control" id="email" name="email" required placeholder="username@gmail.com" style="width:90%; margin:10px auto" value="{{ old('email') }}">

		@if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        

	</div>

	<!-- Password input -->
	<div class="form-group">
		<label for="password_1">Password:</label>
		<input type="password" class="form-control" id="password_1" name="password" required placeholder="Password" style="width:90%; margin:10px auto">

		@if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        

	</div>

	<!-- Password re-enter input -->
	<div class="form-group">
		<label class="sr-only" for="password_2">Re-Enter Password:</label>
		<input type="password" class="form-control" id="password_2" name="password_confirmation" required placeholder="Re-Enter Your Password" style="width:90%; margin:10px auto">

		@if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
        

	</div>


	<!-- Date Of Birth input -->
    <div class="form-group">
        <label for="dob_date">Date of Birth (mm/dd/yyy):</label>
        <input type="text" class="form-control input-medium default-date-picker" name="dob_date" id="dob_date" style="width:90%; margin:10px auto" value="{{ old('dob_date') }}">
    </div>


    <!-- Nationality input -->
    <div class="form-group">
        <label for="nationality">Nationality:</label>
        <select name="nationality" class="form-control" style="width:90%; margin:10px auto">
    	
        	<option value="none"></option>
            @foreach (  $countries as $country  )
                <option value='{{  $country->id  }}'>{{  $country->country_name  }}</option>
            @endforeach

      </select>
    </div>


    <!-- phone contact input -->
	<div class="form-group">
		<label for="phone_contact">Phone Contact:</label>
		<input type="text" class="form-control" id="phone_contact" name="phone_contact" placeholder="256777098900" style="width:90%; margin:10px auto" value="{{ old('phone_contact') }}">

		@if ($errors->has('phone_contact'))
            <span class="help-block">
                <strong>{{ $errors->first('phone_contact') }}</strong>
            </span>
        @endif
        

	</div>
    

	<!-- Location input -->
    <div class="form-group">

        <label for="Location">Location:</label>
        <input type="text" class="form-control" id="Location" name="location" placeholder="e.g. Kampala" style="width:90%; margin:10px auto" value="{{ old('location') }}">

        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif

    </div>
    
	           
	<button type="submit" class="btn btn-primary" data-dismiss="modal" style="width:30%; margin:10px 35%">Register!</button>

</form>


<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
