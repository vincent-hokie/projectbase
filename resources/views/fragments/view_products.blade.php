

	<!--dynamic table-->
	<link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


<!--main content start-->
<div class="col-sm-12">

	<section class="panel">
		
		<div class="panel-body">

			<div class="adv-table">

				<table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

					<thead>
						<tr id="none">
							<th></th>
							<th>Product Name</th>
							<th>Type</th>
							<th>Visits</th>
						</tr>
					</thead>
					<tbody>

					<style type="text/css">

						.table tr td { padding:0 !important }
						.table tr td a { text-decoration: none; color: #000 }
						.table tr td a:hover { text-decoration: none }
						.table tr td a p { padding:8px; margin:0 }
					</style>
					<?php

						for($i = 0; $i < sizeof($Product); $i++){

							echo "<tr id='".$Product[$i]->id."'>
									";
							echo "<td>
									<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
										<p>".($i+1)."</p>
									</a>
								</td>";
							echo "<td>
								<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
									<p>".$Product[$i]->name."</p>
								</a>
							</td>";
							echo "<td>
								<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
									<p>".$Product[$i]->type."</p>
								</a>
							</td>";
							echo "<td>
								<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
									<p>".$Product[$i]->visits."</p>
								</a>
							</td>";
							echo "</tr>";

						}

					?>
					</tbody>
					<tfoot>
						<tr id="none">
							<th></th>
							<th>Product Name</th>
							<th>Type</th>
							<th>Visits</th>
						</tr>
					</tfoot>

				</table>

			</div>

		</div>

	</section>

</div>


	<!-- js placed at the end of the document so the pages load faster -->
	<script type="text/javascript" language="javascript" src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

	<!--dynamic table initialization -->
	<script src="/agri/js/dynamic_table_init.js"></script>

	<!--custom switch-->
  	<script src="/agri/js/bootstrap-switch.js"></script>
  	

</table>