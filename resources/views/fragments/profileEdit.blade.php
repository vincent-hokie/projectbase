<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

@if(  session("changePasswordStatus")  )
    
    @if(  session("changePasswordStatus") == "true"  )
        
        <div class="alert alert-success" style="margin-top:10px">
            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your password change was successful!
        </div>

    @else

    	<div class="alert alert-danger" style="margin-top:10px">
            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your password change was unsuccessful. Please try again
        </div>

    @endif

@endif

<h3>My Profile</h3>

<form role="form" action="/agri/profile/save" method="post" enctype="multipart/form-data">

	<!-- Laravel Requirement -->
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

	<div class="col-lg-5 col-md-5 col-sm-6 col-xs-8 panel panel-default" style="margin-bottom:20px; padding-top:20px; padding-bottom:20px">
		
		@if( !$user->photo || $user->photo == "")
			<img src="/agri/images/users/unknown.png" class="img-thumbnail" alt="User Profile Photo" style="width:95%" width="95%">
		@else
			<img src="/agri/images/users/{{  $user->photo  }}" class="img-thumbnail" alt="User Profile Photo" style="width:95%" width="95%">
		@endif
		
		<div class="btn btn-info col-lg-3 col-md-4 col-sm-5 col-xs-6" data-toggle="collapse" data-target="#demo" style="margin-top:10px">Edit Photo</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 collapse" id="demo">
			
			<div class="form-group" style="margin-top:20px">
				<label>Select an image to upload(.jpg, .jpeg, .png)</label>
				<input type="file" class="form-control" id="crop_img" name="image" >
			</div>

			<div class="form-group">
				<label>or just remove the existing one</label>
				<br><label><input type="radio" name="picAction" style="margin-right:20px" value="remove"

					@if( !$user->photo )
						disabled=""
					@endif

				>Remove Photo</label>
			</div>

		</div>


		<button class="btn btn-info btn-lg save_profile" style="clear:both; float:left">
			<span class="glyphicon glyphicon-save"></span> Save Profile
		</button>

	</div>
					  
	<div class="col-lg-5 col-md-7 col-sm-11 col-xs-11">

		<div class="well well-sm form-group">
			<label class="label label-default">First Name</label>
			<input class="form-control" type="text" name="first_name" required value='{{ $user->first_name }}'>

			@if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif

		</div>
							
		<div class="well well-sm form-group">
			<label class="label label-default">Middle Name</label>
			<input class="form-control" type="text" name="middle_name" value='{{ $user->middle_name }}'>

			@if ($errors->has('middle_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('middle_name') }}</strong>
                </span>
            @endif

		</div>
							
		<div class="well well-sm form-group">
			<label class="label label-default">Last Name</label>
			<input class="form-control" type="text" name="last_name" required value='{{ $user->last_name }}'>

			@if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif

		</div>

		<div class="well well-sm form-group">
			<label class="label label-default">Date Of Birth (mm/dd/yyyy) </label>
			<input class="form-control input-medium default-date-picker" type="text" name="dob" id="dob" value='{{ $user->dob }}' required >

			@if ($errors->has('dob'))
                <span class="help-block">
                    <strong>{{ $errors->first('dob') }}</strong>
                </span>
            @endif

		</div>

		<div class="well well-sm form-group">
			<label class="label label-default">Nationality</label>
			<select name="nationality" class="form-control">
	            	
            	<option value="none"></option>
                @foreach (  $countries as $country  ) 
                    @if(  $user->nationality && $user->nationality == $country->id  )
                    	<option value='{{  $country->id  }}' selected>{{  $country->country_name  }}</option>
                    @else
                    	<option value='{{  $country->id  }}'>{{  $country->country_name  }}</option>
                    @endif

                @endforeach

          </select>
		</div>

	</div>


	<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">

		<!--
		<div class="well well-sm form-group">
			<label class="label label-default">Gender</label><br>

			<label class="radio-inline" style="background-color:rgba(0,0,0,0)"><input type="radio" 
				<?php if($user->gender == "M") echo "checked"; ?> 
				name="gender" value="M">Male</label>
			<label class="radio-inline" style="background-color:rgba(0,0,0,0)"><input type="radio" 
				<?php if($user->gender == "F") echo "checked"; ?>
				name="gender" value="F">Female</label>

				@if ($errors->has('gender'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('gender') }}</strong>
	                </span>
	            @endif

		</div>
		-->

		<div class="well well-sm form-group">
			<label class="label label-default">Location</label>
			<input class="form-control" type="text" name="location" value='{{ $user->location }}'>

			@if ($errors->has('location'))
                <span class="help-block">
                    <strong>{{ $errors->first('location') }}</strong>
                </span>
            @endif

		</div>

		<div class="well well-sm form-group">
			<label class="label label-default">E-Mail</label>
			<input class="form-control" type="email" name="email" value='{{ $user->email }}'>

			@if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

		</div>

		<div class="well well-sm">
			<span class="label label-default">Phone Contact</span>
			<input class="form-control" type="text" name="phone_no" value='{{ $user->phone_no }}'>

			@if ($errors->has('phone_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone_no') }}</strong>
                </span>
            @endif
		</div>

	</div>

	<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
		<div class="well well-sm form-group">
			<label class="label label-default">Bio</label>
			<textarea class="form-control" name="bio" rows="5" id="comment"> {{ $user[  $role.'_profile'  ] }} </textarea>

			@if ($errors->has('bio'))
                <span class="help-block">
                    <strong>{{ $errors->first('bio') }}</strong>
                </span>
            @endif
            
		</div>
	</div>

</form>


<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
