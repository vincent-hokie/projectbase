<?php
	
	use App\Product;
	use App\Application;

	$applications = Application::all()->sortByDesc('created_at');

	$Product = Product::all();

?>

<table class="table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12">

	<tr id="none">
		<th></th>
		<th>Product Name</th>
		<th>Unit Cost</th>
	</tr>
	<style type="text/css">

		.table tr td { padding:0 !important }
		.table tr td a { text-decoration: none; color: #000 }
		.table tr td a:hover { text-decoration: none }
		.table tr td a p { padding:8px; margin:0 }
	</style>
	<?php

		for($i = 0; $i < sizeof($Product); $i++){

			echo "<tr id='".$Product[$i]->id."'>
					";
			echo "<td>
					<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
						<p>".($i+1)."</p>
					</a>
				</td>";
			echo "<td>
				<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
					<p>".$Product[$i]->name."</p>
				</a>
			</td>";
			echo "<td>
				<a class='pushUrl' href='/agri/dashboard/products/".str_replace(" ", "-", strtolower($Product[$i]->name))."'>
					<p>".$Product[$i]->unit_cost."</p>
				</a>
			</td>";
			echo "</tr>";

		}

	?>

</table>