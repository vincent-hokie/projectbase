<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <form role="form" action="/agri/update_need/{{$need->need_id}}" method="post">

        <!-- Laravel Requirement -->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

        <style type="text/css">
            #needEdit span.glyphicon{ font-size: 25px; cursor: pointer; padding: 10px }
            #needEdit span.glyphicon:hover{ background-color: #eee }
            #needEdit .well.well-sm.form-group{ background-color: rbga(0,0,0,0) }
            #needEdit .well.well-sm.form-group p{ margin-top:10px; margin-left:20px }
        </style>

    	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-10">

    		<div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Product Name</label>
                <select class="form-control" name="product_name" id="product_name" required>

                    @foreach($products as $product)
                        @if($product->product_id == $need->product_id)
                            <option value='{{ $product->product_id }}' selected> {{  $product->name  }}</option>
                        @else
                            <option value='{{ $product->product_id }}'> {{  $product->name  }}</option>
                        @endif
                    @endforeach

                </select>

                @if ($errors->has('product_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_name') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <span class="label label-default">Date Required</span>
                <input class="form-control input-medium default-date-picker" type="text" required name="date_required" id="date_required" value='{{ $need->date_required }}'>

                @if ($errors->has('date_required'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_required') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Needed quantity</label>
            	<input class="form-control" name="quantity" required type="text" value='{{ $need->quantity }}'>

                @if ($errors->has('quantity'))
                    <span class="help-block">
                        <strong>{{ $errors->first('quantity') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Proposed unit cost</label>
            	<input class="form-control" type="text" name="proposed_price" required value='{{ $need->proposed_price }}'>

                @if ($errors->has('proposed_price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('proposed_price') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Description</label>
            	<textarea class="form-control" rows="3" name="description">{{ $need->description }}</textarea>

                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif

            </div>

    	</div>

        <div class="col-lg-4 col-md-4 col-sm-7 col-xs-10">

            <button class="btn btn-default" type="submit">
                <span class="glyphicon glyphicon-save" title="Save Product"></span>
                Save Need
            </button>
            
        </div>

    </form>

</div>

<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
