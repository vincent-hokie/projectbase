<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<h4 style="text-align:center">Search for current and future demand! PLAN.</h4>

<div class="alert alert-success bid" style="margin-top:10px; display:none" id="bidSuccess">
    <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your interest was successfully shown!
</div>

<div class="alert alert-danger bid" style="margin-top:10px; display:none" id="bidErrorGeneral">
    <strong><span class="glyphicon glyphicon-thumbs-down"></span></strong>Your interest was not made successfully. Please try again.
</div>

<div class="alert alert-danger bid" style="margin-top:10px; display:none" id="bidError">
    <strong><span class="glyphicon glyphicon-thumbs-down"></span></strong>
    Your interest was not shown. Something went wrong with these:<br>
    <span class="error"></span>
    Please try again.
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="swipeme">
        
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 main-content" id="swipeme-main">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">

            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">

                @if(  false  )
                <a href="/agri/dashboard/search/stock/map" class="btn btn-info col-lg-1 col-lg-offset-5 col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-2  col-xs-offset-4" role="button" style="padding:0">
                    <i class="fa fa-map-marker"></i>
                </a>

                <a href="/agri/dashboard/search/stock" class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" role="button" style="padding:0">
                    <i class="fa fa-table"></i>
                </a>
                @endif

            </div>

            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>

        </div>

        <!-- page center -->


        <!--dynamic table-->
        <link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

        <link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


        <!--main content start-->
        <div class="col-sm-12">

            <section class="panel">
                
                <div class="panel-body">

                    <div class="adv-table">

                        <table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

                            <thead>
                                <tr id="none">
                                    <th></th>
                                    <th>Buyer</th>
                                    <th>Product</th>
                                    <th>Cost</th>
                                    <th>Date Required</th>
                                    <th>Quantity</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
            

                        <style type="text/css">

                            .table tr td { padding:0 !important }
                            .table tr td a { text-decoration: none; color: #000 }
                            .table tr td a:hover { text-decoration: none }
                            .table tr td a p { padding:8px; margin:0 }

                        </style>

                        @if(  isset(  $need  )  )
                            @for(  $i = 0; $i < sizeof($need); $i++  )

                                <tr id='{{  $need[$i]->need_id  }}'>
                                <td>
                                    <p>{{ ($i+1) }}</p>
                                </td>
                                <td>
                                    <a class='pushUrl' href='/agri/dashboard/buyer/{{  str_replace(" ", "-", strtolower($need[$i]->email)) }}'>

                                        <p>
                                            @if(  $need[$i]->verified == "1"  )
                                                <i class='fa fa-leaf' style='margin-right:10px; color: green;'></i>
                                            @endif

                                            {{  $need[$i]->first_name  }} {{  $need[$i]->last_name  }}
                                        </p>

                                    </a>
                                </td>
                                <td>
                                    <p>{{  $need[$i]->name  }}</p>
                                </td>
                                <td>
                                    <p>{{  $need[$i]->proposed_price  }}</p>
                                </td>
                                <td>
                                    <p>{{  $need[$i]->date_required  }}</p>
                                </td>
                                <td>
                                    <p>{{  $need[$i]->quantity  }}</p>
                                </td>
                                <td>

                                    <button type="button" data-placement="top" data-html="true" class="btn btn-success" data-toggle="popover" title="Bid Form" 
                                    data-content='
                                    
                                    <form action="#" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 makeBidForm" role="form" id="{{  $need[$i]->need_id  }}">
        
                                        <!-- Laravel Requirement -->
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                                        <input type="hidden" name="buyer_id" value="{{  $need[$i]->buyer_id  }}" />

                                        <input type="hidden" name="need_id" value="{{  $need[$i]->need_id  }}" />

                                        <div style="margin:10px auto" class="form-group">
                                            <label for="stock_id">The stock you are offering</label>
                                            <select class="form-control" name="stock_id" id="stock_id" required>
                                                
                                                @if(  isset(  $stock  )  )
                                                    @for(  $v = 0; $v < sizeof(  $stock  ); $v++  )
                                                        <option value="{{  $stock[$v]->stock_id  }}">
                                                            {{  $stock[$v]->name  }} ready on {{  $stock[$v]->maturity_date  }}
                                                        </option>
                                                    @endfor
                                                @endif

                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>

                                    </form>'

                                    data-original-title="Popover Header">Show interest</button>

                                </td>

                                </tr>


                            @endfor

                        @endif


                        </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Buyer</th>
                                    <th>Product</th>
                                    <th>Cost</th>
                                    <th>Date Required</th>
                                    <th>Quantity</th>
                                    <th></th>
                                </tr>
                            </tfoot>

                        </table>

                    </div>

                </div>

            </section>

        </div>

            <!-- js placed at the end of the document so the pages load faster -->
            <script type="text/javascript" language="javascript" 
            src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
            <script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

            <!--dynamic table initialization -->
            <script src="/agri/js/dynamic_table_init.js"></script>

        </div>


    <div style="margin-top:10px; padding:0" class="col-lg-3 col-md-3 col-sm-4 col-xs-6 off-canvas-right" id="swipeme-right">

        <div style="padding: 10px; background-color: rgb(74, 69, 69); color: #fff; margin-bottom:0" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <span class="glyphicon glyphicon-filter"></span> Filter</div>
        <div id="demo" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0 0 20px 0">
            
            <form action="/agri/dashboard/search/need" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" role="form" id="searchStockForm" enctype="multipart/form-data">
        
                 <!-- Laravel Requirement -->
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                <div style="margin:10px auto" class="form-group">
                    <label for="product_name">Product</label>
                    <select class="form-control" name="product_name" id="product_name" required>
                        <option value="none"></option>
            
                        @for(  $i = 0; $i <sizeof($products); $i++  )
                            @if( (  isset($theProduct) && $theProduct == $products[$i]->product_id  )  || old('product_name') == $products[$i]->product_id  )
                                <option value='{{  $products[$i]->product_id  }}' selected>{{  $products[$i]->name  }}</option>
                            @else
                                <option value='{{  $products[$i]->product_id  }}'>{{  $products[$i]->name  }}</option>
                            @endif
                        @endfor

                    </select>
                </div>

                <div style="margin:10px auto" class="form-group">
                    <label for="unit_cost">Quantity</label>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="start_quantity">Between</label>
                            <input type="number" class="form-control" name="start_quantity"  min=0
                                <?php if(isset($startQuantity) && $startQuantity) echo "value=".$startQuantity; ?> 
                                id="start_quantity" value="{{ old('start_quantity') }}" >
                        </div>
                        
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="end_quantity">And</label>
                            <input type="number" class="form-control" min=0 name="end_quantity" 
                                <?php if(  isset($endQuantity) && $endQuantity  ) echo "value=".$endQuantity; ?> 
                                id="end_quantity" value="{{ old('end_quantity') }}" >
                        </div>
                    </div>
                    
                </div>

                <div style="margin:10px auto" class="form-group">
                    <label for="unit_cost">Date Required</label>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="start_date">Between</label>
                            <input type="text" class="form-control input-medium default-date-picker" name="start_date" 
                                <?php if(  isset($startDate) && $startDate  ) echo "value=".$startDate; ?> 
                                id="start_date" value="{{ old('start_date') }}" >
                        </div>
                        
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="end_date">And</label>
                            <input type="text" class="form-control input-medium default-date-picker" name="end_date" 
                                <?php if(  isset($endDate) && $endDate  ) echo "value=".$endDate; ?> 
                                id="end_date" value="{{ old('end_date') }}" >
                        </div>
                    </div>

                </div>

                <button type="submit" class="btn btn-primary btn" data-dismiss="modal">Filter Need</button>

            </form>

        </div>
    </div>

</div>

<script>

    $(document).ready(function(){
        
        $('[data-toggle="popover"]').popover();   

        $(document).on("submit", "form.makeBidForm", function(){

            var data = $(this).serialize();

            $.ajax({
              type: "POST",
              url: "/agri/dashboard/farmer/make/bid",
              data: data,
              datatype: "JSON",
              success: function(data){
                
                $(".bid.alert").hide(500);

                if( data == 1)
                    $("#bidSuccess").show(500);
                else if(  data == 0  )
                    $("#bidErrorGeneral").show(500);

                $("[data-toggle='popover']").popover('hide');

              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {

                $(".bid.alert").hide(500);
                var returned = JSON.parse(  XMLHttpRequest.responseText  );
                
                var msgSpan = $("#bidError .error");

                if(  returned["quantity"]  ){
                    msgSpan.append("Quantity: "+returned["quantity"][0]+"<br>");
                }

                if(  returned["unit_cost"]  )
                    msgSpan.append("Unit Cost: "+returned["unit_cost"][0]+"<br>");

                if(  returned["stock_id"]  )
                    msgSpan.append("Stock: The stock identifier isn't properly set!<br>");

                if(  msgSpan.html() != ""  )
                    $("#bidError").show(500);
                else
                    $("#bidErrorGeneral").show(500);

                $("[data-toggle='popover']").popover('hide');

              }
            });

            return false;
        });

    });
</script>


<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
