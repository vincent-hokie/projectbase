<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="/agri/css/leaflet.css" />

<script src="/agri/js/jquery-1.12.4.min.js"></script>
<script src="/agri/js/jquery-ui.min.js"></script>


<script type="text/javascript">

    $(document).ready(function(){

        $(document).on("click", "form#searchStockForm button", function(){

            var fd = $("form#searchStockForm").serialize();
            console.log(  fd, $("form#searchStockForm")[0]  );

            $.ajax({
                type:'POST',
                url:'/agri/dashboard/search/stock',
                data: fd,
                success:function(data){

                    for (var i = 0; i < data.length; i++)
                        $("tableOfSearchedStock").append(" \
                            <tr id='"+$stock[i]->id+"' class='showStock'> \
                                <td> \
                                        <a class='pushUrl' href='/agri/dashboard/stock/"+$stock[i]->id+"'> \
                                            <p>"+(i+1)+"</p> \
                                        </a> \
                                    </td> \
                                <td> \
                                    <a class='pushUrl' href='/agri/dashboard/products/"+$stock[i]->id+"'> \
                                        <p>"+$product[$stock[i]->product_id]->name+"</p> \
                                    </a> \
                                </td> \
                                <td> \
                                    <a class='pushUrl' href='/agri/dashboard/products/"+$stock[i]->id+"'> \
                                         \
                                            if($stock[i]->quantity_actual == 0) \
                                                <p>"+$stock[i]->quantity_expected+"</p><span>( expected )</span> \
                                            else \
                                                <p>"+$stock[i]->quantity_actual+"</p> \
                                    </a> \
                                </td> \
                                <td> \
                                    <a class='pushUrl' href='/agri/dashboard/products/"+$stock[i]->id+"'> \
                                        <p>"+$product[$stock[i]->product_id]->unit_cost+"</p> \
                                    </a> \
                                </td> \
                                <td> \
                                    <a class='pushUrl' href='/agri/dashboard/products/"+$stock[i]->id+"'> \
                                            \
                                            <p>"+date("Y-m-d", strtotime($stock[i]->maturity_date))+"</p> \
                                            $diff = strtotime('now') - strtotime($stock[i]->maturity_date); \
                                            \
                                            if($diff < 0) \
                                                <span>( expected )</span> \
                                            else \
                                                <span></span> \
                                    </a> \
                                </td> \
                            </tr>");

                    
                }
            });

        });

    });

</script>


<h4 style="text-align:center">Search for stock you need now or in the future!</h4>
        
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="swipeme">
        
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 main-content" id="swipeme-main">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">

            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">

                <a href="/agri/dashboard/search/stock/map" class="btn btn-info col-lg-1 col-lg-offset-5 col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-2  col-xs-offset-4" role="button" style="padding:0">
                    <i class="fa fa-map-marker"></i>
                </a>

                <a href="/agri/dashboard/search/stock" class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" role="button" style="padding:0">
                    <i class="fa fa-table"></i>
                </a>

            </div>

            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"></div>

        </div>


        <?php

            $array = [];

            for (  $i = 0; $i < sizeof($stock); $i++) {

                $currentStock = [];
                $latLong = explode(",", $stock[$i]['location']  );
                $currentStock["lat"] = $latLong[0];
                $currentStock["lng"] = $latLong[1];
                $currentStock["name"] = $stock[$i]['city'];

                array_push(  $array, $currentStock  );

            }

            echo "<script> var markers = ".json_encode(  $array  )."; </script>";

        ?>

        <div id="map" style="height: 440px; border: 1px solid #AAA; top:30px" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>

        

    </div>


    <div style="margin-top:10px; padding:0" class="col-lg-3 col-md-3 col-sm-4 col-xs-6 off-canvas-right" id="swipeme-right">

        <div style="padding: 10px; background-color: rgb(74, 69, 69); color: #fff; margin-bottom:0" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <span class="glyphicon glyphicon-filter"></span> Filter</div>
        <div id="demo" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0 0 20px 0">
            
            <form action="/agri/dashboard/search/stock/map" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" role="form" id="searchStockForm" enctype="multipart/form-data">
        
                 <!-- Laravel Requirement -->
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                <div style="margin:10px auto" class="form-group">
                    <label for="product_name">Product</label>
                    <select class="form-control" name="product_name" id="product_name" required>
                        <option value="none"></option>

                        @for(  $i = 0; $i <sizeof($products); $i++  )
                            @if( (  isset($theProduct) && $theProduct == $products[$i]->product_id  ) || old('start_quantity') == $products[$i]->product_id  )
                                <option value='{{  $products[$i]->product_id  }}' selected>{{  $products[$i]->name  }}</option>
                            @else
                                <option value='{{  $products[$i]->product_id  }}'>{{  $products[$i]->name  }}</option>
                            @endif
                        @endfor

                    </select>
                </div>

                <div style="margin:10px auto" class="form-group">
                    <label for="unit_cost">Quantity</label>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="start_quantity">Between</label>
                            <input type="number" class="form-control" name="start_quantity"  min=0
                                <?php if(isset($startQuantity) && $startQuantity) echo "value=".$startQuantity; ?> 
                                id="start_quantity" value="{{ old('start_quantity') }}"  >
                        </div>
                        
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="end_quantity">And</label>
                            <input type="number" class="form-control" min=0 name="end_quantity" 
                                <?php if(  isset($endQuantity) && $endQuantity  ) echo "value=".$endQuantity; ?> 
                                id="end_quantity" value="{{ old('end_quantity') }}"  >
                        </div>
                    </div>
                    
                </div>

                <div style="margin:10px auto" class="form-group">
                    <label for="unit_cost">Maturity Date</label>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="start_date">Between</label>
                            <input type="text" class="form-control input-medium default-date-picker" name="start_date" 
                                <?php if(  isset($startDate) && $startDate  ) echo "value=".$startDate; ?> 
                                id="start_date" value="{{ old('start_date') }}"  >
                        </div>
                        
                        <div class="form-group col-lg-10 col-md-10 col-sm-6 col-xs-8">
                            <label for="end_date">And</label>
                            <input type="text" class="form-control input-medium default-date-picker" name="end_date" 
                                <?php if(  isset($endDate) && $endDate  ) echo "value=".$endDate; ?> 
                                id="end_date" value="{{ old('end_date') }}"  >
                        </div>
                    </div>

                </div>

                <button type="submit" class="btn btn-primary btn" data-dismiss="modal">Filter Stock</button>

            </form>

        </div>
    </div>

</div>


<!-- leaflet script -->
<script type='text/javascript' src='/agri/js/leaflet/leaflet.js'></script>

<!-- map testing scripts -->
<script type='text/javascript' src='/agri/maps/markers.json'></script>
<script type='text/javascript' src='/agri/maps/leaf-demo.js'></script>

<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
