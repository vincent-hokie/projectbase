<div class="row">

	@if(  session("changePasswordStatus")  )
        
        @if(  session("changePasswordStatus") == "true"  )
	        
	        <div class="alert alert-success" style="margin-top:10px">
	            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your password change was successful!
	        </div>

	    @else

	    	<div class="alert alert-danger" style="margin-top:10px">
	            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your password change was unsuccessful. Please try again
	        </div>

	    @endif

    @endif

    @if(  session("productUpdateStatus")  )
        
        @if(  session("productUpdateStatus") == "true"  )
	        
	        <div class="alert alert-success" style="margin-top:10px">
	            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> The products have been successfully updated!
	        </div>

	    @else

	    	<div class="alert alert-danger" style="margin-top:10px">
	            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> The product update was unsuccessful. Please try again
	        </div>

	    @endif

    @endif


	<div class="col-lg-5 col-md-5 col-sm-6 col-xs-8" style="margin-bottom:20px">

		@if(  !$user->photo ||  $user->photo == ""  )
			<img src="/agri/images/users/unknown.png" class="img-thumbnail" alt="User Profile Photo" style="width:95%">
		@else
			<img src="/agri/images/users/{{  $user->photo  }}" class="img-thumbnail" alt="User Profile Photo" style="width:95%">
		@endif

		@if(  $profile  ==  $role  )
			<div href="#profileEdit">
				<a href="/agri/dashboard/profile/edit" class="pushUrl btn btn-info btn-lg edit_profile">
					<span class="glyphicon glyphicon-edit"></span> Edit Profile
				</a>
			</div>
		@endif

	</div>

	<div class="col-lg-5 col-md-7 col-sm-11 col-xs-11">
		<div class="well well-sm">
			<span class="label label-default">First Name</span>
			<p>{{ $user->first_name }}<p/>
		</div>

		<div class="well well-sm">
			<span class="label label-default">Middle Name</span>
			<p>{{ $user->middle_name }}</p>
		</div>

		<div class="well well-sm">
			<span class="label label-default">Last Name</span>
			<p>{{ $user->last_name }}</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
		<div class="well well-sm">
			<span class="label label-default">Date Of Birth</span>
			<p>{{ date('M d Y', strtotime( $user->dob )) }}</p>
		</div>
		
		<div class="well well-sm">
			<span class="label label-default">Nationality</span>
			<p>{{  $nationality  }}</p>
		</div>

		<!--
			<div class="well well-sm">
				<span class="label label-default">Gender</span>
				@if(  $user->gender == "M")
					<p>Male</p>
				@elseif(  $user->gender == "F")
					<p>Female</p>
				@endif
			</div>
		-->

		<div class="well well-sm">
			<span class="label label-default">Location</span>
			<p>{{ $user->location }}</p>
		</div>

		<div class="well well-sm">
			<span class="label label-default">E-Mail</span>
			<p>{{ $user->email }}</p>
		</div>

		<div class="well well-sm">
			<span class="label label-default">Phone Contact</span>
			<p>{{ $user->phone_no }}</p>
		</div>

	</div>

	@if(  $userRole == "farmer"  )
		<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
			<div class="well well-sm">
				<span class="label label-default">Products</span>
				<p>
					@for(  $i = 0; $i < sizeof(  $farmer_products  ); $i++  )

						@if(  $i > 0 && $i != sizeof(  $farmer_products  )  )
						, 
						@endif

						{{  $farmer_products[  $i  ]["name"]  }}

					@endfor
				</p>
			</div>
		</div>
	@endif

	@if(  $role != "staff"  )
		<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
			<div class="well well-sm">
				<span class="label label-default">{{  $userRole  }} profile</span>
				<p>{{ $user[  $userRole.'_profile'  ] }}</p>
			</div>
		</div>
	@endif

	@if( (  $role == "buyer"  && Auth()->guard(  "buyer"  )->user()->buyer_id == $user[  "buyer_id"  ] ) ||
		  (  $role == "staff"  &&  
		  			( Auth()->guard(  "staff"  )->user()->staff_id == $user[  "staff_id"  ]  || 
		  			  Auth()->guard(  "staff"  )->user()->role == 1 
		  			)  
		   ) ||
		  (  $role == "farmer"  && Auth()->guard(  "farmer"  )->user()->farmer_id == $user[  "farmer_id"  ] )
	)

		<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">

			<div class="btn btn-default" data-toggle="collapse" data-target="#demo" style="margin-top:10px">Change Password</div>

			<form action="/agri/{{  $userRole  }}/changePassword/{{  $user->email  }}" method="post" class="collapse" enctype="multipart/form-data" id="demo" style="margin:10px auto">
        
		         <!-- Laravel Requirement -->
		    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

		         <!-- Password input -->
	            <div class="form-group">
	                <label for="password_1">Password:</label>
	                <input type="password" class="form-control" id="password_1" name="password" required placeholder="Password">

	                @if ($errors->has('password'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('password') }}</strong>
		                </span>
		            @endif

	            </div>

	            <!-- Password re-enter input -->
	            <div class="form-group">
	                <label class="sr-only" for="password_2">Re-Enter Password:</label>
	                <input type="password" class="form-control" id="password_2" name="password_confirmation" required placeholder="Re-Enter Your Password" style="margin:10px auto">

	                @if ($errors->has('password_confirmation'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('password_confirmation') }}</strong>
		                </span>
		            @endif

	            </div>

		        <button type="submit" class="btn btn-primary">Change Password</button>

		    </form>

		</div>

	@endif

	@if(  $role == "staff" && Auth()->guard(  "staff"  )->user()->role == 1  &&  $userRole == "farmer"  )

		<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
			<a href="/agri/dashboard/farmer/{{  $user->email  }}/land" class="btn btn-default" role="button" style="margin-top:20px">Make changes to land</a>
		</div>

		<div class="col-lg-5 col-md-5 col-sm-7 col-xs-8">
			
			<div class="btn btn-default" data-toggle="collapse" data-target="#productsForm" style="margin-top:20px">Make changes farmer products</div>

			<form action="/agri/farmer/updateProducts/{{  $user->email  }}" method="post" class="collapse" enctype="multipart/form-data" id="productsForm" style="margin:10px auto">
	    
		         <!-- Laravel Requirement -->
		    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

		    	<div class="form-group">
					<label for="products">Farmer Products:</label>
			          
			          <select name="products[]" class="form-control" multiple="multiple" id="products" size=10 >

			          	<?php  $arrayProd = explode(  ",", $user["products"]  ); ?>
			            @foreach(  $products as $product  )

			            	@if(  array_search(  $product->product_id, $arrayProd  )  )
			              		<option value="{{  $product->product_id  }}" selected>  {{  $product->name }}  </option>
			              	@else
			              		<option value="{{  $product->product_id  }}">  {{  $product->name }}  </option>
			              	@endif

			            @endforeach

			          </select>

			          <script type="text/javascript">

			          	$(document).ready(function(){

			          		$(document).on("click", "select#products option", function () {

							    if(  $("select#products option:selected"  ).length > 5  )
							    	$(this).removeAttr("selected");

							  });

			          	});
			          </script>

				</div>

		        <button type="submit" class="btn btn-primary">Update Products</button>

		    </form>

		</div>

	@endif

</div>
