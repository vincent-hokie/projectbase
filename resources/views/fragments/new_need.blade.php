<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<h4 style="text-align:center">Enter the details of a product you need now or in the future!</h4>
        
    <form action="/agri/create_need" method="post" style="width:70%; margin:auto" enctype="multipart/form-data" class="tasi-form">
        
         <!-- Laravel Requirement -->
    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

        <div class="form-group">
        	<label for="product_name">Product</label>
            <select class="form-control" name="product_name" id="product_name" required>
                
                @for($i = 0; $i < sizeof($products); $i++)
                    @if(  old('product_name') && old('product_name') == $products[$i]->product_id  )
                        <option value="{{ $products[$i]->product_id }}" selected>{{ $products[$i]->name }}</option>
                    @else
                        <option value="{{ $products[$i]->product_id }}">{{ $products[$i]->name }}</option>
                    @endif
                @endfor

            </select>

            @if ($errors->has('product_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('product_name') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
        	<label for="">Date Required</label>
            <input class="form-control input-medium default-date-picker" type="text" name="date_required" id="date_required" required min="{{ date_format(date_create(date('Y-m-d')),'Y-m-d')  }}" value="{{ old('date_required') }}" >

            @if ($errors->has('date_required'))
                <span class="help-block">
                    <strong>{{ $errors->first('date_required') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
            <label for="quantity">Quantity<p id="per" style="display:inline"> ( kgs/ litres/ items )</p></label>
            <input type="number" min=1 class="form-control" name="quantity" required placeholder="e.g. 3000" value="{{ old('quantity') }}" >
            
            @if ($errors->has('quantity'))
                <span class="help-block">
                    <strong>{{ $errors->first('quantity') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('proposed_price') ? ' has-error' : '' }}">
        	<label for="proposed_price">Unit Cost UgX ( per <p id="per" style="display:inline">kg/ litre/ item )</p></label>
            <input type="number" min=1 class="form-control" name="proposed_price" required placeholder="e.g. 2000" value="{{ old('proposed_price') }}" >

            @if ($errors->has('proposed_price'))
                <span class="help-block">
                    <strong>{{ $errors->first('proposed_price') }}</strong>
                </span>
            @endif
            
        </div>

        <div class="form-group">
        	<label for="description">Description</label>
            <textarea rows=3 class="form-control" id="description" name="description" placeholder="A short description of what you would like your products to be/ age/ etc. Anything you would like the farmer to know about the need/ product." >{{ old('description') }}</textarea>
        </div>
            
        <button type="submit" class="btn btn-primary col-lg-4 col-md-4 col-sm-5 col-xs-6">Create Need</button>

    </form>

<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>

