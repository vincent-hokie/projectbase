<h4 style="text-align:center">Enter the new product here!</h4>
        
    <form action="/agri/uploadFile" method="post" style="width:70%; margin:auto" enctype="multipart/form-data">
        
         <!-- Laravel Requirement -->
    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

        <div style="margin:10px auto" class="form-group">
        	<label for="product_name">Product</label>
            <input type="name" class="form-control" name="product_name" id="product_name" required placeholder="Product Name" >

            @if($errors->has('product_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('product_name') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
            <label>Product Type</label>
            <select name="product_type" class="form-control">
                
                <option value="none"></option>
                @foreach($types as $type)
                    <option value="{{  $type->type_id  }}">{{  $type->name  }}</option>
                @endforeach

              </select>

            @if ($errors->has('product_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('product_type') }}</strong>
                </span>
            @endif

        </div>

        <div style="margin:10px auto" class="form-group">
        	<label for="description">Description</label>
            <textarea rows=3 class="form-control" id="description" name="description" placeholder="A short description of where the products are grown/ bred. Characteristics etc." ></textarea>
        </div>

        <div style="margin:10px auto" class="form-group">
        	<label for="crop_img">Select an image to upload(.jpg, .jpeg, .png)</label>
            <input type="file" class="form-control" id="crop_img" name="image" required >

            @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif

        </div>
            
        <button type="submit" class="btn btn-primary btn-lg" data-dismiss="modal" style="width:30%; margin:10px 35%">Upload Crop</button>

    </form>