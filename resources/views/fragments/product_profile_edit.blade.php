<h3>{{ $product->name }}<hr></h3>
<div>

    <form role="form" action="/agri/product/{{ strtolower(str_replace(' ', '-', $product->name )) }}/save" method="post" enctype="multipart/form-data">

        <!-- Laravel Requirement -->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
        <input type="hidden" name="_data" value="{{ $product->product_id }}" />

    	<div class="image col-lg-5 col-md-5 col-sm-7 col-xs-10">
    		<img class="thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12" src="/agri/images/products/{{ $product->photo }}" />
    		
            @if(  $isAdmin  )
        		<div class="form-group" style="margin-top:20px">
                    <label>Select an image to upload(.jpg, .jpeg, .png, .gif)</label>
                    <input type="file" class="form-control" id="crop_img" name="image" >
                </div>

                @if ($errors->has('image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif

            @endif
            
            <button class="btn btn-default" type="submit">
                <span class="glyphicon glyphicon-save" title="Save Product"></span> Save Product
            </button>

    	</div>

    	<div>

    		<div class="well well-sm form-group col-lg-5 col-md-5 col-sm-7 col-xs-10">
            	<label class="label label-default">Product Name</label>
            	<input class="form-control" name="product_name" type="text" required value='{{ $product->name }}'>

                @if ($errors->has('product_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_name') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-5 col-md-5 col-sm-7 col-xs-10">
            	<label class="label label-default">Product Type</label>
                <select name="product_type" class="form-control">
                    
                    <option value="none"></option>
                    @foreach($types as $type)

                        <option value="{{  $type->type_id  }}"

                        @if(  $product->type == $type->type_id  )
                            selected
                        @endif

                        >{{  $type->name  }}</option>

                    @endforeach

                  </select>

                @if ($errors->has('product_type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_type') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-5 col-md-5 col-sm-7 col-xs-10">
            	<label class="label label-default">Description</label>
            	<textarea class="form-control" name="description" rows="3">{{ $product->description }}</textarea>

                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif

            </div>


    	</div>

    </form>

</div>
