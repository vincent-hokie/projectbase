
	@if(  session("staff")  )
		
		<div class="alert alert-success" style="margin-top:10px">
            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> The staff member, {{  session("staff")  }} has been successfully added. You're free to revoke and give rights as you please below.
        </div>

	@endif

	<!--dynamic table-->
	<link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


<!--main content start-->
<div class="col-sm-12">

	<section class="panel">
		
		<div class="panel-body">

			<div class="adv-table">

				<table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

					<thead>
						<tr id="none">
							<th></th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>E-Mail address</th>
							<th>Enabled</th>
							<th>System Rights</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

					<style type="text/css">

						.table tr td { padding:0 !important }
						.table tr td a { text-decoration: none; color: #000 }
						.table tr td a:hover { text-decoration: none }
						.table tr td a p { padding:8px; margin:0 }

					</style>

					<?php

						for($i = 0; $i < sizeof($staff); $i++){

							echo "<tr id='".$staff[$i]->id."'>
									";
							echo "<td>
									<p>".($i+1)."</p>
								</td>";
							echo "<td>
									<p>".$staff[$i]->first_name."</p>
								</td>";
							echo "<td>
									<p>".$staff[$i]->last_name."</p>
								</td>";
							echo "<td>
									<p>".$staff[$i]->email."</p>
								 </td>";
							echo "<td>
									  <input type='checkbox' data-size='mini' data-on-text='YES' data-off-text='NO'
										data-off-color='danger' data-on-color='success' data-email='".$staff[$i]->email."'  ";
									  	if( $staff[$i]->enabled == 1) echo " checked ";
							echo " class='staff_enabled'>
								</td>";
							echo "<td>
									  <input type='checkbox' data-size='mini' data-on-text='YES' data-off-text='NO'
											data-off-color='danger' data-on-color='success' data-email='".$staff[$i]->email."'  ";
												if( $staff[$i]->role == 1) echo " checked ";
							echo "	class='staff_sys_rights'>
									  
								</td>";
							echo "<td>
									<a class='pushUrl' href='/agri/dashboard/staff/".strtolower($staff[$i]->email)."'>
										<p>Edit Profile</p>
									</a>
								</td>";
							echo "</tr>";

						}

					?>

					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>E-Mail address</th>
							<th>Enabled</th>
							<th>System Rights</th>
							<th></th>
						</tr>
					</tfoot>

				</table>

			</div>

		</div>

	</section>

</div>


	<!-- js placed at the end of the document so the pages load faster -->
	<script type="text/javascript" language="javascript" src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

	<!--dynamic table initialization -->
	<script src="/agri/js/dynamic_table_init.js"></script>

	<script type="text/javascript">

  		$(document).ready(function(){

  			$(".staff_enabled").on("switchChange.bootstrapSwitch", function(event, state){
  				
  				//when the switch is toggles, disable it untill the db is changed accordingly
  				$(this).attr("disabled", "disabled");

  				var theSwitch = $(this);
  				var email = $(this).attr("data-email");
  				var action = "enable";

  				if(  !state  )
  					action = "disable";
  				
  				$.ajax({
                    type:'POST', url:'/agri/dashboard/staff/enable/'+action+'/'+email,
                    data:{  _token : $("meta[name='csrf-token'").attr('content')  },
                    success:function(data){

                    	$(this).removeAttr(  "disabled"  );

                        if(  data != 1  )
                        	theSwitch.bootstrapSwitch("toggleState");
                        
                    }
                    
                });

  			});

  			$(".staff_sys_rights").on("switchChange.bootstrapSwitch", function(event, state){
  				
  				//when the switch is toggles, disable it untill the db is changed accordingly
  				$(this).attr("disabled", "disabled");

  				var theSwitch = $(this);
  				var email = $(this).attr("data-email");
  				var action = "enable";

  				if(  !state  )
  					action = "disable";
  				
  				$.ajax({
                    type:'POST', url:'/agri/dashboard/staff/verify/'+action+'/'+email,
                    data:{  _token : $("meta[name='csrf-token'").attr('content')  },
                    success:function(data){

                    	$(this).removeAttr(  "disabled"  );

                        if(  data != 1  )
                        	theSwitch.bootstrapSwitch("toggleState");

                        if(  data == 3  )
                       		window.location = "/agri/dashboard";

                       	if(  data == 2  )
                       		vex.dialog.alert('There\'s only one System administrator remaining, you can\'t revoke your rights!');
                       		
                        
                    }
                    
                });

  			});

  		});

  	</script>

  	