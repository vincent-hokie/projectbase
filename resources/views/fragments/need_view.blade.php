<div>

	<style type="text/css">
		#needView .body .image span.glyphicon{ font-size: 25px; cursor: pointer; padding: 10px }
		#needView .body .image span.glyphicon:hover{ background-color: #eee }
		#needView .body .well.well-sm p{ margin-top:10px; margin-left:20px }
	</style>

	<div class="image col-lg-5 col-md-5 col-sm-7 col-xs-10">
		<label>Product</label>
		<img class="thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12" src="/agri/images/products/{{ $need->photo }}" />
		
		<div class="panel panel-default" style="float:left" id="{{ $need->need_id }}">

			<a href='/agri/dashboard/my-needs/{{ $need->need_id }}/edit' class="btn btn-default" role="button">
            	<span class="glyphicon glyphicon-edit" title="Edit Need"></span> Edit Need
            </a>

            <button class="btn btn-default" type="button" id="deleteNeed">
                <span class="glyphicon glyphicon-trash" title="Delete Need"></span> Delete Need
            </button>

		</div>

	</div>

	<div>

		<div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Product Name</span>
			<p>{{ $need->name }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Date Required</span>
			<p>{{ date('M d Y', strtotime( $need->date_required )) }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Neede quantity</span>
			<p>{{ $need->quantity }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Proposed unit cost ( per kg/litres/items ) </span>
			<p>{{ $need->proposed_price }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Description</span>
			<p>{{ $need->description }}<p/>
        </div>

	</div>

</div>