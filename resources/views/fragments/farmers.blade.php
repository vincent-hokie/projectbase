
	<!--dynamic table-->
	<link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


<!--main content start-->
	<div class="col-sm-12">

		@if(  session("farmer_insert")  )
        
	        <div class="alert alert-success" style="margin-top:10px">
	            <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> The farmer and their land has been successfully added!
	        </div>

	    @endif

		<section class="panel">
			
			<div class="panel-body">

				<div class="adv-table">

					<table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

						<thead>
							<tr id="none">
								<th></th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>E-Mail address</th>
								<th>Verified</th>
								<th>Enabled</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<style type="text/css">

							.table tr td { padding:0 !important }
							.table tr td a { text-decoration: none; color: #000 }
							.table tr td a:hover { text-decoration: none }
							.table tr td a p { padding:8px; margin:0 }

							/* The switch - the box around the slider */
							.switch { position: relative; display: inline-block; width: 60px; height: 34px; }

							/* Hide default HTML checkbox */
							.switch input {display:none;}

							/* The slider */
							.slider { position: absolute; cursor: pointer; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc;
								-webkit-transition: .4s; transition: .4s; }

							.slider:before { position: absolute; content: ""; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: white;
							-webkit-transition: .4s; transition: .4s; }

							input:checked + .slider { background-color: #2196F3; }

							input:focus + .slider { box-shadow: 0 0 1px #2196F3; }

							input:checked + .slider:before { -webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(26px); }

							/* Rounded sliders */
							.slider.round { border-radius: 34px; }

							.slider.round:before { border-radius: 50%; }

						</style>

						<?php

							for($i = 0; $i < sizeof($farmers); $i++){

								echo "<tr id='".$farmers[$i]->id."'>
										";
								echo "<td>
										<p>".($i+1)."</p>
									</td>";
								echo "<td>
										<p>".$farmers[$i]->first_name."</p>
									</td>";
								echo "<td>
										<p>".$farmers[$i]->last_name."</p>
									</td>";
								echo "<td>
										<p>".$farmers[$i]->email."</p>
									 </td>";
								echo "<td>
										<label class='switch'>
											<input type='checkbox' class='farmer_verified' 
											data-size='mini' data-on-text='YES' data-off-text='NO'
											data-off-color='danger' data-on-color='success' data-email='".$farmers[$i]->email."' ";
												if( $farmers[$i]->verified == 1) echo " checked ";
								echo "</td>";
								echo "<td>
										<input type='checkbox' class='farmer_enabled' 
										data-on-text='YES' data-off-text='NO' data-size='mini'
										data-off-color='danger' data-on-color='success' data-email='".$farmers[$i]->email."' ";
								
								if( $farmers[$i]->enabled == 1)
									echo " checked ";

								echo " /> 
										</td>";
								echo "<td>
										<a class='pushUrl' href='/agri/dashboard/farmer/".strtolower($farmers[$i]->email)."'>
											<p>Edit Profile</p>
										</a>
									</td>";
								echo "</tr>";

							}

						?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>E-Mail address</th>
								<th>Verified</th>
								<th>Enabled</th>
								<th></th>
							</tr>
						</tfoot>

					</table>
			</div>

		</div>

	</section>

</div>


	<!-- js placed at the end of the document so the pages load faster -->
	<script type="text/javascript" language="javascript" src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

	<!--dynamic table initialization -->
	<script src="/agri/js/dynamic_table_init.js"></script>

  	<script type="text/javascript">

  		$(document).ready(function(){

  			$(".farmer_enabled").on("switchChange.bootstrapSwitch", function(event, state){
  				
  				//when the switch is toggles, disable it untill the db is changed accordingly
  				$(this).attr("disabled", "disabled");

  				var theSwitch = $(this);
  				var email = $(this).attr("data-email");
  				var action = "enable";

  				if(  !state  )
  					action = "disable";
  				
  				$.ajax({
                    type:'POST', url:'/agri/dashboard/farmer/enable/'+action+'/'+email,
                    data:{  _token : $("meta[name='csrf-token'").attr('content')  },
                    success:function(data){

                        if(data == 1)
                            theSwitch.removeAttr(  "disabled"  );
                        else
                        	theSwitch.bootstrapSwitch("toggleState");
                        
                    }
                    
                });

  			});

  			$(".farmer_verified").on("switchChange.bootstrapSwitch", function(event, state){
  				
  				//when the switch is toggles, disable it untill the db is changed accordingly
  				$(this).attr("disabled", "disabled");

  				var theSwitch = $(this);
  				var email = $(this).attr("data-email");
  				var action = "enable";

  				if(  !state  )
  					action = "disable";
  				
  				$.ajax({
                    type:'POST', url:'/agri/dashboard/farmer/verify/'+action+'/'+email,
                    data:{  _token : $("meta[name='csrf-token'").attr('content')  },
                    success:function(data){

                        if(data == 1)
                            theSwitch.removeAttr(  "disabled"  );
                        else
                        	theSwitch.bootstrapSwitch("toggleState");
                        
                    }
                    
                });


  			});

  		});

  	</script>
  	