<div>

	<style type="text/css">
		#needView .body .image span.glyphicon{ font-size: 25px; cursor: pointer; padding: 10px }
		#needView .body .image span.glyphicon:hover{ background-color: #eee }
		#needView .body .well.well-sm p{ margin-top:10px; margin-left:20px }
	</style>

	<div class="image col-lg-5 col-md-5 col-sm-7 col-xs-10">
		<label>Product Photo</label>
		<img class="thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12" src="/agri/images/products/{{ $stock->photo }}" />
		
		<div class="panel panel-default" style="float:left" id="{{ $stock->stock_id }}">

			<a href='/agri/dashboard/my-stock/{{ $stock->stock_id }}/edit' class="btn btn-default" role="button">
            	<span class="glyphicon glyphicon-edit" title="Edit Need"></span> Edit Stock
            </a>

            <button class="btn btn-default" type="button" id="deleteStock">
                <span class="glyphicon glyphicon-trash" title="Delete Need"></span> Delete Stock
            </button>

		</div>

	</div>

	<div>

		<div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Product Name</span>
			<p>{{ $stock->name }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Maturity Date</span>
			<p>{{ date('M d Y', strtotime( $stock->maturity_date )) }}<p/>
        </div>

        @if( strtotime( $stock->maturity_date ) <= strtotime(date("Y/m/d")) )
	        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
	            <span class="label label-default">Quantity ( Actual )</span>
				<p>{{ $stock->quantity_actual }}<p/>
	        </div>
	    @else
	        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
	            <span class="label label-default">Quantity ( Expected )</span>
				<p>{{ $stock->quantity_expected }}<p/>
	        </div>
	    @endif

	    <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Unit Cost</span>
			<p>{{ $stock->unit_cost }}<p/>
        </div>

	    <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Land Name</span>
			<p>{{ $stock->land_name }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Description</span>
			<p>{{ $stock->description }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Valid</span>
			
			@if(  $stock->valid == 1  )
				<p>Yes<p/>
			@else
				<p>No<p/>
			@endif

        </div>

	</div>

</div>