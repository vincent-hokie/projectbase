<style type="text/css">
	
	.table tr td { padding:0 !important }
	.table tr td a { text-decoration: none; color: #000 }
	.table tr td a:hover { text-decoration: none }
	.table tr td a p { padding:8px; margin:0 }

</style>


	<!--dynamic table-->
	<link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


<!--main content start-->
	<div class="col-sm-12">

		<section class="panel">
			
			<div class="panel-body">

				<div class="adv-table">

					<table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

						<thead>
							<tr id="none">
								<th></th>
								<th>Product</th>
								<th>Quantity( kgs/ litres/ items )</th>
								<th>Cost ( Ugx per kg/ litre/ item )</th>
								<th>Date Required</th>
								<th>Valid</th>
							</tr>
						</thead>
						<tbody>

							<?php

								for($i = 0; $i < sizeof($needs); $i++){

									echo "<tr id='".$needs[$i]->need_id."'>";
									echo "<td>
											<a class='pushUrl' href='/agri/dashboard/my-needs/".$needs[$i]->need_id."'>
												<p>".($i+1)."</p>
											</a>
										</td>";
									echo "<td>
											<a class='pushUrl' href='/agri/dashboard/my-needs/".$needs[$i]->need_id."'>
												<p>".$needs[$i]->name."</p>
											</a>
										</td>";
									echo "<td>
											<a class='pushUrl' href='/agri/dashboard/my-needs/".$needs[$i]->need_id."'>
												<p>".$needs[$i]->quantity."</p>
											</a>
										</td>";
									echo "<td>
											<a class='pushUrl' href='/agri/dashboard/my-needs/".$needs[$i]->need_id."'>
												<p>".$needs[$i]->proposed_price."</p>
											</a>
										</td>";
									echo "<td>
											<a class='pushUrl' href='/agri/dashboard/my-needs/".$needs[$i]->need_id."'>
												<p>".$needs[$i]->date_required."</p>
											</a>
										</td>";
									echo "<td>
										<input type='checkbox' class='need_valid' 
										data-on-text='YES' data-off-text='NO' data-size='mini'
										data-off-color='danger' data-on-color='success' data-email='".$needs[$i]->need_id."' ";
											if( $needs[$i]->valid == 1) echo " checked ";
								echo " /></td>";
								echo "</tr>";

							}

						?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th>Product</th>
								<th>Quantity( kgs/ litres/ items )</th>
								<th>Cost ( Ugx per kg/ litre/ item )</th>
								<th>Date Required</th>
								<th>Valid</th>
							</tr>
						</tfoot>

					</table>
			</div>

		</div>

	</section>

</div>


	<!-- js placed at the end of the document so the pages load faster -->
	<script type="text/javascript" language="javascript" src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

	<!--dynamic table initialization -->
	<script src="/agri/js/dynamic_table_init.js"></script>

  	<script type="text/javascript">

  		$(document).ready(function(){

  			$(".need_valid").on("switchChange.bootstrapSwitch", function(event, state){
  				
  				//when the switch is toggles, disable it untill the db is changed accordingly
  				$(this).attr("disabled", "disabled");

  				var theSwitch = $(this);
  				var email = $(this).attr("data-email");
  				var action = "enable";

  				if(  !state  )
  					action = "disable";
  				
  				$.ajax({
                    type:'POST', url:'/agri/dashboard/need/valid/'+action+'/'+email,
                    data:{  _token : $("meta[name='csrf-token'").attr('content')  },
                    success:function(data){

                        if(data == 1)
                            theSwitch.removeAttr(  "disabled"  );
                        else
                        	theSwitch.bootstrapSwitch("toggleState");
                        
                    }
                    
                });

  			});

  		});

  	</script>