<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<script type="text/javascript">
    
    $(document).ready(function(){

        var dateInput = $("form#stock_form input[name='maturity_date']");

        var qActual = $("form#stock_form input[name='quantity_actual']");
        var qExpected = $("form#stock_form input[name='quantity_expected']");

        //get the quantity expected if its future stock otherwise show a quantity field
        $(document).on("change", dateInput, function(){

            var date = dateInput.val()
            if( new Date() >  new Date(date) ){

                qActual.parents("div.form-group").show(1000);

                qExpected.parents("div.form-group").hide(1000);
                qExpected.val(0);

            }else{

                qExpected.parents("div.form-group").show(1000);

                qActual.parents("div.form-group").hide(1000);
                qActual.val(0);

            }
        });

        

        $(document).on("change", "input[name='date_required']", function(){

            var today = Date.parese(  new Date()  );
            var someday = Date.parese(  $(this).val()  );

            if (someday > today) {
                text = "Today is before January 14, 2100.";
            } else {
                text = "Today is after January 14, 2100.";
            }

        });
        
        

    });

</script>

<h4 style="text-align:center">Enter the details of a stock you have or WILL have!</h4>
        
    <form action="/agri/create_stock" method="post" style="width:70%; margin:auto" id="stock_form">
        
         <!-- Laravel Requirement -->
    	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

        <div class="form-group">
        	<label for="product_name">Product</label>
            <select class="form-control" name="product_name" id="product_name" required>

                @foreach(  $products as $product  )
                    <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group">
            <label for="land_name">Land name</label>
            <select class="form-control" name="land_name" id="land_name" required>

                @foreach(  $lands as $land  )
                    <option value="{{  $land->land_id  }}">{{  $land->name  }}</option>
                @endforeach

            </select>
        </div>

        <div class="form-group {{ $errors->has('maturity_date') ? ' has-error' : '' }}">
        	<label for="">Maturity Date (expected or past)</label>
            <input class="form-control input-medium default-date-picker" type="text" name="maturity_date" id="maturity_date" required >

            @if ($errors->has('maturity_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('maturity_date') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('quantity_actual') ? ' has-error' : '' }}" style="display:none">
            <label for="quantity">Quantity<p id="per" style="display:inline"> ( kgs/ litres/ items )</p></label>
            <input type="number" class="form-control" name="quantity_actual" value=0 placeholder="e.g. 3000" >
            
            @if ($errors->has('quantity_actual'))
                <span class="help-block">
                    <strong>{{ $errors->first('quantity_actual') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('quantity_expected') ? ' has-error' : '' }}" style="display:none">
            <label for="quantity">Estimated Quantity Expected<p id="per" style="display:inline"> ( kgs/ litres/ items )</p></label>
            <input type="number" class="form-control" name="quantity_expected" value=0 placeholder="e.g. 3000" >
            
            @if ($errors->has('quantity_expected'))
                <span class="help-block">
                    <strong>{{ $errors->first('quantity_expected') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('unit_cost') ? ' has-error' : '' }}">
            <label for="unit_cost">Unit Cost Ugx per<p id="per" style="display:inline"> ( kgs/ litres/ items )</p></label>
            <input type="number" class="form-control" name="unit_cost" value=0 placeholder="e.g. 3000" >
            
            @if ($errors->has('unit_cost'))
                <span class="help-block">
                    <strong>{{ $errors->first('unit_cost') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
        	<label for="description">Description</label>
            <textarea rows=3 class="form-control" id="description" name="description" placeholder="A short description of your product, how its grown, particular breed etc" ></textarea>
        </div>
            
        <button type="submit" class="btn btn-primary col-lg-4 col-md-4 col-sm-5 col-xs-6">Create Stock</button>

    </form>

    <script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/agri/js/advanced-form-components.js"></script>
