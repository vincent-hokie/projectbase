<h3>{{ $product->name }}<hr></h3>
<div>

	<style type="text/css">
		#productProfile .image span.glyphicon{ font-size: 25px; cursor: pointer; padding: 10px }
		#productProfile .image span.glyphicon:hover{ background-color: #eee }
		#productProfile .well.well-sm p{ margin-top:10px; margin-left:20px }
	</style>

	<div class="image col-lg-5 col-md-5 col-sm-7 col-xs-10">
		<img class="thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12" src="/agri/images/products/{{ str_replace(  ' ', '-', $product->photo  ) }}" />
		
		<div class="panel panel-default" style="float:left" id="{{ $product->product_id }}">

            <a href='/agri/dashboard/products/{{ str_replace(" ", "-", strtolower($product->name)) }}/edit' class="btn btn-default" role="button">
            	<span class="glyphicon glyphicon-edit" title="Edit Product"></span> Edit Product
            </a>

            @if(  $isAdmin  )
	            <button class="btn btn-default" type="button" id="deleteProduct">
	                <span class="glyphicon glyphicon-trash" title="Delete Product"></span> Delete Product
	            </button>
            @endif

		</div>

	</div>

	<div>

		<div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Product Name</span>
			<p>{{ $product->name }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Product Type</span>
			<p>{{ $product->type }}<p/>
        </div>

        <div class="well well-sm col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <span class="label label-default">Description</span>
			<p>{{ $product->description }}<p/>
        </div>

	</div>

</div>