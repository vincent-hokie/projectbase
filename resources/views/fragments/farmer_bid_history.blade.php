

	<!--dynamic table-->
	<link href="/agri/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
	<link href="/agri/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
	<link rel="stylesheet" href="/agri/assets/data-tables/DT_bootstrap.css" />

	<link rel="stylesheet" type="text/css" href="/agri/css/data_table.css">


<!--main content start-->
<div class="col-sm-12">

	<section class="panel">
		
		<div class="panel-body">

			<div class="adv-table">

				<table class="display table table-striped col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dynamic-table">

					<thead>
						<tr id="none">
							<th></th>
							<th>Buyer</th>
							<th>Product</th>
							<th>Maturity Date</th>
							<th>My Quantity( actual, expected )</th>
							<th>My Unit Cost</th>
						</tr>
					</thead>
					<tbody>

					<style type="text/css">

						.table tr td { padding:0 !important }
						.table tr td a { text-decoration: none; color: #000 }
						.table tr td a:hover { text-decoration: none }
						.table tr td a p { padding:8px; margin:0 }

					</style>

					
						@for($i = 0; $i < sizeof(  $bids  ); $i++)

							<tr id='{{  $bids[$i]->stock_id  }}'>
									
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>{{  ($i+1)  }}</p>
								</a>
							</td>
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>
										@if(  $bids[$i]->verified == "1"  )
		                                    <i class='fa fa-leaf' style='margin-right:10px'></i>
		                                @endif

		                                {{  $bids[$i]->first_name  }}  {{  $bids[$i]->last_name  }}
		                            </p>
								</a>
							</td>
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>{{  $bids[$i]->name  }}</p>
								</a>
							 </td>
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>{{  $bids[$i]->maturity_date  }}</p>
								</a>
							</td>
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>{{  $bids[$i]->quantity_actual  }}, {{  $bids[$i]->quantity_expected  }}</p>
								</a>
							</td>
							<td>
								<a href='/agri/dashboard/farmer/bid/converstion/{{  $bids[$i]->stock_id  }}/{{  $bids[$i]->buyer_id  }}'>
									<p>{{  $bids[$i]->unit_cost  }}</p>
								</a>
							</td>
							
							</tr>

						@endfor

					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th>Buyer</th>
							<th>Product</th>
							<th>Maturity Date</th>
							<th>My Quantity( actual, expected )</th>
							<th>My Unit Cost</th>
						</tr>
					</tfoot>

				</table>

			</div>

		</div>

	</section>

</div>


	<!-- js placed at the end of the document so the pages load faster -->
	<script src="/agri/js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript" language="javascript" src="/agri/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/agri/assets/data-tables/DT_bootstrap.js"></script>

	<!--dynamic table initialization -->
	<script src="/agri/js/dynamic_table_init.js"></script>

  	