<!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="/agri/css/jquery.steps.css" />

<style type="text/css">
  
  .stepy-titles li div {  background: #fff; }
  p.default-buttons{ margin-top: 40px }

</style>

<h4 style="text-align:center">Register a new farmer here!</h4>

<!-- page start-->
<div class="row">
  <div class="col-lg-12">
      <section>
          <div class="panel-body">
              <div class="stepy-tab">
                  <ul id="default-titles" class="stepy-titles clearfix">
                      <li id="default-title-0">
                          <div>Farmer Details</div>
                      </li>
                      <li id="default-title-1" class="current-step">
                          <div>Products</div>
                      </li>
                      <li id="default-title-2" class="">
                          <div>Property</div>
                      </li>
                  </ul>
              </div>
              <form class="form-horizontal" id="default" role="form" action="/agri/save/farmer/products" method="post" style="width:70%; margin:auto">

                    <!-- Laravel Requirement -->
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                    <input type="hidden" name="farmer_id" value="{{ $farmer_id }}" />

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 1</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type0" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 2</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type1" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 3</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type2" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 4</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type3" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 5</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type4" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                  <button class="btn btn-info" style="float:right" type="submit">Next</button>
                  
              </form>
          </div>
      </section>
  </div>
</div>
