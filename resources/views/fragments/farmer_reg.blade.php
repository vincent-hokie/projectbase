<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="/agri/css/jquery.steps.css" />


<style type="text/css">
  
  .stepy-titles li div {  background: #fff; }

</style>

<h4 style="text-align:center">Register a new farmer here!</h4>

<!-- page start-->
<div class="row">
  <div class="col-lg-12">
      <section>
          <div class="panel-body">
              <div class="stepy-tab">
                  <ul id="default-titles" class="stepy-titles clearfix">
                      <li id="default-title-0" class="current-step">
                          <div>Farmer Details</div>
                      </li>
                      <li id="default-title-1" class="">
                          <div>Land</div>
                      </li>
                  </ul>
              </div>
              <form class="form-horizontal" id="default" role="form" action="/agri/save/farmer" method="post" style="width:70%; margin:auto">
                      
                <!-- Laravel Requirement -->
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

				<!-- Names of the user input -->
				<div style="width:100%; margin:auto">
					<div style="width:30%; margin:10px auto; display:inline-block">
						<input type="text" class="required form-control" name="first_name" id="first_name" required placeholder="First Name" value="{{ old('first_name') }}">

						@if ($errors->has('first_name'))
				            <span class="help-block">
				                <strong>{{ $errors->first('first_name') }}</strong>
				            </span>
				        @endif

					</div>

					<div style="width:30%; margin:10px auto; display:inline-block">
						<input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="{{ old('middle_name') }}">

						@if ($errors->has('middle_name'))
				            <span class="help-block">
				                <strong>{{ $errors->first('middle_name') }}</strong>
				            </span>
				        @endif

					</div>

					<div style="width:30%; margin:10px auto; display:inline-block">
						<input type="text" class="form-control" name="last_name" id="last_name" required placeholder="Last Name" value="{{ old('last_name') }}">

						@if ($errors->has('last_name'))
				            <span class="help-block">
				                <strong>{{ $errors->first('last_name') }}</strong>
				            </span>
				        @endif

					</div>
				</div>
				 
				<!-- E-mail input -->
				<div class="form-group">
					<label for="email">Email address:</label>
					<input type="email" class="form-control" id="email" name="email" required placeholder="username@gmail.com" style="width:90%; margin:10px auto" value="{{ old('email') }}">

					@if ($errors->has('email'))
			            <span class="help-block">
			                <strong>{{ $errors->first('email') }}</strong>
			            </span>
			        @endif
			        

				</div>

				<!-- Password input -->
				<div class="form-group">
					<label for="password_1">Password:</label>
					<input type="password" class="form-control" id="password_1" name="password" required placeholder="Password" style="width:90%; margin:10px auto">

					@if ($errors->has('password'))
			            <span class="help-block">
			                <strong>{{ $errors->first('password') }}</strong>
			            </span>
			        @endif
			        

				</div>

				<!-- Password re-enter input -->
				<div class="form-group">
					<label class="sr-only" for="password_2">Re-Enter Password:</label>
					<input type="password" class="form-control" id="password_2" name="password_confirmation" required placeholder="Re-Enter Your Password" style="width:90%; margin:10px auto">

					    @if ($errors->has('password_confirmation'))
			            <span class="help-block">
			                <strong>{{ $errors->first('password_confirmation') }}</strong>
			            </span>
			        @endif
			        

				</div>



				<!-- Date Of Birth input -->
			    <div class="form-group">
			        <label for="dob_date">Date of Birth (mm/dd/yyy):</label>
			        <input type="text" class="form-control input-medium default-date-picker" name="dob_date" id="dob_date" style="width:90%; margin:10px auto" value="{{ old('dob_date') }}">
			    </div>

			    <!-- Nationality input -->
			    <div class="form-group">
			        <label for="nationality">Nationality:</label>
			        <select name="nationality" class="form-control" style="width:90%; margin:10px auto">
	            	
		            	<option value="none"></option>
		                @foreach (  $countries as $country  )
		                    <option value='{{  $country->id  }}'>{{  $country->country_name  }}</option>
		                @endforeach

		          </select>

			    </div>


			    <!-- phone contact input -->
				<div class="form-group">
					<label for="phone_contact">Phone Contact:</label>
					<input type="text" class="form-control" id="phone_contact" name="phone_contact" placeholder="256777098900" style="width:90%; margin:10px auto" value="{{ old('phone_contact') }}">

					@if ($errors->has('phone_contact'))
			            <span class="help-block">
			                <strong>{{ $errors->first('phone_contact') }}</strong>
			            </span>
			        @endif
			        

				</div>
			    

				<!-- Location input -->
			    <div class="form-group">

			        <label for="Location">Location:</label>
			        <input type="text" class="form-control" id="Location" name="location" placeholder="e.g. Kampala" style="width:90%; margin:10px auto" value="{{ old('location') }}">

			        @if ($errors->has('location'))
			            <span class="help-block">
			                <strong>{{ $errors->first('location') }}</strong>
			            </span>
			        @endif

			    </div>


			    <h4 style="margin-top:30px">Below, enter the products that this farmer will be dealing in.</h4>

			    <input type="hidden" name="product_number" value="5" />

			    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 1</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type0" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 2</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type1" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 3</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type2" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 4</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type3" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Product 5</label>
                      <div class="col-lg-10">
                          
                          <select name="product_type4" class="form-control">

                            <option value="none"></option>
                            @foreach($products as $product)
                                <option value="{{  $product->product_id  }}">{{  $product->name  }}</option>
                            @endforeach

                          </select>

                      </div>
                    </div>



                <button class="btn btn-info" style="float:right" type="submit">Next</button>
                  
              </form>
          </div>
      </section>
  </div>
</div>


<!--Form Wizard-->
<script src="/agri/js/jquery.stepy.js"></script>
<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
