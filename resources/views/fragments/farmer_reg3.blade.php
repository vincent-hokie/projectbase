<!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="/agri/css/jquery.steps.css" />
<link rel="stylesheet" type="text/css" href="/agri/css/leaflet.css" />
<link rel="stylesheet" href="/agri/css/leaflet-search.css" />

<style type="text/css">
  
  .stepy-titles li div {  background: #fff; }
  p.default-buttons{ margin-top: 40px }

</style>

<h4 style="text-align:center">Register a new farmer here!</h4>

<!-- page start-->
<div class="row">
  <div class="col-lg-12">
      <section>

        @if(  session("farmer_id")  )
            
            <div class="alert alert-success" style="margin-top:10px">
                <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> The farmer has been successfully registered, register the lands that they own below.
            </div>

        @endif

          <div class="panel-body">
              <div class="stepy-tab">
                  <ul id="default-titles" class="stepy-titles clearfix">
                      <li id="default-title-0">
                          <div>Farmer</div>
                      </li>
                      <li id="default-title-1" class="current-step">
                          <div>Land</div>
                      </li>
                  </ul>
              </div>


              <div class="alert alert-info" style="margin:0">
                <strong><span class="glyphicon glyphicon-info-sign"></span></strong>Click the map below to select the locations of the farmer's land, ensure to enter the name of the land below the map after selecting a location
              </div>
              

              <div id="map" style="height: 440px; border: 1px solid #AAA; top:30px"></div>


              <form class="form-horizontal" id="default" role="form" action="/agri/save/farmer/land" method="post" style="width:70%; margin:auto">

                      <!-- Laravel Requirement -->
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                      <input type="hidden" name="number" value="0">

                      <input type="hidden" name="farmer_id" value="{{  $farmer_id  }}">

                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="top:40px">

                        <div class="form-group">

                            <div class="col-lg-1"></div>

                            <div class="col-lg-5">
                              <label>Land Name</label>
                            </div>

                            <div class="col-lg-5">
                              <label>Size (decimals)</label>
                            </div>

                        </div>

                        <div class="row marker-inputs">

                        </div>

                      </div>

                  <button type="submit" class="finish btn btn-success" style="margin-top:40px">Finish</button>
                  
              </form>
          </div>
      </section>
  </div>
</div>


<!--Form Wizard-->
<script src="/agri/js/jquery.stepy.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyC99J6rFowiMloJVi2pQ4c7ZIYhM22uLEc"></script>
<script src="/agri/js/leaflet/leaflet.js"></script>
<script src="/agri/js/leaflet/leaflet-search.js"></script>

<script>
  

  var map = new L.Map('map', {zoom: 7, center: new L.latLng([ 1.0 , 32.0 ]) });
  map.addLayer(new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')); //base layer

  map.addControl( new L.Control.Search({
      sourceData: googleGeocoding,
      formatData: formatJSON,
      markerLocation: true,
      autoType: false,
      autoCollapse: true,
      minLength: 2
    }) );
  
  map.on('click', addMarker);
  
  //var myURL = jQuery( 'script[src$="leaf-demo.js"]' ).attr( 'src' ).replace( 'leaf-demo.js', '' )
  /*var markers = [];*/

  var myIcon = L.icon({
    iconUrl: '/agri/images/icons/pin24.png',
    iconRetinaUrl: '/agri/images/icons/pin48.png',
    iconSize: [29, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
  });

  var on_focus_icon = L.icon({
    iconUrl: '/agri/images/icons/marker-icon.png',
    iconRetinaUrl: '/agri/images/icons/marker-icon-2x.png',
    iconSize: [29, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
  });

  function addMarker(e){
      // Add marker to map at click location; add popup window
      
      //var newMarker = new L.marker( e.latlng, {icon: myIcon} ).addTo(map);
      var id = e.latlng.lat+"-"+e.latlng.lng;
      id = id.split('.').join('').split('-').join('');

      var newMarker = new L.marker(e.latlng, { icon: myIcon, id: id })
                              .on('click', markerOnClick)
                              .on("popupopen", onPopupOpen)
                              .bindPopup("<input type='button' value='Delete this marker' class='marker-delete-button btn btn-danger' />")
                              .addTo(map);

      var length = parseInt(  $("form input[name='number']").val()  );

      $(".row.marker-inputs").append('\
                          <div class="form-group" id="'+id+'"> \
\
                              <div class="col-lg-1"> \
                                    <img src="/agri/images/icons/map.png" style="float:left; width: 100%; cursor:pointer" class="select_map" /> \
                              </div> \
\
                              <div class="col-lg-5"> \
                                    <input type="text" class="form-control" name="land'+length+'" class="land" required> \
                              </div> \
\
                              <div class="col-lg-5"> \
                                    <input type="number" class="form-control col-lg-11 size" name="size'+length+'" min=1 required> \
\
                                    <input type="hidden" name="latlng'+length+'" class="latlng"> \
                                    <input type="hidden" name="general'+length+'" class="general"> \
                                    <input type="hidden" name="general_latlng'+length+'" class="general_latlng"> \
\
                              </div> \
                              <div class="col-lg-1"> \
                                    <img src="/agri/images/icons/loading.gif" style="height:30px" class="gif_loader" /> \
                              </div> \
\
                          </div>');
      
      //increment the number of created markers
      $("form input[name='number']").val( (length+1)  );

      //aggregate the coordinates to the city name      
      asyncReverseGeocode(  e.latlng.lat+","+e.latlng.lng, id);

      /*markers[  e.latlng.lat+"-"+e.latlng.lng  ] = newMarker ;*/

  }


  $(document).on("focus", "form input.size", function(){

    var id = $(this).parents(".form-group").attr("id");
    getAllMarkers(id)

  });

  // Function to handle delete as well as other events on marker popup open
  function onPopupOpen() {

      var tempMarker = this;
      var id = this.options.id;

      // To remove marker on click of delete
      $(".marker-delete-button:visible").click(function () {

          $("div#"+id).hide(1000).remove();
          map.removeLayer(tempMarker);
          
      });

  }


  function markerOnClick(e){
    /*console.log( e  );
    alert("hi. you clicked the marker at "+ e.latlng+" "+e.target.options.id);*/
  }

  var geocoder = new google.maps.Geocoder();

  function googleGeocoding(text, callResponse){
    geocoder.geocode({address: text}, callResponse);
  }

  function formatJSON(rawjson){

    var json = {},
      key, loc, disp = [];
    for(var i in rawjson)
    {
      key = rawjson[i].formatted_address;
      
      loc = L.latLng( rawjson[i].geometry.location.lat(), rawjson[i].geometry.location.lng() );
      
      json[ key ]= loc; //key,value format
    }
    return json;

  }

  function asyncGeoCoding(address, id, specific_latlng){

    $.ajax({
        type:'POST',
        url:'https://maps.googleapis.com/maps/api/geocode/json?address='+ address +'&key=AIzaSyC99J6rFowiMloJVi2pQ4c7ZIYhM22uLEc',
        data:{ },
        success:function(data){

          var latlng = data.results[0].geometry.location;

          $(  ".row.marker-inputs div#"+id+" input.latlng"  ).val( specific_latlng );
          $(  ".row.marker-inputs div#"+id+" input.general_latlng"  ).val( latlng.lat+","+latlng.lng );
          $(  ".row.marker-inputs div#"+id+" input.general"  ).val( address );
          $(  ".row.marker-inputs div#"+id+" img.gif_loader"  ).hide( 500 );
            
        }
        
    });

  }


  function asyncReverseGeocode(latlng, id){

    $.ajax({
        type:'POST',
        url:'https://maps.googleapis.com/maps/api/geocode/json?latlng='+ latlng +'&key=AIzaSyC99J6rFowiMloJVi2pQ4c7ZIYhM22uLEc',
        data:{ },
        success:function(data){

          var results = data.results[0].address_components;

          var address = results[0].long_name+', '+results[1].long_name+', '+results[2].long_name;

          if(  results.length == 4  )
            address = results[1].long_name+', '+results[2].long_name+', '+results[3].long_name;
          
          if(  results.length == 5  )
            address = results[2].long_name+', '+results[3].long_name+', '+results[4].long_name;

          if(  results.length == 6  )
            address = results[3].long_name+', '+results[4].long_name+', '+results[5].long_name;

          //get the city name coordinates
          asyncGeoCoding(address, id, latlng);

            
        }
        
    });

  }
  

  //console.log(map._layers);

  // getting all the markers at once
function getAllMarkers(id) {
    
    var allMarkersObjArray = [];//new Array();
    var allMarkersGeoJsonArray = [];//new Array();

    //console.log(map._layers);

    $.each(map._layers, function (ml) {
        
        //console.log( map._layers[ml] );

        if (  map._layers[ml].feature || map._layers[ml]._popupHandlersAdded  ) {
            
            console.log( this  );

            if(  id == this.options.id  )
              this.options.icon = on_focus_icon;
            else
              this.options.icon = myIcon;
              
            

            //allMarkersObjArray.push(this);
            //allMarkersGeoJsonArray.push(JSON.stringify(this.toGeoJSON()));

        }
    })

    console.log(allMarkersObjArray);

}


</script>