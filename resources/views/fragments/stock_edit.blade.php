<link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <form role="form" action="/agri/update_stock/{{  $stock->stock_id  }}" method="post">

        <!-- Laravel Requirement -->
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

        <style type="text/css">
            .well { border: none !important }
            .well.well-sm.form-group{ background-color: rgba(0,0,0,0) !important }
            .well.well-sm.form-group p{ margin-top:10px; margin-left:20px }
        </style>

        <div class="image col-lg-5 col-md-5 col-sm-7 col-xs-10">
            <label>Product Photo</label>
            <img class="thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12" src="/agri/images/products/{{ $stock->photo }}" />
            
            <div class="panel panel-default" style="float:left" id="{{ $stock->stock_id }}">

                <button class="btn btn-default" type="submit">
                    <span class="glyphicon glyphicon-save" title="Save Stock"></span>
                    Save Stock
                </button>
                
            </div>

        </div>

    	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-10">

    		<div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Product Name</label>
                <select class="form-control" name="product_name" id="product_name" required>

                    @foreach($products as $product)
                        @if(  $product->product_id == $stock->product_id  )
                            <option value='{{ $product->product_id }}' selected> {{  $product->name  }}</option>
                        @else
                            <option value='{{ $product->product_id }}'> {{  $product->name  }}</option>
                        @endif
                    @endforeach

                </select>

                @if ($errors->has('product_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_name') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <span class="label label-default">Maturity Date</span>
                <input class="form-control input-medium default-date-picker" type="text" required name="maturity_date" id="maturity_date" value='{{ $stock->maturity_date }}'>

                @if ($errors->has('maturity_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('maturity_date') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Quantity ( Actual )</label>
            	<input class="form-control" name="quantity_actual" required type="number" value='{{ $stock->quantity_actual }}'>

                @if ($errors->has('quantity_actual'))
                    <span class="help-block">
                        <strong>{{ $errors->first('quantity_actual') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="label label-default">Quantity ( Expected )</label>
                <input class="form-control" name="quantity_expected" required type="number" value='{{ $stock->quantity_expected }}'>

                @if ($errors->has('quantity_expected'))
                    <span class="help-block">
                        <strong>{{ $errors->first('quantity_expected') }}</strong>
                    </span>
                @endif

            </div>


            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="label label-default">Unit Cost</label>
                <input class="form-control" name="unit_cost" required type="number" value='{{ $stock->unit_cost }}'>

                @if ($errors->has('unit_cost'))
                    <span class="help-block">
                        <strong>{{ $errors->first('unit_cost') }}</strong>
                    </span>
                @endif

            </div>


            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="label label-default">Land name</label>
                <select class="form-control" name="land_name" id="land_name" required>

                    @foreach(  $lands as $land  )
                        @if(  $land->product_id == $stock->land_id  )
                            <option value='{{ $land->land_id }}' selected> {{  $land->name  }}</option>
                        @else
                            <option value='{{ $land->land_id }}'> {{  $land->name  }}</option>
                        @endif
                    @endforeach

                </select>

                @if ($errors->has('land_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('land_name') }}</strong>
                    </span>
                @endif

            </div>

            <div class="well well-sm form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<label class="label label-default">Description</label>
            	<textarea class="form-control" rows="3" name="description">{{ $stock->description }}</textarea>

                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif

            </div>

    	</div>

    </form>

</div>

<script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/agri/js/advanced-form-components.js"></script>
