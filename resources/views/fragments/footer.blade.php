<footer class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:50px">

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12">AgriMarket © Copyright 2017 - <?php echo date('Y') ?>. All Rights Reserved.</p>
    </div>
    
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <ul>
                <a href="#"><li>About AgriMarket</li></a>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <ul>
                <a href="#"><li>Terms of Use</li></a>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <ul>
                <a href="#"><li>Geographical Reach</li></a>
            </ul>
        </div>
    </div>
              
</footer>