
<style type="text/css">
  nav.top { padding: 0;     background-color: #222; border-color: #080808; }
</style>

<nav class="navbar navbar-inverse top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Used when the menu collapses into a responsive layoout  -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/agri" style="color:#fff">
              <b>Online Agricultural Products Management System</b>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                
                <!-- Authentication Links -->
                @if(  
                    Auth()->guard( "staff" )->check() || 
                    Auth()->guard( "buyer" )->check() || 
                    Auth()->guard( "farmer" )->check()
                  )

                    <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="font_awesome.html#" aria-expanded="false">

                              @if(  Auth()->guard( "staff" )->check()  )

                                @if(  !Auth::guard( "staff" )->user()->photo ||  Auth::guard( "staff" )->user()->photo == ""  )
                                                  <img src="/agri/images/users/unknown.png" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @else
                                                  <img src="/agri/images/users/{{  Auth::guard( "staff" )->user()->photo  }}" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @endif

                                              <span class="username">
                                                {{ Auth()->guard( "staff" )->user()->first_name }} 
                                                {{ Auth()->guard(  "staff"  )->user()->last_name }}
                                              </span>

                              @elseif(  Auth()->guard( "buyer" )->check()  )

                                @if(  !Auth::guard( "buyer" )->user()->photo ||  Auth::guard( "buyer" )->user()->photo == ""  )
                                                  <img src="/agri/images/users/unknown.png" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @else
                                                  <img src="/agri/images/users/{{  Auth::guard( "buyer" )->user()->photo  }}" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @endif

                                              <span class="username">
                                                {{ Auth()->guard( "buyer" )->user()->first_name }} 
                                                {{ Auth()->guard(  "buyer"  )->user()->last_name }}
                                              </span>

                              @elseif(  Auth()->guard( "farmer" )->check()  )

                                @if(  !Auth::guard( "farmer" )->user()->photo ||  Auth::guard( "farmer" )->user()->photo == ""  )
                                                  <img src="/agri/images/users/unknown.png" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @else
                                                  <img src="/agri/images/users/{{  Auth::guard( "farmer" )->user()->photo  }}" class="img-thumbnail" alt="User Profile Photo" style="width:30px; padding:0">
                                              @endif

                                              <span class="username">
                                                {{ Auth()->guard( "farmer" )->user()->first_name }} 
                                                {{ Auth()->guard(  "farmer"  )->user()->last_name }}
                                              </span>

                              @endif

                              <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout" style="box-shadow: 0 2px 8px rgba(0, 0, 0, 0.176) !important; margin-top:15px">
                                <div class="log-arrow-up"></div>
                                <li><a href="/agri/dashboard/profile"><i class=" fa fa-user"></i>Profile</a></li>
                                <li><a href="/agri/dashboard/preferences"><i class="fa fa-cog"></i> Preferences</a></li>
                                <li><a href="/agri/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li id="logoutButton"><a href="#"><i class="fa fa-key"></i> Log Out</a></li>
                            </ul>
                        </li>
                      
                @else

                  <li><a data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Login</a></li>
                  <li><a href="register" style="color:#fff"><span class="glyphicon glyphicon-log-in" style="margin-right:10px"></span>Register</a></li>
                    

                @endif

            </ul>
        </div>
    </div>
</nav>