<?php
    use App\Country;
?>
<!doctype html>
<html>

    <head>
    
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Register</title>
            
            <link href="/agri/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />

            <script src="/agri/js/jquery-1.12.4.min.js"></script>
            <script src="/agri/js/bootstrap.min.js"></script>
            
            <script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script src="/agri/js/advanced-form-components.js"></script>
            
            <style type="text/css">
                span.help-block { color: #a94442; }
            </style>

    </head>

    <body>
        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/agri">AgriSupermarket</a>
              </div>
            </div>
        </nav>
        
        <h4 style="text-align:center">Register and begin buying from our supermaket!</h4>
        
        <form role="form" action="{{ url('/register') }}" method="post" style="width:70%; margin:auto" class="form-horizontal">

            <!-- Laravel Requirement -->
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

            <!-- Names of the user input -->
            <div style="width:90%; margin:auto">
                <div style="width:30%; margin:10px auto; display:inline-block">
                    <input type="text" class="form-control" name="first_name" id="first_name" required placeholder="First Name" value="{{ old('first_name') }}">

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif

                </div>

                <div style="width:30%; margin:10px auto; display:inline-block">
                    <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="{{ old('middle_name') }}">

                    @if ($errors->has('middle_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('middle_name') }}</strong>
                        </span>
                    @endif

                </div>

                <div style="width:30%; margin:10px auto; display:inline-block">
                    <input type="text" class="form-control" name="last_name" id="last_name" required placeholder="Last Name" value="{{ old('last_name') }}">

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif

                </div>
            </div>
            
            <!-- Date Of Birth input -->
            <div class="form-group">
                <label for="dob_date">Date of Birth (mm/dd/yyy):</label>
                <input type="text" class="form-control input-medium default-date-picker" name="dob_date" id="dob_date" 
                <?php 
                    //we expect adults between 18 and 100 to use the application
                    echo "min='".date_format(date_sub(date_create(date('Y-m-d')),date_interval_create_from_date_string("100 years")),"Y-m-d")."' max='".date_format(date_sub(date_create(date('Y-m-d')),date_interval_create_from_date_string("18 years")),"Y-m-d")."'"; 
                ?> 
                required value="{{ old('dob_date') }}" >

                @if ($errors->has('dob_date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dob_date') }}</strong>
                    </span>
                @endif

            </div>

            <!-- Nationality input -->
            <div class="form-group">
                <label for="nationality">Nationality:</label>
                  <select name="nationality" class="form-control">
                    <option value="none"></option>
                    <?php

                        $countries = Country::orderBy("country_name")->get();
                        foreach ($countries as $country) 
                            echo "<option value='".$country->id."'>".$country->country_name."</option>";

                    ?>
                  </select>

                @if ($errors->has('nationality'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nationality') }}</strong>
                    </span>
                @endif

            </div>

            <!-- Gender input --
            <div class="form-group">
                <label for="gender">Gender:</label>
                <input style="display:none" type="radio" name="gender" checked value="none">
                <label class="radio-inline"><input type="radio" name="gender" value="M">Male</label>
                <label class="radio-inline"><input type="radio" name="gender" value="F">Female</label>
            </div>
            -->

            <!-- Location input -->
            <div class="form-group">
                <label for="Location">Location:</label>
                <input type="text" class="form-control" id="Location" name="location" placeholder="e.g. Kampala" style="margin:10px auto" value="{{ old('location') }}">

                @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                @endif

            </div>
            
            <!-- E-mail input -->
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" id="email" name="email" required placeholder="username@gmail.com" style="margin:10px auto" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

            </div>

            <!-- Password input -->
            <div class="form-group">
                <label for="password_1">Password:</label>
                <input type="password" class="form-control" id="password_1" name="password" required placeholder="Password" style="margin:10px auto">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            </div>

            <!-- Password re-enter input -->
            <div class="form-group">
                <label class="sr-only" for="password_2">Re-Enter Password:</label>
                <input type="password" class="form-control" id="password_2" name="password_confirmation" required placeholder="Re-Enter Your Password" style="margin:10px auto">

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif

            </div>

            <!-- Buyer Biography input -->
            <div class="form-group">
                <label for="bio">Buyer Profile:</label>
                <textarea class="form-control" id="bio" rows="5" name="bio" placeholder="Tell the farmers a little about yourself" style="margin:10px auto">{{ old('bio') }}</textarea>

                @if ($errors->has('bio'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bio') }}</strong>
                    </span>
                @endif

            </div>
           
            <button type="submit" class="btn btn-primary" data-dismiss="modal" style="width:30%; margin:10px 35%">Register!</button>

        </form>
    </body>
</html>