
    <li>
        <a href="/agri/dashboard/profile">
            <i class="fa fa-user"></i>
            <span>Farmer Profile</span>
        </a>
    </li>

    <li class="sub-menu">
        <a href="javascript:;">
            <i class="fa fa-laptop"></i>
            <span>Stock</span>
        </a>
        <ul class="sub">
            <li><a href="/agri/dashboard/new/stock">Create Stock</a></li>
            <li><a href="/agri/dashboard/my-stock">View My Stock</a></li>
        </ul>
    </li>

    <li>
        <a href="/agri/dashboard/search/need">
            <i class="fa fa-search"></i>
            <span>Search Need</span>
        </a>
    </li>

    <li>
        <a href="/agri/dashboard/farmer/bid/history">
            <i class="fa fa-clock-o"></i>
            <span> Bid History</span>
        </a>
    </li>
    
    <!--
  	<li>
        <a href="/dashboard/new/farmer">
            <i class="fa fa-dashboard"></i>
            <span>Give Feedback</span>
        </a>
    </li>
    -->