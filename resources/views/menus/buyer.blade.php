
    <li>
        <a href="/agri/dashboard/profile">
            <i class="fa fa-user"></i>
            <span>Buyer Profile</span>
        </a>
    </li>

    <li class="sub-menu">
        <a href="javascript:;">
            <i class="fa fa-thumb-tack"></i>
            <span>Need</span>
        </a>
        <ul class="sub">
            <li><a href="/agri/dashboard/new/need">Create Need</a></li>
            <li><a href="/agri/dashboard/my-needs">View My Needs</a></li>
        </ul>
    </li>

    <li>
        <a href="/agri/dashboard/search/stock">
            <i class="fa fa-search"></i>
            <span>Search Stock</span>
        </a>
    </li>

    <li>
        <a href="/agri/dashboard/buyer/bid/history">
            <i class="fa fa-clock-o"></i>
            <span> Bid History</span>
        </a>
    </li>
   
   <!--
  	<li>
        <a href="/dashboard/new/farmer">
            <i class="fa fa-dashboard"></i>
            <span>Give Feedback</span>
        </a>
    </li>
    -->