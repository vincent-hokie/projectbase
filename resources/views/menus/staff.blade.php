<?php
    use Illuminate\Support\Facades\Auth;
    $user = Auth()->guard("staff")->user();
?>

    <li>
        <a href="/agri/dashboard/profile">
            <i class="fa fa-user"></i>
            <span>Staff Profile</span>
        </a>
    </li>

    <li class="sub-menu">
        <a href="javascript:;">
            <i class="fa fa-leaf"></i>
            <span>Farmer</span>
        </a>
        <ul class="sub">
            <li><a href="/agri/dashboard/new/farmer">Add Farmer</a></li>
            <li><a href="/agri/dashboard/farmers">View Farmers</a></li>
        </ul>
    </li>

    @if(  $user->role == 1)
        <li class="sub-menu">
            <a href="javascript:;">
                <i class="fa fa-user"></i>
                <span>Staff</span>
            </a>
            <ul class="sub">
                <li><a href="/agri/dashboard/new/staff">Add Staff</a></li>
                <li><a href="/agri/dashboard/staff">View Staff</a></li>
            </ul>
        </li>

        <li>
            <a href="/agri/dashboard/buyers">
                <i class="fa  fa-users"></i>
                <span>View Buyers</span>
            </a>
        </li>
    @endif

    <li class="sub-menu">
        <a href="javascript:;">
            <i class="fa  fa-shopping-cart"></i>
            <span>Product</span>
        </a>
        <ul class="sub">
            @if(  $user->role == 1)
                <li><a href="/agri/dashboard/new/product">Add Product</a></li>
            @endif
            <li><a href="/agri/dashboard/products">View Products</a></li>
        </ul>
    </li>

    <!--
  	<li>
        <a href="/dashboard/new/farmer">
            <i class="fa fa-dashboard"></i>
            <span>Give Feedback</span>
        </a>
    </li>
    -->
    