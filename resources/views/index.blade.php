<!doctype html>
<html>
		<head>
		
			<title>Home</title>
			
			@include('templates.header')

			<link href="/agri/css/general.css" rel="stylesheet" type="text/css" />


			<script src="/agri/js/jquery-1.12.4.min.js"></script>

			@include('templates.js_links')

			<script src="/agri/js/home_page.js"></script>
		
		</head>
		
		<body>
			
		<style>
				
				/* Remove the navbar's default rounded borders and increase the bottom margin */ 
				.navbar {
					margin-bottom: 50px;
					border-radius: 0;
				}
				
				/* Remove the jumbotron's default bottom margin */ 
				 .jumbotron {
					margin-bottom: 0;
				}
			 
				/* Add a gray background color and some padding to the footer */
				footer {
				background-color: #f2f2f2;
					padding: 25px;
				}

				.navbar-nav>li>a {
				    padding-top: 10px;
				    padding-bottom: 0;
				}

				nav.top{  background-color: rgba(0,0,0,0.6) !important; margin-top: -50px; margin-bottom: 0 }

		</style>
	
	<body>
		
		@if(  sizeof($menu) > 0  )

			<?php

				$i = mt_rand(0,  sizeof($menu)-1 );
				$count = 0;
				$id = "";

				foreach($menu as $men){

					if($i == $count){
						$id = $men->product_id;
						break;
					}
						
					$count++;

				}

			?>

			@if( isset($menu[$id-1])  )
				<div class="jumbotron" style="background-image: url(/agri/images/products/{{ $menu[$id-1]->photo }}); background-size: contain;">
			@else
				<div class="jumbotron" style="background-image: url(/agri/images/products/default.jpg); background-size: contain;">
			@endif
		@else
			<div class="jumbotron" style="background-image: url(/agri/images/products/default.jpg); background-size: contain;">
		@endif

			<div class="container text-center" style="color: #fff; background-color: rgba(0,0,0,0.5);">
				<h2><b>Online Agricultural Products Management System</b></h2>
				<p style="    background-color: rgba(0,0,0,0.1); padding: 10px; padding-bottom: 15px;">
					<STRONG>Case Study of PamRone Investments</STRONG>
				</p>
				<!-- <p>Mission, Vission & Values</p> -->
			</div>
		</div>

		@include('fragments.dashboardNavbar')
		
		@if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
			<nav class="navbar navbar-inverse" id="types" style="overflow-x:scroll; min-height: 40px;">
			  <div class="container-fluid" style="width:2000px">
			    <ul class="nav navbar-nav">

			    	<li 
			      @if(  !$typee  )
			      	class="active"
			      @endif
			      	><a href="/agri/">All</a></li>

			      @foreach($types as $type)
			      	<li
			      		@if(  $typee  && $typee == strtolower(  str_replace(' ', '-', $type->name)  ) )
				      	class="active"
				      @endif

			      	><a href="/agri/{{  strtolower(  str_replace(' ', '-', $type->name)  )  }}">{{  $type->name }}</a></li>
			      @endforeach
			      
			    </ul>
			  </div>
			</nav>
		@endif

		<div style="width:30%; margin: 20px auto">

			<form action="/agri/search" method="get">
					<span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
					<span>Enter the product here</span>
					<input type="text" class="form-control" name="search" required placeholder="Product search word" size="30" style="width:90%; display:inline-block; margin-bottom:20px" />
			</form>

		</div>
		
		@include('templates.login_modal')

		<div class="container">

			@if(  session('registration')  )
				
				@if(  session('registration') == "success"  )
					<div class="alert alert-success" style="margin-top:10px">
		                <strong><span class="glyphicon glyphicon-thumbs-up"></span></strong> Your registration was successful! Login to begin using AgriMarket!!
		            </div>
	            @else
	            	<div class="alert alert-danger" style="margin-top:10px">
		                <strong><span class="glyphicon glyphicon-thumbs-down"></span></strong>Something went wrong during registration, please try agin. :(
		            </div>
	            @endif

			@endif
			
			<div style="float: left; width: 100%" id="menu-container">
						
						<div class="loading-gif col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center"><img src="/agri/images/icons/hex-loader.gif" style="width:90px"> Loading... </div>

						<?php

						$i = 0;


						foreach($menu as $men){
								
							if($i == 0)
								echo '<a href="product_profile/'.strtolower(str_replace(" ", "-", $men->name  )).'" id='.$men->id.'><img class="col-lg-6 col-md-6 col-sm-4 col-xs-12 dope-menu dope-menu-first resting_menu" src="/agri/images/products/'.$men->photo.'" /></a>';
							else
								echo '<a href="product_profile/'.strtolower(str_replace(" ", "-", $men->name  )).'" id='.$men->id.'><img class="col-md-3 col-md-3 col-sm-4 col-xs-12 dope-menu resting_menu" src="/agri/images/products/'.$men->photo.'" /></a>';
							
							$i++;

						}


						?>
			</div>

		</div>

		<br><br>

		<footer class="container-fluid text-center footer">
			<!--
				<p>Online Store Copyright</p>  
				<form class="form-inline">Get deals:
					<input type="email" class="form-control" size="50" placeholder="Email Address">
					<button type="button" class="btn btn-danger">Sign Up</button>
				</form>
			-->
		</footer>


	</body>

</html>