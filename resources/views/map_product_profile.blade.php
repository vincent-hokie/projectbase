<!doctype html>
<html>
<head>
    
    <title>Item</title>

    @include('templates.header')

    <link type="text/css" rel="stylesheet" href="/agri/css/template.css" />
    <link rel="stylesheet" type="text/css" href="/agri/css/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="/agri/assets/bootstrap-datepicker/css/datepicker.css" />
    
    <script src="/agri/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/agri/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/agri/js/advanced-form-components.js"></script>

</head>
<style>

	.farmers{ margin-top:10px; cursor:pointer }
	.farmers p { background-color: #eee; padding: 5px; margin: 1px 0 0 0 }

  .product_info{height: 100px; background-color: rgba(0,0,0,0.5); margin-left: 20px; margin-top: 0.5%; border-radius: 20px; display: none}
  .product_info p{ color: #fff; margin: 0; position: relative; text-align: center; padding: 15px;}

    .showFilterFormNoJs { display: block; }
    .hideFilterFormJs {  position: absolute; top: 80px; }

    body{ transition:1s; overflow-x:hidden; }
    #swipeme{ transition:1s; }

    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 0;
    }

    nav{  background-color: rgba(0,0,0,0.6) !important; margin-top: -50px; }

</style>
<body>
    
    <div class="jumbotron" style="background-image: url(/agri/images/products/{{ $product->photo }}); background-size: contain; margin-bottom: 0">
      <div class="container text-center" style="color: #fff; background-color: rgba(0,0,0,0.1);">
        <h1>{{ $product->name }}</h1>      
        <p><strong>Average unit cost: UgX {{ $average }}</strong></p>
      </div>
    </div>

		@include('fragments.dashboardNavbar')
        
        @include('templates.login_modal')

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="swipeme">
        	
            <!-- if the user is logged in, show his dash here as well -->
                <!-- dashboard menu -->
                    @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
                        <aside class="col-lg-2 col-md-3 col-sm-4 col-xs-6 off-canvas-left" style="margin-top:30px; background: #2a3542" id="swipeme-left">
                            
                            <div id="sidebar" class="nav-collapse">
                                <!-- sidebar menu start-->
                                <ul class="sidebar-menu" id="nav-accordion">
                                    
                                    <li>
                                        <a href="/agri/dashboard">
                                            <i class="fa fa-dashboard"></i>
                                            <span>Dashboard</span>
                                        </a>
                                    </li>


                                    @include('menus.'.$role)

                                </ul>
                                <!-- sidebar menu end-->
                            </div>

                        </aside>
                    @endif
                <!-- dashboard menu end -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 main-content" id="swipeme-main">

                @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:10px">

                        <button class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding:0" id="dashboard_show">
                            <i class="fa fa-dashboard"></i>
                        </button>

                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">

                            <a href="/agri/product_profile/{{ strtolower($product->name) }}/map" class="btn btn-info col-lg-1 col-lg-offset-5 col-md-1 col-md-offset-5 col-sm-2 col-sm-offset-4 col-xs-2  col-xs-offset-4" role="button" style="padding:0">
                                <i class="fa fa-map-marker"></i>
                            </a>

                            <a href="/agri/product_profile/{{ strtolower($product->name) }}" class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" role="button" style="padding:0">
                                <i class="fa fa-table"></i>
                            </a>

                        </div>

                        <button class="btn btn-info col-lg-1 col-md-1 col-sm-2 col-xs-2" style="padding:0" id="filter_show">
                            <span class="glyphicon glyphicon-filter"></span>
                        </button>

                    </div>

                @else

                    <div class="alert alert-info" style="margin-top:10px">
                        <strong><span class="glyphicon glyphicon-info-sign"></span></strong> Register and sign in to have access to farmer profiles and future stock information!
                    </div>

                @endif

                <?php

                    $array = [];

                    for (  $i = 0; $i < sizeof($stock); $i++) {

                        $currentStock = [];
                        $latLong = explode(",", $stock[$i]['location']  );
                        $currentStock["lat"] = $latLong[0];
                        $currentStock["lng"] = $latLong[1];
                        $currentStock["name"] = $stock[$i]['city'];

                        array_push(  $array, $currentStock  );

                    }

                    echo "<script> var markers = ".json_encode(  $array  )."; </script>";

                ?>

                <div id="map" style="height: 440px; border: 1px solid #AAA; top:30px"></div>

            </div>


            <div style="margin-top:10px; padding:0" class="col-lg-3 col-md-3 col-sm-4 col-xs-6 off-canvas-right" id="swipeme-right">

                <div style="padding: 10px; background-color: rgb(74, 69, 69); color: #fff; margin-bottom:0" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="glyphicon glyphicon-filter"></span> Filter</div>
                <div id="demo" class="panel panel-default col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0 0 20px 0">
                    
                    <form action="/agri/product_profile/{{ strtolower($product->name) }}/map" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" role="form" enctype="multipart/form-data">
                
                         <!-- Laravel Requirement -->
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />

                        <div style="margin:10px auto" class="form-group">
                            <label for="unit_cost">Quantity</label>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                    <label for="start_quantity">Between</label>
                                    <input type="number" class="form-control" name="start_quantity" min=0 
                                        <?php if(  isset($startQuantity) && $startQuantity  ) echo "value='".$startQuantity."'"; ?> 
                                        id="start_quantity" >
                                </div>
                                
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                    <label for="end_quantity">And</label>
                                    <input type="number" class="form-control" name="end_quantity" min=0
                                        <?php if(  isset($endQuantity) && $endQuantity  ) echo "value='".$endQuantity."'"; ?> 
                                        id="end_quantity" >
                                </div>
                            </div>
                            
                        </div>

                        @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
                            <div style="margin:10px auto" class="form-group">
                                <label for="unit_cost">Maturity Date</label>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                        <label for="start_date">Between</label>
                                        <input type="text" class="form-control input-medium default-date-picker" name="start_date" id="start_date"
                                            @if($startDate)
                                                value='{{  $startDate  }}'
                                            @endif
                                            >
                                    </div>
                                    
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                        <label for="end_date">And</label>
                                        <input type="text" class="form-control input-medium default-date-picker" name="end_date" id="end_date"
                                            @if($endDate) 
                                                value='{{  $endDate  }}'
                                            @endif
                                        >
                                    </div>
                                </div>

                            </div>
                        @endif


                        <button type="submit" class="btn btn-primary btn-sm col-lg-6 col-md-7 col-sm-9 col-xs-10" data-dismiss="modal">Filter Stock</button>

                    </form>

                </div>
            </div>

        </div>


    @include('templates.js_links')
    
    <!-- allow us have the fancy dashboard on the left -->
    <script class="include" type="text/javascript" src="/agri/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/agri/js/jquery.scrollTo.min.js"></script>

    <!-- custom script (should be removed or edited) -->
    <script src="/agri/js/common-scripts.js"></script>

    <!-- leaflet script -->
    <script type='text/javascript' src='/agri/js/leaflet/leaflet.js'></script>

    <!-- map testing scripts -->
    <script type='text/javascript' src='/agri/maps/markers.json'></script>
    <script type='text/javascript' src='/agri/maps/leaf-demo.js'></script>

    @if(  Auth()->guard( "staff" )->check() ||  Auth()->guard( "buyer" )->check() ||  Auth()->guard( "farmer" )->check()  )
        <script type="text/javascript">
            $(document).ready(function(){
                $("aside").css("margin-left", -$("aside").width()-50  );
            });
        </script>
    @endif

    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on("click", "#dashboard_show", function(){
                $("#swipeme").css("margin-left", $("aside").width()+50);
            });

            $(document).on("click", "#filter_show", function(){
                $("#swipeme").css("margin-left", 0);
            });


        });

    </script>

</body>
</html> 