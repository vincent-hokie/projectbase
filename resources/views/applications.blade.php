@extends('templates.dashboard')

@section('menu')
    @include('menus.'.$role)
@endsection

@section('content')

<div class="tab-content">

    <div id="menu1" class="tab-pane fade">
        <h3>Menu 1</h3>
        <p>Some content in menu 1.</p>
    </div>

    <div id="profile" class="tab-pane fade">

        <h3>My Profile<hr></h3>
        <div class="body"></div>

    </div>

    <div id="profileEdit" class="tab-pane fade"><div class="body"></div></div>

    <div id="searchStock" class="tab-pane fade"><div class="body"></div></div>

    <div id="newNeed" class="tab-pane fade"><div class="body"></div></div>

    <div id="myNeeds" class="tab-pane fade"><div class="body"></div></div>

    <div id="applications" class="tab-pane fade in active">
        <div class="body">
            @include('fragments.applications')
        </div>
    </div>

    <div id="needView" class="tab-pane fade">

        <h3>AgriNeed<hr></h3>

        <div class="body"></div>

    </div>

    <div id="needEdit" class="tab-pane fade">

        <h3>AgriNeed<hr></h3>

        <div class="body"></div>

    </div>

    <div id="deals" class="tab-pane fade"><div class="body"></div></div>

</div>

@endsection
