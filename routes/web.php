<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');


/* 	Route::method(url, controller@function)	 */

Route::get('/', 'MiscController@index');



Route::get('/search', 'MiscController@product_search');
Route::get('/dashboard', "DashboardPages@index");
Route::get('/{type}', 'MiscController@index');


Route::get('/product_profile/{product}', 'MiscController@get_item_profile');
Route::get('/product_profile/{product}/map', 'MiscController@get_map_profile');
Route::post('/product_profile/{product}', 'MiscController@get_item_profile');
Route::post('/product_profile/{product}/map', 'MiscController@get_map_profile');



Route::get('/product_profile', function () { return Redirect::to('/'); });

/*****************************************************************
 *						Dashboard Pages
 *****************************************************************/



/********* default dashboard page **********/







/********* user profile routes **********/

Route::get('/dashboard/profile', "DashboardPages@profile");
Route::get('/dashboard/profile/edit', "DashboardPages@profileEdit");
Route::post('/profile/save', "DashboardPages@profileSave");
Route::post('/{role}/changePassword/{email}', "DashboardPages@changePassword");
Route::post('/farmer/updateProducts/{email}', "StaffController@updateProducts");





/********* routes concerning products **********/

Route::get('/dashboard/products', "StaffController@products");
Route::get('/dashboard/products/{item}', "StaffController@getProduct");
Route::get('/dashboard/products/{item}/edit', "StaffController@editProduct");
Route::get('/dashboard/new/product', "StaffController@newProduct");
Route::post('/product/{item}/save', "StaffController@saveProduct");
Route::post('/uploadFile', 'StaffController@insert_product');
Route::post('/deleteProduct', 'StaffController@deleteProduct');




/********* routes concerning farmers **********/

Route::get('/dashboard/new/farmer', "StaffController@newFarmer");
Route::get('/dashboard/new/farmer/2', "StaffController@newFarmer2");
Route::get('/dashboard/new/farmer/3', "StaffController@newFarmer3");

Route::get('/dashboard/farmers', "StaffController@view_farmers");
Route::get('/dashboard/farmer/{email}', "DashboardPages@farmer_profile");
Route::get('/dashboard/farmer/{email}/land', "StaffController@farmer_land");

Route::post('/dashboard/farmer/verify/{action}/{id}', "StaffController@verify_farmer");
Route::post('/dashboard/farmer/enable/{action}/{id}', "StaffController@enable_farmer");

Route::post('/save/farmer', "StaffController@farmer_insert");
Route::post('/save/farmer/land', "StaffController@farmer_insert_land");
Route::post('/save/farmer/land/update/{id}', "StaffController@farmer_update_land");





/********* routes concerning staff **********/

Route::get('/dashboard/new/staff', "StaffController@newStaff");
Route::get('/dashboard/staff', "StaffController@view_staff");
Route::get('/dashboard/staff/{email}', "StaffController@staff_profile");

Route::post('/dashboard/staff/enable/{action}/{id}', "StaffController@enable_staff");
Route::post('/dashboard/staff/verify/{action}/{id}', "StaffController@verify_staff");

Route::post('/save/staff', "StaffController@staff_insert");





/********* routes concerning buyers **********/

Route::get('/dashboard/buyers', "StaffController@view_buyers");
Route::get('/dashboard/buyer/{email}', "DashboardPages@buyer_profile");

Route::post('/dashboard/buyer/enable/{action}/{id}', "StaffController@enable_buyer");
Route::post('/dashboard/buyer/verify/{action}/{id}', "StaffController@verify_buyer");




/********* routes concerning buyer needs **********/

Route::get('/dashboard/new/need', "BuyerController@newNeed");
Route::get('/dashboard/my-needs', "BuyerController@myNeeds");
Route::get('/dashboard/my-needs/{item}', "BuyerController@getNeed");
Route::get('/dashboard/my-needs/{item}/edit', "BuyerController@editNeed");
Route::get('/dashboard/search/need', "FarmerController@searchNeedForm");
Route::post('/dashboard/search/need', "FarmerController@searchNeedForm");
Route::post('/update_need/{item}', 'BuyerController@update_need');
Route::post('/create_need', 'BuyerController@store_need');
Route::post('/deleteNeed', 'BuyerController@deleteNeed');

Route::post('/dashboard/need/valid/{action}/{id}', "BuyerController@validate_need");





/********* routes concerning farmer stock **********/

Route::get('/dashboard/new/stock', "FarmerController@newStock");
Route::get('/dashboard/my-stock', "FarmerController@myStock");
Route::get('/dashboard/my-stock/{item}', "FarmerController@getStock");
Route::get('/dashboard/my-stock/{item}/edit', "FarmerController@editStock");
Route::get('/dashboard/search/stock', "DashboardPages@searchStock_table");
Route::post('/dashboard/search/stock', "DashboardPages@searchStock_table");

Route::get('/dashboard/search/stock/map', "DashboardPages@searchStock_map");
Route::post('/dashboard/search/stock/map', "DashboardPages@searchStock_map");

Route::post('/update_stock/{item}', 'FarmerController@update_stock');
Route::post('/create_stock', 'FarmerController@store_stock');
Route::post('/deleteStock', 'FarmerController@deleteStock');

Route::post('/dashboard/stock/valid/{action}/{id}', "FarmerController@validate_stock");



/********* routes concerning stock bids **********/
Route::post('/dashboard/make/bid', "BuyerController@makeBid");
Route::post('/dashboard/farmer/make/bid', "FarmerController@farmerInterest");

Route::post('/buyer/make/bid', "BuyerController@makeBidReload");
Route::post('/farmer/make/bid', "FarmerController@makeBidReload");

Route::get('/dashboard/buyer/bid/history', "BuyerController@buyerBidHistory");
Route::get('/dashboard/farmer/bid/history', "FarmerController@farmerBidHistory");

Route::get('/dashboard/buyer/bid/converstion/{stock_id}/{buyer_id}', "BuyerController@buyerBids");
Route::get('/dashboard/farmer/bid/converstion/{stock_id}/{buyer_id}', "FarmerController@farmerBids");


