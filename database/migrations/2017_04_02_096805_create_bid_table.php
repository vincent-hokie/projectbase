<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('bid', function (Blueprint $table) {
            
            $table->increments('bid_id');
            $table->integer('buyer_id')->unsigned()->nullable();
            $table->foreign('buyer_id')->references('buyer_id')->on('buyer');
            $table->integer('stock_id')->unsigned();
            $table->foreign('stock_id')->references('stock_id')->on('stock');
            $table->decimal('unit_cost', 10, 2);
            $table->decimal('quantity', 10, 2);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('bid');
    }
}
