<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('stock', function (Blueprint $table) {
            
            $table->increments('stock_id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('product_id')->on('product');
            $table->integer('farmer_id')->unsigned();
            $table->foreign('farmer_id')->references('farmer_id')->on('farmer');
            $table->decimal('quantity_actual', 10, 2)->default(0);
            $table->decimal('quantity_expected', 10, 2)->default(0);
            $table->date('maturity_date')->nullable();
            $table->date('sowing_date')->nullable();
            $table->decimal('unit_cost', 10, 2)->default(0);
            $table->text('description');
            $table->enum('valid', array('0','1'))->default('1');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('stock');
    }
}
