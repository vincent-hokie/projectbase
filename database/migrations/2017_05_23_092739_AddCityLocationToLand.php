<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityLocationToLand extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::table('land', function ($table) {
            $table->text('city_location')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::table('land', function ($table) {
            $table->dropColumn('city_location');
        });

    }


}
