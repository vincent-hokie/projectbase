<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Staff;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('staff', function (Blueprint $table) {

            $table->increments('staff_id');
            $table->text('photo')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password', 128);

            $table->date('dob')->nullable();
            $table->integer('nationality')->unsigned()->nullable();
            $table->foreign('nationality')->references('id')->on('countries');
            $table->string('phone_no', 31)->nullable();
            $table->enum('role', array('0','1'))->default('0');
            $table->string('department')->nullable();

            $table->rememberToken();
            $table->timestamps();

        });


        Staff::create([
            'first_name' => "Vincent",
            'last_name' => "Asante_Staff",
            'email' => "vincenthokie_staff@gmail.com",
            'password' => bcrypt("staff")
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('stock');
    }
}
