<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNeedIdToBidTable extends Migration{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('bid', function ($table) {

            $table->integer('need_id')->unsigned()->nullable();
            $table->foreign('need_id')->references('need_id')->on('need');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::table('bid', function ($table) {
            $table->dropColumn('need_id');
        });

    }


}
