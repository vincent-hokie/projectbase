<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('land', function (Blueprint $table) {

            $table->increments('land_id');
            $table->integer('farmer_id')->unsigned();
            $table->foreign('farmer_id')->references('farmer_id')->on('farmer');
            $table->string('name');
            $table->text('location');
            $table->decimal('size', 10, 2);
            $table->timestamps();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('land');
    }
}
