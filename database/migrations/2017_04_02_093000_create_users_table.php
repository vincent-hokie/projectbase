<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Farmer;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('farmer', function (Blueprint $table) {

            $table->increments('farmer_id');
            $table->text('photo')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->text('products')->nullable();
            $table->string('email')->unique();
            $table->string('password', 128);
            $table->string('phone_no')->nullable();

            $table->date('dob')->nullable();
            $table->integer('nationality')->unsigned()->nullable();
            $table->foreign('nationality')->references('id')->on('countries');
            $table->string('location')->nullable();
            $table->text('farmer_profile')->nullable();
            $table->integer('production_scale')->nullable();

            $table->integer('staff_id')->unsigned()->default(1);
            $table->foreign('staff_id')->references('staff_id')->on('staff');

            $table->rememberToken();
            $table->timestamps();

        });

        Farmer::create([
            'first_name' => "Vincent",
            'last_name' => "Asante_Farmer",
            'email' => "vincenthokie_farmer@gmail.com",
            'password' => bcrypt("farmer"),
            'staff_id' => 1
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farmer');
    }
}
