<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Type;

class CreateTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('type', function (Blueprint $table) {
            $table->increments('type_id');
            $table->string('name');
            $table->timestamps();
        });

        Type::create([  'name' => "Cereals"  ]);
        Type::create([  'name' => "Legumes"  ]);
        Type::create([  'name' => "Seeds"  ]);
        Type::create([  'name' => "Fruits"  ]);
        Type::create([  'name' => "Vegetables"  ]);
        Type::create([  'name' => "Dry Fruits and nuts"  ]);
        Type::create([  'name' => "Flowers"  ]);
        Type::create([  'name' => "Edible Oils"  ]);
        Type::create([  'name' => "Essential Oils"  ]);
        Type::create([  'name' => "Beverages and juices"  ]);
        Type::create([  'name' => "Fertilizers"  ]);
        Type::create([  'name' => "Aromatic plants"  ]);
        Type::create([  'name' => "Herbal products"  ]);
        Type::create([  'name' => "Other"  ]);
        Type::create([  'name' => "Livestock"  ]);
        Type::create([  'name' => "Livestock products"  ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('type');
    }

}
