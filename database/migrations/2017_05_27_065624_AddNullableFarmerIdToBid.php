<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableFarmerIdToBid extends Migration{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('bid', function ($table) {

            $table->integer('farmer_id')->unsigned()->nullable();
            $table->foreign('farmer_id')->references('farmer_id')->on('farmer');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::table('bid', function ($table) {
            $table->dropColumn('farmer_id');
        });

    }

}
