<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Buyer;

class CreateBuyerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('buyer', function (Blueprint $table) {

            $table->increments('buyer_id');
            $table->text('photo')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password', 128);
            $table->string('phone_no')->nullable();

            $table->date('dob')->nullable();
            $table->integer('nationality')->unsigned()->nullable();
            $table->foreign('nationality')->references('id')->on('countries');

            $table->string('location')->nullable();
            $table->text('buyer_profile')->nullable();
            $table->text('products')->nullable();
            $table->text('farmers')->nullable();
            

            $table->rememberToken();
            $table->timestamps();
            
        });

        Buyer::create([
            'first_name' => "Vincent",
            'last_name' => "Asante_Buyer",
            'email' => "vincenthokie_buyer@gmail.com",
            'password' => bcrypt("buyer")
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('buyer');
    }
}
