<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('need', function (Blueprint $table) {
            
            $table->increments('need_id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('product_id')->on('product');
            $table->integer('buyer_id')->unsigned();
            $table->foreign('buyer_id')->references('buyer_id')->on('buyer');
            $table->date('date_required');
            $table->decimal('quantity', 10, 2)->default(0);
            $table->decimal('proposed_price', 10, 2)->default(0);
            $table->text('description');
            $table->enum('valid', array('0','1'))->default('1');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::dropIfExists('need');
    }
}
