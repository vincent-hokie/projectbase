<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLandIdToStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('stock', function ($table) {

            $table->integer('land_id')->unsigned();
            $table->foreign('land_id')->references('land_id')->on('land');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::table('stock', function ($table) {
            $table->dropColumn('enabled');
        });

    }
}
