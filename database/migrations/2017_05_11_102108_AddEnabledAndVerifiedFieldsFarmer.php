<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnabledAndVerifiedFieldsFarmer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('farmer', function ($table) {
            $table->enum('enabled', array('0','1'))->default('1');
            $table->enum('verified', array('0','1'))->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('farmer', function ($table) {
            $table->dropColumn('enabled');
            $table->dropColumn('verified');
        });
    }
}
